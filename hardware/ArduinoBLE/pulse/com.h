/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef COM
#define COM

#include <Arduino.h>

class Com {
  public:
    //Declare enum to mode send
    enum modeSend{
      none = 0,
      sendSol = 1,
      sendBlue = 2,
      upBlue = 3,
      downBlue = 4
    };
    
    /**
     * @brief Create object
     * @param {Int} Information on the type of solenoids
     * @param {Int} Size of the buffer of Pulse
     * @param {String} MAC address to the Bluetooth
     * @param {Int} Size of the buffer of Bluetooth
     */
    void init(int *bSol2, int sizePulse2, String mac2, int sizeBlue2){
      bSol = bSol2;
      mac = mac2;
      sizePulse = sizePulse2;
      sizeBlue = sizeBlue2;
      Serial.begin(115200);
    }

    /**
     * @brief Start to USB connection
     */
    void start(){
      if(reconnect){
        while(!Serial);
        while(Serial.available()>0) Serial.read();
        Serial.print("I");
        for(int i=0;i<3;i++) Serial.print((String) bSol[i] + ",");
        Serial.print((String) sizePulse + "," + mac + "," + (String) sizeBlue + ";");
        sError=0;
        readyStart(false);
        readyR=false;
        resetData();
      }
      reconnect=!Serial;
    }

    /**
     * @brief Send a Error
     * @param {Integer} Number of error
     */
    void error(int nError){
      if (nError == sError) return;
      if (0<sError && sError<4) return;
      if (nError==0){
        if(readyS>pulseToReady) sError=0;
        return;
      }
      
      if(!(nError==5 && sError==6) && stand==0){
        if (nError==5) {
          readyStart(false);
          return;
        }
        if (nError == 6) return;
        sError = nError;
      } else sError = nError;

      while(Serial.available()>0) Serial.read();
      stand=0;

      Serial.print("E" + (String) sError + ";");
      
      if (4 <= sError) readyStart(false);
      mode = modeSend::none;
    }

    /**
     * @brief Send info if ready to recive (initialize)
     * @param {Bool} Indicates if you are ready to send data
     */
    void readyStart(bool readyS2){
      if (!readyS2) {
        readyS=0;
        return;
      }
      if (readyS>pulseToReady) return;
      else if (readyS<pulseToReady) readyS++;
      else {
        readyS++;
        Serial.print("R");
      }
    }

    /**
     * @brief Send when the stream ready to continue recive (stream data)
     */
    void readyRecive(){
      if (sError != 0 || readyR) return;
      Serial.print("R");
      readyR=true;
    }
     
    /**
     * @brief Get the type of data received
     * @return {Enum} Type of data recived
     */
    modeSend getType(){
      if(stand>0) return mode;
      if(Serial.available()>0) mode = readType();
      else mode = modeSend::none;
      return mode;
    }

    /**
     * @brief Get data of the buffer
     * @param {byte} Pointer of bytes
     * @param {int} Maximum size allocate of pointer
     */
    int getData(byte *infSend, int sizeSend){
      int cStand = (sizeSend>stand) ? stand : sizeSend;
      for(int i=0; i<cStand; i++) infSend[i]=buff[(pos+i)%128];
      stand -= cStand;
      pos = (pos+cStand)%128;
      if(stand==0)readyR=false;      
      return cStand;
    }

    /**
     * @brief Reset all buffer fill of 0x00 data
     */
    void resetData(){
      stand=0;
    }

    /**
     * @brief Recive data from the port COM and save in the Buffer
     */
    void receiveData() {
      if (stand>240 || mode!=readType()) return;
      Serial.read();
      int posStart=(pos+stand)%128, addStand = getToSymb(',').toInt();
      for (int i=posStart; i<posStart+addStand; i++) buff[i%128]=Serial.read();
      stand += addStand;
      readyR=false;
    }

    /**
     * @brief Recive data from the port until to symbol
     * @param {Char} Symbol to stop
     * @return {String} Data to recive
     */
    String getToSymb(char symb){
      String text="";
      char infoRead = Serial.read();
      while(infoRead != symb){
        text += infoRead;
        if(Serial.available()>0) infoRead = Serial.read();
        else break;
      }
      return text;
    }

    /**
     * @brief Quit data
     */
    void quitByte(){
      readyR=false;
      Serial.read();
    }

    /**
     * @brief Get stand
     */
    int getStand(){ return stand; }

    bool getReconnect(){ return reconnect; }

    void readyBlue(){
      Serial.print("C");
    }

  private:
    //Information to device
    int *bSol, sizePulse, sizeBlue;
    String mac;
    
    //Counters for an event
    const int pulseToReady=50;
    int sError=0, readyS=0;
    boolean readyR=false;
    
    //Mode to send
    modeSend mode = modeSend::none;
    
    //Buffer info and positon to send
    byte buff[128];
    
    //Remaining to be transmitted
    int stand=0, pos=0;

    //Detect if stop next time
    bool reconnect=true;

    /**
     * @brief Get of type 
     */
    modeSend readType(){
      switch(Serial.peek()){
        case 'S': return modeSend::sendSol;
        case 'B': return modeSend::sendBlue;
        case 'U': return modeSend::upBlue;
        case 'D': return modeSend::downBlue;
        default: return modeSend::none;
      }
    }
};

#endif
