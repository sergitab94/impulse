/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef DRIVEBLUE
#define DRIVEBLUE

#include <ArduinoBLE.h>
#include <Scheduler.h>

class DriveBlue {
  public:
    /**
     * @brief Initialize the service Bluetooth
     * @param {Char} Device Name
     */
    void init(char *device_name){
      this->device_name = device_name;

      //Configure Service
      bData.addCharacteristic(longPassword);
      bData.addCharacteristic(byteData);
      longPassword.setEventHandler(BLEWritten, password);
      byteData.setEventHandler(BLESubscribed, subData);
      byteData.setEventHandler(BLEUnsubscribed, unsubData);
      byteData.setEventHandler(BLEWritten, readData);
      
      //Configure Scheduler
      Scheduler.startLoop(thread);
    }

    /**
     * @brief Destroy object. Shutdown Bluetooth.
     */
    ~DriveBlue(){ BLE.end(); }
    
    String getMac(){
      String address=BLE.address();
      address.replace(":", "");
      return address;
    }

    /**
     * @brief Start advertising Bluetooth
     * @return {Mixed} Error in the device bluetooth
     */
    int startAdv(){
      powerOn();      
      return 0;
    }

    /**
     * @brief Stop advertising Bluetooth
     * @return {Mixed} Error in the device bluetooth
     */
    void stopAdv(){
      isConn = false;
      auth = false;
      sData = false;
      powerOff();
    }

    /**
     * @brief Set new password
     * @param {String} New Password
     */
    void setPass(String pinR){
      pin = pinR.toInt();
    }

    /**
     * @brief Send data to device
     * @param {Int} Value to send
     * @param {Int} Size of the value to send
     */
    int sendData(const uint8_t *values, int sizeValue){
      if(!auth) return 8;
      sData = true;
      if(!byteData.writeValue(values, sizeValue)) return 8;
      return 0;
    }
    
    /**
     * @brief Check if device is connected (subscribed to GATT service)
     */
    bool isConnect(){ return isConn; }

    /**
     * @brief Check if device is send data
     */
    bool isSend(){ return sData; }

  private:
    //Service
    BLEService bData = BLEService("0000180a-0000-1000-8000-00805f9b34fb");  //Device Information

    //Characteristic
    BLEUnsignedLongCharacteristic longPassword = BLEUnsignedLongCharacteristic("00002a3d-0000-1000-8000-00805f9b34fb", BLEWrite);   //String
    BLECharacteristic byteData = BLECharacteristic("00002a57-0000-1000-8000-00805f9b34fb", BLERead | BLEWrite | BLENotify, 20);  //Digital Output

    //Information of drive
    char *device_name;
    static bool isConn, auth, sData;
    static unsigned long pin;

    int powerOn(){
      if(!BLE.begin()) return 3;
      
      //Configure BLE
      BLE.setDeviceName(device_name);
      BLE.setLocalName(device_name);
      BLE.setEventHandler(BLEConnected, connectDev);
      BLE.setEventHandler(BLEDisconnected, disconnectDev);
      BLE.addService(bData);

      //Advertise
      byteData.setValue(0);
      if(!BLE.advertise()) return 3;
      
      disconnectedLight();
      return 0;
    }

    void powerOff(){
      BLE.end();
      poweroffLight();
    }

    static void connectDev(BLEDevice central) {
      isConn = false;
      auth = false;
      sData = false;
      BLE.stopAdvertise();
      connectedLight();
    }
    
    static void disconnectDev(BLEDevice central) {
      isConn = false;
      auth = false;
      BLE.advertise();
      disconnectedLight();      
    }

    static void password(BLEDevice central, BLECharacteristic characteristic){
      unsigned long pinR = 0;
      characteristic.readValue(pinR);
      pinR &= 0xFFFFFF;
      byte voidInfo[] = {0x00, 0x00, 0x00};
      characteristic.writeValue(voidInfo, 3);
      auth=(pin==pinR);
    }

    static void subData(BLEDevice central, BLECharacteristic characteristic) {
      if(auth) isConn = true;
    }

    static void unsubData(BLEDevice central, BLECharacteristic characteristic) {
      isConn = false;
    }
    
    static void readData(BLEDevice central, BLECharacteristic characteristic) {
      Serial.print("F");
      sData = false;
    }

    static void thread(){
      BLE.poll();
      yield();
    }
  
    static void connectedLight() {
      analogWrite(LEDR, 256);
      analogWrite(LEDG, 250);
    }
    
    static void disconnectedLight() {
      analogWrite(LEDR, 250);
      analogWrite(LEDG, 256);
    }

    static void poweroffLight(){
      analogWrite(LEDR, 256);
      analogWrite(LEDG, 256);
    }
};

bool DriveBlue::isConn = false;
bool DriveBlue::auth = false;
bool DriveBlue::sData = false;
unsigned long DriveBlue::pin = 0;

#endif
