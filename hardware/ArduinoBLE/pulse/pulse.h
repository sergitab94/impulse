/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef PULSE
#define PULSE

#include "com.h"

#include <Arduino.h>

class Pulse {
  public:
    /**
     * @brief Create object
     * @param {Integer[]} X solenoid pins
     * @param {Integer[]} Y solenoid pins
     * @param {Integer[]} Z solenoid pins
     * @param {Object} Object to send info through the port
     * @param {Integer} Size recover data (max. 64 and divisible for 60)
     */
    Pulse(int *x, int *y, int *z, int *bitSol, Com *com2, int sizePulse2 = 60) {
      sol[0] = x;
      sol[1] = y;
      sol[2] = z;
      for(int i = 0; i<3; i++) bSol[i] = pow(2, bitSol[i])-1;
      com = com2;
      sizePulse = sizePulse2;
      mSizePulse = sizePulse2/2;
      for(int i=0; i<3; i++) tSol[i] = initSol(sol[i]);
    }

    /**
     * @brief Get size of buffer
     * @return {Int} Size of buffer
     */
    inline int getSize() { return sizePulse; }

    inline void resetData(){
      resetSolenoid();
      com->resetData();
    }

    /**
     * @brief Delete data of buffer
     */
    inline void resetSolenoid(){
      byte infSend[3]={0x00, 0x00, 0x00};
      sendDataSol(infSend, 3);
    }

    /**
     * @brief Send data through the Solenoid
     */
    inline void sendData(){
      byte infSend[3]={0x00, 0x00, 0x00};
      int sizeSend = com->getData(infSend, 3);
      sendDataSol(infSend, sizeSend);
      if(com->getStand()<=mSizePulse && com->getStand()!=0) com->readyRecive();
    }

  private:
    int *sol[3], tSol[3], bSol[3];
    int sizePulse, mSizePulse;
    Com *com;

    /**
     * @brief Initialize solenoid
     * @param {Integer[]} Solenoid to initialize
     * @return {Integer} Size of solenoid
     */
    inline int initSol(int sol[]) {
      int i = 0;
      while(sol[i] != -1) {
        pinMode(sol[i], OUTPUT);
        digitalWrite(sol[i], 0);
        i++;
      }
      return i;
    }

    /**       
     * @brief Send data to solenoid (maximum 1 byte)
     * @param {Byte} Data to send
     * @param {Integer[]} Solenoid pins
     * @param {Integer} Solenoid size
     */
    inline void sendSol(byte num, int *locSol, int locBSol, int locTSol) {
      if(num>locBSol) return;
      int numF = num + locSol[locTSol+1];
      for (int x = 0; x < locTSol; x++) digitalWrite(locSol[x], bitRead(numF, x));
    }

    /**
     * @brief Send a maximum of one byte per solenoid
     * @param {Byte[]} Information to send
     * @param {Integer} Size information to send
     */
    inline void sendDataSol(byte *info, unsigned int sizeInfo) {
      unsigned int i = 0;
      for (i = 0; i < sizeInfo; i++) sendSol(info[i], sol[i], bSol[i], tSol[i]);
      for (; i<3; i++) sendSol(0x00, sol[i], bSol[i], tSol[i]);
    }
};

#endif
