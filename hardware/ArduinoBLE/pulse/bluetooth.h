/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef BLUETOOTH
#define BLUETOOTH

#include <Arduino.h>

#include "driveblue.h"
#include "com.h"
#include "timer.h"
#include "pulse.h"

class Blue {
  public:

    /**
     * @brief Intialize Bluetooth
     * @param {Char} Name of device
     * @param {Object} Send to the port COM
     * @param {Int} Size of the buffer (max. 64 and divisible for 20)
     */
    void init(char *device_name, int timeWait2, Pulse *pulse2, Com *com2, int sizeBlue2 = 60) {
      timeWait = timeWait2*1000L;
      pulse = pulse2;
      com = com2;
      sizeBlue = sizeBlue2;
      mSizeBlue = sizeBlue2/2;
      driver.init(device_name);
      timePass=0;
    }

    /**
     * @brief Get "Media Access Control" of the Bluetooth
     * @return {String} MAC address
     */
    String getMac(){ return driver.getMac(); }

    /**
     * @brief Size of the buffer
     * @return {Int} Size of the buffer
     */
    int getSize(){ return sizeBlue; }

    /**
     * @brief Start up the bluetooth
     */
    void up(){
      int error;
      com->quitByte();
      driver.setPass(com->getToSymb(';'));
      error=driver.startAdv();
      com->error(error);
      com->readyRecive();
      if(error!=0) return;
      timePass=millis()+timeWait;
    }

    /**
     * @brief Check if device is connected
     */
    void check(){
      if(com->getReconnect()){
        driver.stopAdv();
        timePass=0;
      }else if(timePass==0){ return;
      }else if(driver.isConnect()){
        resetSol=true;
        com->readyBlue();
        timePass=0;
      }else if(timePass<=millis()){
        com->error(7);
        driver.stopAdv();
        timePass=0;
      }
    }

    /**
     * @brief Stop the bluetooth
     */
    void down(){
      driver.stopAdv();
      com->quitByte();
      com->readyRecive();
      timePass=0;
    }

    /**
     * @brief Send info saved in the buffer
     */
    void sendData(){
      if (!driver.isConnect()){
        com->error(8);
        driver.stopAdv();
        return;
      }
      if (driver.isSend()) return;
      int errorInt=0, sizeSend = com->getData(infSend, 20);
      errorInt = driver.sendData((const uint8_t*) infSend, sizeSend);
      com->error(errorInt);
      if (errorInt>=7) driver.stopAdv();
      if (com->getStand()<=mSizeBlue && com->getStand()!=0) com->readyRecive();
      if(resetSol) {
        resetSol = false;
        pulse->resetSolenoid();
      }
    }
    
  private:
    Com *com;
    Pulse *pulse;
    bool resetSol;
    DriveBlue driver;
    byte infSend[20]={0x00};
    int sizeBlue, mSizeBlue;
    unsigned long timeWait, timePass;
};

#endif
