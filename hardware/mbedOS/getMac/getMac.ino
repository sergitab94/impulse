/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include <mbed.h>
#include <rtos.h>
#include <ble/BLE.h>
#include <ble/Gap.h>

static events::EventQueue event_queue(16 * EVENTS_EVENT_SIZE);
rtos::Thread *t;

class Device: SecurityManager::EventHandler,
ble::Gap::EventHandler,
ble::GattServer::EventHandler{
  public:
    void init(){
      if (ble.hasInitialized()) return;
      ble.onEventsToProcess(makeFunctionPointer(this, &Device::scheduleble_events));
      ble.init(this, &Device::init_complete);
      
      t = new rtos::Thread();
      t->start(mbed::callback(&_event_queue, &events::EventQueue::dispatch_forever));
    }
    
    String getMac(){ return mac; }

  private:
    events::EventQueue &_event_queue = event_queue;
    BLE &ble = BLE::Instance();
    String mac;

    void scheduleble_events(BLE::OnEventsToProcessCallbackContext *context) {
      _event_queue.call(mbed::callback(&context->ble, &BLE::processEvents));
    }

    void init_complete(BLE::InitializationCompleteCallbackContext *event) {
      ble::own_address_type_t addressType;
      ble::address_t macByte;
      ble.gap().getAddress(addressType, macByte);
      mac = "";
      for (int i = 5; i >= 0; i--) mac += String(macByte[i], HEX);
    }
};

Device info;

void setup() {
  Serial.begin(9600);
  info.init();
}

void loop() {
  if(Serial){
    Serial.println("MAC: «" + info.getMac() + "»");
    while(Serial);
  }
}
