/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#include "bluetooth.h"
#include "detect.h"
#include "com.h"
#include "pulse.h"
#include "timer.h"

//Solenoid pins (Z, Y, X)
int sol1[] = {A7, A6, A3, A2, -1, 2};
int sol2[] = {-1};
int sol3[] = {-1};

//Solenoid Size (Z, Y, X)
int bSol[] = {3, 0, 0};

//Pinout to power
int powerPin=2;

//Device name bluetooth
String mac="001122334455";
const char *device_name = "Impulse";
int timeWait=300;

//Control the bluetooth
Blue blue;
//Sensor, accelerometer and proximity
Detect detect;
//Control data to send from port COM
Com com;
//Control the solenoids
Pulse pulse(sol1, sol2, sol3, bSol, &com);
//Control the time
Timer timer(millis);

//Repeat run
void setup(){
  blue.init(mac, device_name, timeWait, &pulse, &com);
  detect.init(powerPin, &com);
  com.init(bSol, pulse.getSize(), blue.getMac(), blue.getSize());
  timer.work(50, daemon);
}

//Repeat execute
void loop(){
  timer.tick();
}

/**
 * @brief Run daemon in an exact time
 */
inline void daemon(){
  com.start();
  blue.check();
  Com::modeSend typeInfo = com.getType();
  switch(typeInfo){
  case Com::modeSend::none:
    detect.checkSensors();
    timer.changeTime(50);
    return;
  case Com::modeSend::sendSol:
    com.receiveData();
    if (!detect.checkSensors()) return pulse.resetData();
    pulse.sendData();
    timer.changeTime(50);
    return;
  case Com::modeSend::sendBlue:
    com.receiveData();
    blue.sendData();
    timer.changeTime(0);
    return;
  case Com::modeSend::upBlue:
    blue.up();
    timer.changeTime(1000);
    return;
  case Com::modeSend::downBlue:
    blue.down();
    timer.changeTime(50);
    return;
  }
}
