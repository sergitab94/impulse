/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef DETECT
#define DETECT

#include <Arduino.h>
#include <SparkFun_APDS9960.h>
#include <Arduino_LSM9DS1.h>

#include "com.h"

class Detect {
  public:

    /**
     * @brief Initialize the sensors
     * @param {Int} Pin in detect power input
     * @param {Object} Send to the port COM
     */
    void init(int powerPin2, Com *com2) {
      powerPin = powerPin2;
      com = com2;
      apds.init();
      apds.setProximityGain(PGAIN_2X);
      apds.enableProximitySensor(false);
      IMU.begin();
      pinMode(powerPin, INPUT);
    }

    /**
     * @brief Check the sensors and data can be sent
     * @return {Bool} True if it works properly
     */
    inline bool checkSensors() {
      if (power()) {
        com->error(4);
        return false;
      }
      if (proximity()) {
        com->error(5);
        return false;
      }
      if (motion()) {
        com->error(6);
        return false;
      }
      com->error(0);
      com->readyStart(true);
      return true;
    }
    
  private:
    //Send data
    Com *com;
    //Proximity Sensor
    SparkFun_APDS9960 apds = SparkFun_APDS9960();
    //Info to calibrate
    uint8_t data_proximity = 0;
    int powerPin;


    /**
     * @brief Check if mobile is available
     * @return {Bool} Return if mobile is available
     */
    inline bool proximity() {
      if (apds.readProximity(data_proximity)) return (data_proximity < 250);
      return false;
    }

    /**
     * @brief Check if move the object
     * @return {Bool} Return if move the object
     */
    inline bool motion() {
      float x, y, z;
      if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(x, y, z);
        return (abs(x)>20.0 || abs(y)>20.0 || abs(z)>20.0);
      }
      return false;
    }

    /**
     * @brief Check if power supply is up
     * @return {Bool} Return if power supply is up
     */
    inline bool power() {
      return (digitalRead(powerPin) == LOW);
    }
};

#endif
