/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef DRIVEBLUE
#define DRIVEBLUE

#include <mbed.h>
#include <rtos.h>
#include <ble/BLE.h>
#include <ble/Gap.h>
#include <SecurityManager.h>

//Event queue of the thread and thread
static events::EventQueue event_queue(16 * EVENTS_EVENT_SIZE);
rtos::Thread *t;

class DriveBlue : SecurityManager::EventHandler,
ble::Gap::EventHandler,
ble::GattServer::EventHandler {
  public:
    /**
     * @brief Initialize the service Bluetooth
     * @param {Char} Device Name
     * @param {String} Passwor of device (only numbers)
     * @return {Mixed} Error in the device bluetooth
     */
    void init(const char *device_name = "Example", String password = "123456"){
      this->device_name = device_name;
      setPass(password);
      pinMode(LEDR, OUTPUT);
      pinMode(LEDG, OUTPUT);
      poweroffLight();
    }

    /**
     * @brief Destroy object. Shutdown Bluetooth.
     */
    virtual ~DriveBlue(){ if (ble.hasInitialized()) ble.shutdown();  }

    /**
     * @brief Disconnect to the device
     * @return {Mixed} Error in the device bluetooth
     */
    ble_error_t disconnect() {
      if (isConn) return ble.gap().disconnect(connection, ble::local_disconnection_reason_t::POWER_OFF);
      return ble_error_t::BLE_ERROR_NONE;
    }

    /**
     * @brief Start advertising Bluetooth
     * @return {Mixed} Error in the device bluetooth
     */
    ble_error_t startAdv(){
      if (ble.hasInitialized()) stopAdv();
      start();
      disconnectedLight();
      return error;
    }

    /**
     * @brief The device need power off
     */
    bool needStopPower(){
      return permissionPower;
    }

    /**
     * @brief Stop advertising Bluetooth
     * @return {Mixed} Error in the device bluetooth
     */
    ble_error_t stopAdv(){
      if (ble.hasInitialized()){
        disconnect();
        ble_error_t error = ble.shutdown();
        t->terminate();
        poweroffLight();
        return error;
      }
      return ble_error_t::BLE_ERROR_NONE;
    }

    /**
     * @brief Set new password
     * @param {String} New Password
     */
    void setPass(String pass){
      if (pass.length() == 6 && isNumber(pass))
        for (int i = 0; i < 6; i++) passkey[i] = pass[5-i];
    }

    /**
     * @brief Send data to device
     * @param {Int} Value to send
     * @param {Int} Size of the value to send
     * @return {Mixed} Error in the device bluetooth
     */
    ble_error_t sendData(const uint8_t *values, uint16_t sizeValue){
      sData = true;
      return ble.gattServer().write(gatt, values, sizeValue);
    }
    
    /**
     * @brief Check if device is connected (subscribed to GATT service)
     */
    bool isConnect(){ return isConn; }

    /**
     * @brief Check if device is send data
     */
    bool isSend(){ return sData; }

  private:
    /**
     * @brief Check if String is a number
     * @param {String} String to check
     * @return {Bool} True if is a Number
     */
    bool isNumber(String s){
      for (unsigned int i = 0; i < s.length(); i++)
        if (isdigit(s[i]) == false) return false;
      return true;
    }

    /**
     * @brief Start Thread and Initialize Bluetooth
     */
    void start(){
      error = ble_error_t::BLE_ERROR_NONE;
      if (ble.hasInitialized()) return;
      ble.onEventsToProcess(makeFunctionPointer(this, &DriveBlue::scheduleble_events));
      ble.securityManager().setSecurityManagerEventHandler(this);
      ble.gap().setEventHandler(this);
      error = ble.init(this, &DriveBlue::on_init_complete);
      if (error) return;
      t = new rtos::Thread();
      t->start(mbed::callback(&_event_queue, &events::EventQueue::dispatch_forever));
    }

    /**
     * @brief Initialize Bluetooth
     */
    void on_init_complete(BLE::InitializationCompleteCallbackContext *event){
      if (event->error != BLE_ERROR_NONE) return;      
      error = ble.securityManager().init(true, true, SecurityManager::IO_CAPS_DISPLAY_ONLY, passkey);
      if (error) return;
      error = ble.securityManager().allowLegacyPairing(false);
      if (error) return;
      config_advertising();
    }

    /**
     * @brief Configure advertising (include creation of GATT servers
     */
    void config_advertising(){      
      uint8_t adv_buffer[ble::LEGACY_ADVERTISING_MAX_SIZE];
      ble::AdvertisingDataBuilder adv_data_builder(adv_buffer);
      adv_data_builder.setFlags();
      adv_data_builder.setName(device_name);
      adv_data_builder.setAppearance(ble::adv_data_appearance_t::UNKNOWN);

      GattCharacteristic *serialCharacteristic = new GattCharacteristic(0x2A57,
          serialValue,
          0,  //minLength
          sizeof(serialValue), //maxLength
          GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ | 
          GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE | 
          GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_INDICATE);
      GattCharacteristic::SecurityRequirement_t auth = GattCharacteristic::SecurityRequirement_t::AUTHENTICATED;
      serialCharacteristic->setSecurityRequirements(auth, auth, auth);

      GattService serialService(GattService::UUID_DEVICE_INFORMATION_SERVICE, &serialCharacteristic, 1);
      ble.gattServer().setEventHandler(this);
      
      ble.gattServer().addService(serialService);
      gatt = serialCharacteristic->getValueHandle();

      if(firstTime){
        ble::AdvertisingParameters adv_parameters(ble::advertising_type_t::CONNECTABLE_UNDIRECTED);
        error = ble.gap().setAdvertisingParameters(ble::advertising_type_t::CONNECTABLE_UNDIRECTED, adv_parameters);
        if (error) return;
        firstTime=false;
      }      
      error = ble.gap().setAdvertisingPayload(ble::LEGACY_ADVERTISING_HANDLE, adv_data_builder.getAdvertisingData());
      if (error) return;
      error = ble.gap().startAdvertising(ble::LEGACY_ADVERTISING_HANDLE);
      if (error) return;
    }

    /**
     * @brief Schelude events of the Thread
     */
    void scheduleble_events(BLE::OnEventsToProcessCallbackContext *context){
      _event_queue.call(mbed::callback(&context->ble, &BLE::processEvents));
    }

    /**
     * @brief Call when the connection is completed
     */
    virtual void onConnectionComplete(const ble::ConnectionCompleteEvent &event){
      ble.securityManager().setLinkSecurity(
        event.getConnectionHandle(),
        SecurityManager::SECURITY_MODE_ENCRYPTION_WITH_MITM);
    }

    /**
     * @brief Call when the connection is disconnect
     */
    void onDisconnectionComplete(const ble::DisconnectionCompleteEvent &event) override{
      isConn = false;
      sData = false;
      ble.gap().startAdvertising(ble::LEGACY_ADVERTISING_HANDLE);
      disconnectedLight();
    }

    /**
     * @brief Call when the device is pairing
     */
    void pairingResult(ble::connection_handle_t connectionHandle, SecurityManager::SecurityCompletionStatus_t result) override{
      if (result == SecurityManager::SEC_STATUS_SUCCESS) connection = connectionHandle;
    }

    /**
     * @brief Call when the device is subscribe to server is enabled
     */
    void onUpdatesEnabled(const GattUpdatesEnabledCallbackParams &params) override{
      isConn = true;
      permissionPower=false;
      connectedLight();
    }

    /**
     * @brief Call when the device is subscribe to server is disabled
     */
    void onUpdatesDisabled(const GattUpdatesDisabledCallbackParams &params) override{
      isConn = false;
      disconnectedLight();
    }

    /**
     * @brief Call when the device is recived the data
     */
    void onDataSent(const GattDataSentCallbackParams &params) override{
      sData = false;
    }

    /**
     * @brief A signal recived from the phone for the power off the bluetooth device
     */
    void onDataWritten(const GattWriteCallbackParams &params) override{
      permissionPower=true;
    }

  private:
    //Info to the device
    SecurityManager::Passkey_t passkey;
    const char *device_name;
    bool firstTime=true;
    
    //Objects to manipule device
    ble_error_t error = ble_error_t::BLE_ERROR_NONE;
    events::EventQueue &_event_queue = event_queue;
    ble::connection_handle_t connection = 0;
    GattAttribute::Handle_t gatt = 0;
    BLE &ble = BLE::Instance();
    
    //State of the device
    bool isConn = false, sData = false, permissionPower=false;
    
    //Buff to the driver
    byte serialValue[40];

    /**
     * @brief Power on led geen
     */
    void connectedLight() {
      analogWrite(LEDR, 256);
      analogWrite(LEDG, 250);
    }

    /*
     * @brief Power on led red
     */
    void disconnectedLight() {
      analogWrite(LEDR, 250);
      analogWrite(LEDG, 256);
    }

    /**
     * @brief Power off all leds
     */
    void poweroffLight(){
      analogWrite(LEDR, 256);
      analogWrite(LEDG, 256);
    }
};

#endif
