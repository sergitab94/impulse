/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef TIMER
#define TIMER

#include <Arduino.h>

class Timer {
  public:
    unsigned long (*time_func)();
    typedef void (*tHandler)();

    /**
     * @brief Create object
     * @param {Function} Time function (default: milliseconds)
     */
    Timer(unsigned long (*t)() = millis){time_func = t;}

    /**
     * @brief Runs all the time
     */
    inline void tick(){
        if (!handler) return;
        const unsigned long timeFunc = time_func(), duration = timeFunc - start;
        if (duration < expires) return;
        if (duration >= expires*2) start = timeFunc;
        else start += expires;
        handler();
        return;
    }

    /**
     * @brief Initialize the procedure to run
     * @param {Long} Run interval
     * @param {Mixed} Procedure to execute
     */
    inline void work(unsigned long interval, tHandler h){
        if (handler != nullptr) return;
        handler = h;
        start = time_func();
        expires = interval;
    }

    /**
     * @brief Change the frequency interval
     * @param {Long} Interval time
     */
    inline void changeTime(unsigned long interval){
      expires = interval;
    }

  private:
    tHandler handler = nullptr;
    unsigned long start, expires;
};

#endif
