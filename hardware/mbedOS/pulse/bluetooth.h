/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/
#ifndef BLUETOOTH
#define BLUETOOTH

#include <Arduino.h>

#include "driveblue.h"
#include "com.h"
#include "timer.h"
#include "pulse.h"

class Blue {
  public:

    /**
     * @brief Intialize Bluetooth
     * @param {Char} Name of device
     * @param {Int} Time waiting connection in power on in seconds
     * @param {Int} Time waiting to power off in seconds
     * @param {Object} Send to the port COM
     * @param {Int} Size of the buffer (max. 64 and divisible for 40)
     */
    void init(String mac2, const char *device_name, int timeWaitOn2, int timeWaitOff2, Pulse *pulse2, Com *com2, int sizeBlue2 = 60) {
      mac = mac2;
      timeWaitOff = ceil((timeWaitOff2*1000.0)/50.0);
      timeWaitOn = timeWaitOn2*1000L;
      pulse = pulse2;
      com = com2;
      sizeBlue = sizeBlue2;
      mSizeBlue = sizeBlue2/2;
      driver.init(device_name);
      timePassOn=0;
      timePassOff=-1;
    }

    /**
     * @brief Get "Media Access Control" of the Bluetooth
     * @return {String} MAC address
     */
    String getMac(){ return mac; }

    /**
     * @brief Size of the buffer
     * @return {Int} Size of the buffer
     */
    int getSize(){ return sizeBlue; }

    /**
     * @brief Start up the bluetooth
     */
    void up(){
      int error;
      com->quitByte();
      driver.setPass(com->getToSymb(';'));
      error=errorToInt(driver.startAdv());
      com->error(error);
      com->readyRecive();
      if(error!=0) return;
      timePassOn=millis()+timeWaitOn;
    }

    /**
     * @brief Check if device is connected
     */
    void check(){
      if(com->getReconnect()){
        driver.stopAdv();
        timePassOn=0;
      }else if(timePassOn==0){
        return;
      }else if(driver.isConnect()){
        resetSol=true;
        com->resetData();
        com->readyBlue();
        timePassOn=0;
      }else if(timePassOn<=millis()){
        com->error(7);
        driver.stopAdv();
        timePassOn=0;
      }
    }

    /**
     * @brief Stop the bluetooth
     */
    void down(){
      if(timePassOff<0) timePassOff=timeWaitOff;
      if(!driver.needStopPower() && (--timePassOff)>0) return;
      com->quitByte();
      com->error(errorToInt(driver.stopAdv()));
      com->readyRecive();
      timePassOn=0;
      timePassOff=-1;
    }

    /**
     * @brief Send info saved in the buffer
     */
    void sendData(){
      if (!driver.isConnect()){
        com->error(8);
        driver.stopAdv();
        return;
      }
      if (driver.isSend()) return;
      int errorInt=0, sizeSend = com->getData(infSend, 20);
      errorInt = errorToInt(driver.sendData((const uint8_t*) infSend, (uint16_t) sizeSend));
      com->error(errorInt);
      if (errorInt>=7) driver.stopAdv();
      if (com->getStand()<=mSizeBlue) com->readyRecive();
      if(resetSol) {
        resetSol = false;
        pulse->resetSolenoid();
      }
    }
    
  private:
    Com *com;
    String mac;
    Pulse *pulse;
    bool resetSol;
    DriveBlue driver;
    byte infSend[20]={0x00};
    int sizeBlue, mSizeBlue;
    int timeWaitOff, timePassOff;
    unsigned long timeWaitOn, timePassOn;
    
    /**
     * @brief Convert ble_error_t to int error
     * @param {Mixed} Error to convert
     * @return {Int} Number to error standardized
     */
    int errorToInt(const ble_error_t error){
      switch (error) {
        //ERROR_NONE
        default:
        case ble_error_t::BLE_ERROR_NONE:
          return 0;

        //FAIL_INITIALIZATION
        case ble_error_t::BLE_ERROR_NOT_IMPLEMENTED:
        case ble_error_t::BLE_ERROR_INVALID_PARAM:
        case ble_error_t::BLE_ERROR_INITIALIZATION_INCOMPLETE:
        case ble_error_t::BLE_ERROR_ALREADY_INITIALIZED:
          return 3;

        //FAIL_TRANSFER
        case ble_error_t::BLE_ERROR_BUFFER_OVERFLOW:
        case ble_error_t::BLE_ERROR_PARAM_OUT_OF_RANGE:
        case ble_error_t::BLE_STACK_BUSY:
        case ble_error_t::BLE_ERROR_NO_MEM:
        case ble_error_t::BLE_ERROR_INTERNAL_STACK_FAILURE:
        case ble_error_t::BLE_ERROR_INVALID_STATE:
          return 8;

        //FAIL_UNKNOWN
        case ble_error_t::BLE_ERROR_OPERATION_NOT_PERMITTED:
        case ble_error_t::BLE_ERROR_UNSPECIFIED:
        case ble_error_t::BLE_ERROR_NOT_FOUND:
          return 9;
      }
    }
};

#endif
