# GNU/Linux
La versión estática es funcional, pero puede dar algún segment fault. El problema está relacionado con que las librerías estáticas pueden no ser del todo compatible con la versión GNU/Linux en la que se esté ejecutando el sistema

Sin embargo la versión dinámica es mucho estable, pero se debe de instalar libqt5serialport5 y libqt5printsupport5 para poder ejecutar la aplicación

Además de que tanto la versión estática como dinámica el usuario debe de estar incluido en el grupo dialout, sino no se podrá conectar al dispositivo a través del puerto COM

Recuerda para añadir el usuario al grupo debes de usar el comando usermod -aG dialout <usuario>

# Windows
Se da una única versión en formato estático. No es necesaria ninguna librería ni ningún requisito extra. Compatibles con todos los dispositivos con Windows 7 o superiores

# Android
El apk es autofirmado, así que se tiene que confiar en la instalación.
