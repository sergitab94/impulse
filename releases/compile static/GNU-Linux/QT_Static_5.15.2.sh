#!/bin/bash
open="/home/tabares/openssl"
qtStatic="/home/tabares/Qt/Static/5.15.2"

sudo apt install build-essential ruby perl python3 libgl1-mesa-dev libglu1-mesa-dev libx11-xcb1 libx11-xcb-dev ^libxcb.*-dev libx11-dev libxkbcommon-dev libxkbcommon-x11-dev libxkbcommon0  xcb libxinerama-dev libxrender-dev libxi-dev  libxfixes-dev libxext-dev libx11-dev libegl1-mesa-dev libfontconfig-dev libfontconfig1-dev libfreetype6-dev

mkdir -p $qtStatic
export PATH="$open/bin:$open/lib:$open/include:$PATH"
export OPENSSL_LIBS="$open/lib/libssl.a $open/lib/libcrypto.a -L$open/lib -ldl -lpthread"
export OPENSSL_PREFIX=$open
export OPENSSL_INCDIR=$open/include 
export OPENSSL_LIBDIR=$OPENSSL_PATH/lib

./configure -static -release -opensource -confirm-license -prefix "$qtStatic" -xcb-xlib -xcb -bundled-xcb-xinput -fontconfig -system-freetype -qt-zlib -qt-libpng -qt-webp -qt-libjpeg -opengl desktop -skip qt3d -skip qtandroidextras -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgamepad -skip qtlocation -skip qtlottie -skip qtmacextras -skip qtmultimedia -skip qtpurchasing -skip qtquick3d -skip qtquickcontrols -skip qtquickcontrols2 -skip qtquicktimeline -skip qtremoteobjects -skip qtscript -skip qtsensors -skip qtspeech -skip qtwayland -skip qtwebglplugin -skip qtwebview -skip webengine -make libs -nomake tools -nomake examples -nomake tests -ssl -openssl-linked -I "$open/include" -L "$open/lib" OPENSSL_LIBS="-llibssl -llibcrypto"

gmake
gmake install