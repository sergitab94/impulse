#!/bin/bash
open="/home/tabares/openssl"
qtStatic="/home/tabares/Qt/Static/5.15.2"

sudo apt install perl build-essential
mkdir -p $open
./Configure --prefix="$open" --openssldir="$open" no-shared --release linux-x86_64
make depend && make && make install_sw