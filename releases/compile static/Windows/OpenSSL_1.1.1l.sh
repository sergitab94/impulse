#!/bin/bash
pacman -Syu
pacman -Su
pacman -S mingw-w64-x86_64-perl mingw-w64-x86_64-gcc mingw-w64-x86_64-make mingw-w64-x86_64-python mingw-w64-x86_64-ruby --noconfirm --needed 
pacman -Syuu --noconfirm
mkdir -p /c/openssl
./Configure --prefix=/c/openssl --openssldir=/c/openssl no-shared --release mingw64
export PATH=$PATH:"/mingw64/bin/"
mingw32-make depend && mingw32-make && mingw32-make install_sw