/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef BB_BLUE_H
#define BB_BLUE_H

#include <QObject>
#include <QFile>

/**
 * @brief The bb_Blue class send data to the bluetooth
 */
class bb_Blue : public QObject {
    Q_OBJECT

public:
    bb_Blue(QByteArray data, QFile *file=nullptr);
    ~bb_Blue();

    float getBytes(QByteArray *output);
    void resetPosition();
    bool getDone();

private:
    int buffProcess;
    uint length;
    uint32_t sizeDo, sizeT;

    //Data and iterator
    QByteArray::iterator posData=nullptr;
    QByteArray data;
    QFile *file=nullptr;
};

#endif // BB_BLUE_H
