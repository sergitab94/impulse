/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef A_APPLICATION_H
#define A_APPLICATION_H

#include <QObject>

#include "ba_datalink.h"
#include "bb_blue.h"

/**
 * @brief The a_Application class is the top Layer.
 * Contain all information to send.
 */
class a_Application : public QObject {
    Q_OBJECT
public:
    enum TypeSec {
        OPEN,
        WPA2,
        WPA3
    };
    Q_ENUM(TypeSec)

    ~a_Application();

    void setBluetooth(QByteArray mac, uint32_t pin);
    void setFile(QFile *file, bool blue);
    void setMessage(QString message, bool blue);
    void setWifi(QString name, TypeSec sec, bool hidden, QString password, bool blue);
    void setSms(QString phone, QString message, bool blue);
    void setEmail(QString to, QString cc, QString subject, QString bcc, QString message, bool blue);
    void setBitcoin(QString address, double amount, QString message, bool blue);

    float getBytes(QByteArray *data);
    void resetPosition();
    bool getDone();
    bool forceEnd(QByteArray *data);

private:
    //Datalink object
    ba_DataLink *datalink = nullptr;
    bb_Blue *blue = nullptr;

    void generateHeader(uint8_t typeFrame, bool blue, QByteArray data, QFile *file=nullptr);
};

#endif // A_APPLICATION_H
