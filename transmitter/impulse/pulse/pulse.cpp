/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "pulse.h"

#include <QSerialPortInfo>
#include <QDateTime>
#include "c_infodevice.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif


Pulse* Pulse::instance = nullptr;

/**
 * @brief Pulse::getInstance Get instance unique of Pulse of Singeltone
 * @return Instance object Pulse
 */
Pulse* Pulse::getInstance() {
    if (instance == nullptr) instance = new Pulse();
    return instance;
}

/**
 * @brief Pulse::delInstance Delete instance object Pulse
 */
void Pulse::delInstance() {
    if (instance != nullptr){
        delete instance;
        instance=nullptr;
    }
}

/**
 * @brief Pulse::conn Connect to the port COM
 * @param port Name port to connect
 * @return True if connect succesfully
 */
bool Pulse::conn(QString port){
    if(serial!=nullptr && serial->isOpen()) serial->close();
    if(serial!=nullptr) delete serial;
    serial = new QSerialPort(this);
    serial->setPortName(port);
    serial->setBaudRate(QSerialPort::Baud115200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    if (!serial->open(QIODevice::ReadWrite)) {
        serial->close();
        serial->setPortName("");
        delete serial;
        serial=nullptr;
        mutex.unlock();
        emit error(DeviceException(PulseError::portCom));
        emit connectDevice(false);
        return false;
    }
    serial->setDataTerminalReady(true);
    serial->setRequestToSend(true);
    connect(serial, &QSerialPort::errorOccurred, this, &Pulse::portError);
    connect(serial, &QSerialPort::readyRead, this, &Pulse::readData);
    connect(this, &Pulse::write, this, &Pulse::writeData);
    return true;
}

/**
 * @brief Pulse::disconn Disconnect to the port COM
 */
void Pulse::disconn(){
    if(serial==nullptr) return;
    mutex.lock();
    statePort=disconnPulse;
    foreach(Task *task, lTask)
        if(task!=nullptr) delete task;
    lTask.clear();
    if(serial->isOpen()){
        serial->close();
        serial->setPortName("");
    }
    disconnect(serial, &QSerialPort::errorOccurred, this, &Pulse::portError);
    disconnect(serial, &QSerialPort::readyRead, this, &Pulse::readData);
    disconnect(this, &Pulse::write, this, &Pulse::writeData);
    delete serial;
    serial=nullptr;
    c_infoDevice::delInstance();
    mutex.unlock();
    emit connectDevice(false);
}

/**
 * @brief Pulse::getPort Get the name port
 * @return Name of the Port connected
 */
QString Pulse::getPort(){
    if(serial==nullptr) return "";
    return serial->portName();
}

/**
 * @brief Pulse::listPorts Obtain a list of the Ports COM
 * @return List of Strings with Ports COM
 */
QStringList Pulse::listPorts(){
    QList<QSerialPortInfo> list=QSerialPortInfo::availablePorts();
    QStringList endData=QStringList();

    for(QSerialPortInfo& l : list){
        if(l.vendorIdentifier()!=9025) continue;
        endData.append(l.portName());
    }
    return endData;
}

/**
 * @brief Pulse::addTask Add task to the list
 * @param task Task to add
 */
void Pulse::addTask(Task *task){
    if(task->wrongTask()){
        if(task->getType()==Task::taskToStr(Task::File)){
            emit error(DeviceException(PulseError::FileNotExist));
            emit deleteTask(lTask.size());
        }
        return;
    }
    mutex.lock();
    lTask.push_back(task);
    mutex.unlock();
    if (!isRunning()) start();
}


/**
 * @brief Pulse::removeTask Delete task in the index
 * @param index Delete task in the position list
 */
void Pulse::removeTask(int index){
    mutex.lock();
    if(index==0 && statePort!=connPulse){
        deleteActTask=true;
        mutex.unlock();
        return;
    }
    emit deleteTask(index);
    delete lTask[index];
    lTask.remove(index);
    mutex.unlock();
}

/**
 * @brief Pulse::repeatTask Change type repeat task
 * @param index Index of the task to modify
 * @param repeat True if repeat task
 */
void Pulse::repeatTask(int index, bool repeat){
    if(lTask[index]!=nullptr) lTask[index]->setRepeat(repeat);
}

/**
 * @brief Pulse::portError Callback error in the QSerialPort
 * @param exception Type exception send
 */
void Pulse::portError(QSerialPort::SerialPortError exception){
    Q_UNUSED(exception)

    mutex.lock();
    statePort=disconnPulse;
    foreach(Task *task, lTask)
        if(task!=nullptr) delete task;
    lTask.clear();
    if(serial->isOpen()){
        serial->close();
        serial->setPortName("");
    }
    disconnect(serial, &QSerialPort::errorOccurred, this, &Pulse::portError);
    disconnect(serial, &QSerialPort::readyRead, this, &Pulse::readData);
    disconnect(this, &Pulse::write, this, &Pulse::writeData);
    c_infoDevice::delInstance();
    mutex.unlock();
    emit connectDevice(false);
    emit error(DeviceException(PulseError::portCom));

    delete serial;
    serial=nullptr;
}

/**
 * @brief Pulse::readData Callback receiving the data
 */
void Pulse::readData(){
    while (serial!=nullptr && serial->bytesAvailable()>0){
        char data=serial->read(1).front();
        #ifdef QT_DEBUG
        qDebug()<<data;
        #endif
        if (data=='R') {
            if(statePort==connPulse && !waitRetired) emit waiting(false);

            mutex.lock();
            //Device Ready
            statePort=readyTask;
            if(waitRetired){
                statePort=connPulse;
                waitRetired=false;
                emit waiting(true);
            }
            if(preEnd){
                emit deleteTask(0);
                preEnd=false;
                delete lTask[0];
                lTask.remove(0);
            }
            mutex.unlock();
            if (!isRunning()) start();
        } else if (data=='E') {
            //Device Error
            mutex.lock();

            QStringList err = getToSymb(';').split(',');
            #ifdef QT_DEBUG
            qDebug()<<err;
            #endif

            int nErr=err.at(0).toInt();
            PulseError dError=intToError(nErr);

            if(lTask.size()>0 && lTask[0]->isBlue()){
                if(dError==PulseError::connectionBlue){
                    #ifdef QT_DEBUG
                    qDebug()<<"ConnectionBlue";
                    #endif
                    lTask[0]->changeBlue(false);
                    waitRetired=(statePort!=connPulse);
                    statePort=connPulse;
                    mutex.unlock();
                    emit error(DeviceException(dError));
                    return;
                } else if(dError==PulseError::proximity){
                    #ifdef QT_DEBUG
                    qDebug()<<"Proximity";
                    #endif
                    lTask[0]->resetBlueProximity();
                    statePort=connPulse;
                    mutex.unlock();
                    return;
                }
            }

            if(lTask.size()>0) lTask[0]->resetPosition();
            statePort=connPulse;

            mutex.unlock();
            if (dError==PulseError::hardware || dError==PulseError::power) disconn();
            emit error(DeviceException(dError));

         } else if (data=='I'){
            //Recive Information of device
            mutex.lock();
            statePort=connPulse;

            //Get Info
            QStringList inf = getToSymb(';').split(',');

            //Save info to the device
            c_infoDevice *device=c_infoDevice::getInstance();
            for(int i=0; i<3; i++) device->sol[i]=inf.at(i).toInt();
            device->buffPulse=inf.at(3).toInt();
            device->mac=inf.at(4);
            device->buffBlue=inf.at(5).toInt();
            device->generateRS();
            mutex.unlock();

            emit connectDevice(true);
        } else if (data=='C') {
            //Device connected
            mutex.lock();
            statePort=readyTask;
            lTask[0]->changeBlue(true);
            mutex.unlock();
            if (!isRunning()) start();
        }
    }
}

/**
 * @brief Pulse::writeData Write all data send for COM port
 * @param data Data to send
 */
void Pulse::writeData(QByteArray *data){
    #ifdef QT_DEBUG
    qDebug()<<"Output:"<<data->toHex();
    #endif
    serial->write(*data, data->size());
    serial->waitForBytesWritten(-1);
    writeA=false;
}

/**
 * @brief Pulse::~Pulse Delete object disconnect QSerialPort
 */
Pulse::~Pulse(){
    disconn();
}

/**
 * @brief Pulse::run The starting point for the thread
 */
void Pulse::run(){
    if(lTask.size()==0) return;
    mutex.lock();
    if (statePort==readyTask){
        QByteArray data;
        if(deleteActTask){
            if(lTask[0]->forceEnd(&data)){
                emit deleteTask(0);
                delete lTask[0];
                lTask.remove(0);
                deleteActTask = false;
            }
        } else {
            lTask[0]->getBytes(0, &data);
            StateTask sTask = lTask[0]->getState();
            if (sTask==StateTask::endTask) preEnd=true;
            else if(sTask==StateTask::waitQuitPhone) waitRetired=true;
        }
        statePort=waitTask;

        writeA=true;
        emit write(&data);
        while(writeA) msleep(20);
    }
    mutex.unlock();
}

/**
 * @brief Pulse::getToSymb Read to Symbol in the serial port
 * @param symb Symbol to read
 * @return String of the serial port
 */
QString Pulse::getToSymb(char symb){
    QString text = "";
    QByteArray infoRead = serial->read(1);
    while(infoRead[0] != symb){
        text += infoRead[0];
        if(serial->bytesAvailable()>0) infoRead = serial->read(1);
        else break;
    }
    return text;
}

/**
 * @brief Pulse::intToError Convert int number in a Error
 * @param error Number of error
 * @return Enum of different Errors
 */
PulseError Pulse::intToError(int error){
    switch(error) {
    case 1:
    case 2:
    case 3:
        return PulseError::hardware;
    case 4: return PulseError::power;
    case 5: return PulseError::proximity;
    case 6: return PulseError::move;
    case 7: return PulseError::connectionBlue;
    case 8: return PulseError::transferBlue;
    case 9: return PulseError::unknownBlue;
    default: return PulseError::unknown;
    }
}
