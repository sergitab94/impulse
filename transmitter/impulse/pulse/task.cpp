/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "task.h"
#include <QRandomGenerator>
#include "deviceexception.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

/**
 * @brief Task::~Task Destroy next layers
 */
Task::~Task(){
    delete application;
    if(app_blue!=nullptr) delete app_blue;
}

/**
 * @brief Task::setFile Task type File
 * @param file File to send
 * @param repeat The task will be repeat
 * @param blue The task will be send for bluetooth
 */
void Task::setFile(QFile *file, bool repeat, bool blue){
    type=TypeTask::File;
    this->repeat=repeat;

    if(!file->isOpen()) return;

    nextLayer(blue);
    if(blue) app_blue->setFile(file, blue);
    else application->setFile(file, blue);
}

/**
 * @brief Task::setMessage Task type Message
 * @param message Message to send
 * @param repeat The task will be repeat
 * @param blue The task will be send for bluetooth
 */
void Task::setMessage(QString message, bool repeat, bool blue){
    type=TypeTask::Message;
    this->repeat=repeat;

    nextLayer(blue);
    if(blue) app_blue->setMessage(message, blue);
    else application->setMessage(message, blue);
}

/**
 * @brief Task::setWifi Task type Wifi
 * @param name SSID of the Wifi
 * @param sec Type of security
 * @param hidden True if the network is hidden
 * @param password Pre-shared key of the Wifi
 * @param repeat The task will be repeat
 * @param blue The task will be send for bluetooth
 */
void Task::setWifi(QString name, TypeSec sec, bool hidden, QString password, bool repeat, bool blue){
    type=TypeTask::Wifi;
    this->repeat=repeat;

    nextLayer(blue);
    if(blue) app_blue->setWifi(name, sec, hidden, password, blue);
    else application->setWifi(name, sec, hidden, password, blue);
}

/**
 * @brief Task::setSms Task type SMS
 * @param phone Number to phone
 * @param message Message to send
 * @param repeat The task will be repeat
 * @param blue The task will be send for bluetooth
 */
void Task::setSms(QString phone, QString message, bool repeat, bool blue){
    type=TypeTask::Sms;
    this->repeat=repeat;

    nextLayer(blue);
    if(blue) app_blue->setSms(phone, message, blue);
    else application->setSms(phone, message, blue);
}

/**
 * @brief Task::setEmail Task type Email
 * @param to Main recipient of the mail
 * @param cc Carbon Copy of the mail
 * @param subject Subject of the mail
 * @param bcc Blind Carbon Copy of the mail
 * @param message Body of the mail
 * @param repeat The task will be repeat
 * @param blue The task will be send for bluetooth
 */
void Task::setEmail(QString to, QString cc, QString subject, QString bcc, QString message, bool repeat, bool blue){
    type=TypeTask::Email;
    this->repeat=repeat;

    nextLayer(blue);
    if(blue) app_blue->setEmail(to, cc, subject, bcc, message, blue);
    else application->setEmail(to, cc, subject, bcc, message, blue);
}

/**
 * @brief Task::setBitcoin Task type Bitcoin
 * @param address Wallet to send Bitcoins
 * @param amount Total number of Bitcoin
 * @param message Subject of the transaction
 * @param repeat The task will be repeat
 * @param blue The task will be send for bluetooth
 */
void Task::setBitcoin(QString address, double amount, QString message, bool repeat, bool blue){
    type=TypeTask::Bitcoin;
    this->repeat=repeat;

    nextLayer(blue);
    if(blue) app_blue->setBitcoin(address, amount, message, blue);
    else application->setBitcoin(address, amount, message, blue);
}

/**
 * @brief Task::wrongTask State of the task
 * @return True if not initialized successfully
 */
bool Task::wrongTask(){
    return (application==nullptr);
}

/**
 * @brief Task::taskToStr Convert a type of task in a String for humans
 * @param type Type of task
 * @return String for humans
 */
QString Task::taskToStr(TypeTask type){
    switch (type) {
        case TypeTask::File: return tr("File");
        case TypeTask::Message: return tr("Message");
        case TypeTask::Wifi: return tr("Wi-Fi");
        case TypeTask::Sms: return tr("SMS");
        case TypeTask::Email: return tr("E-mail");
        case TypeTask::Bitcoin: return tr("Bitcoin");
        default: return "";
    }
}

/**
 * @brief Task::getBytes Get the next bytes of data
 * @param task Number of task in the list
 * @param data Bytes to send
 */
void Task::getBytes(int task, QByteArray *data){
    if (application==nullptr) return data->clear();
    float prg;

    if(endedBlue){
        data->append("D");
        endedBlue=false;
        return;
    }

    if(emitBlue){
        prg=app_blue->getBytes(data);
        if (prg>=0 && prg<=100) emit progress(task, prg);
        data->prepend(QStringLiteral("B%1,").arg(data->size()).toUtf8());
        endedBlue=app_blue->getDone();
        return;
    }

    if(app_blue!=nullptr && startBlue) {
        startBlue=false;
        data->append(QStringLiteral("U%1;").arg(pin, 6, 10, QLatin1Char('0')).toUtf8());
        #ifdef QT_DEBUG
        qDebug()<<"Password:"<<QStringLiteral("%1").arg(pin, 6, 10, QLatin1Char('0')).toUtf8();
        #endif
        return;
    }

    prg=application->getBytes(data);
    if (prg>=0 && prg<=100 && app_blue==nullptr) emit progress(task, prg);
    data->prepend(QStringLiteral("S%1,").arg(data->size()).toUtf8());
    if(app_blue!=nullptr && application->getDone()) application->resetPosition();
}

/**
 * @brief Task::resetPosition The task is reset
 */
void Task::resetPosition(){
    startBlue=true;
    emitBlue=endedBlue=false;
    if(application==nullptr) return;
    if(app_blue==nullptr) application->resetPosition();
    else{
        resetBlue();
        app_blue->resetPosition();
    }
}

/**
 * @brief Task::getState Get the state of the task
 * @return State of the task
 */
Task::StateTask Task::getState(){
    if(application==nullptr) return endTask;
    if(app_blue!=nullptr){
        if(repeat && app_blue->getDone() && !endedBlue){
            startBlue=true;
            emitBlue=endedBlue=false;
            resetBlue();
            app_blue->resetPosition();
            return waitQuitPhone;
        }
        if(app_blue->getDone() && !endedBlue) {
            delete application;
            delete app_blue;
            application=nullptr;
            app_blue=nullptr;
            return endTask;
        }
        return waitSend;
    }

    if(repeat && application->getDone()){
        application->resetPosition();
        return waitQuitPhone;
    }
    if(application->getDone()) {
        delete application;
        application=nullptr;
        return endTask;
    }
    return waitSend;    
}

/**
 * @brief Task::forceEnd The task is forced to end
 * @param data Data to send for end task
 * @return True if all data was sent to finish the task
 */
bool Task::forceEnd(QByteArray *data){
    if (application==nullptr) {
        data->clear();
        return true;
    }
    if(app_blue==nullptr){
        bool endInfo=application->forceEnd(data);
        data->prepend(QStringLiteral("S%1,").arg(data->size()).toUtf8());
        return endInfo;
    }
    data->append("D");
    return true;
}

/**
 * @brief Task::isBlue Check if the task send from bluetooth
 * @return True if send from bluetooth
 */
bool Task::isBlue(){
    return app_blue!=nullptr;
}

/**
 * @brief Task::changeBlue The task is changed to type Bluetooth
 * @param state True if changed to bluetooth
 */
void Task::changeBlue(bool state){
    emitBlue=state;
    if(!state) {
        startBlue=true;
        emitBlue=endedBlue=false;
        resetBlue();
    }
}

/**
 * @brief Task::resetBlueProximity Reset information to connection bluetooth
 */
void Task::resetBlueProximity(){
    application->resetPosition();
}

/**
 * @brief Task::nextLayer Initiazlize the next layer when known the task
 * @param blue The task will be send for bluetooth
 */
void Task::nextLayer(bool blue){
    if(application!=nullptr) delete application;
    if(app_blue!=nullptr) delete app_blue;
    application=new a_Application();
    app_blue=nullptr;
    if(blue) {
        resetBlue();
        app_blue=new a_Application();
    }
    startBlue=true;
    emitBlue=endedBlue=false;
}

/**
 * @brief Task::resetBlue Failed connection bluetooth reset connection
 */
void Task::resetBlue(){
    c_infoDevice *device=c_infoDevice::getInstance();
    QByteArray mac = QByteArray::fromHex(device->mac.toLocal8Bit());
    pin = QRandomGenerator::global()->bounded(1, 999999);
    application->setBluetooth(mac, pin);
}
