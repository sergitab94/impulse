/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "bb_blue.h"

#include "c_infodevice.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

/**
 * @brief bb_Blue::bb_Blue Constructor of the class
 * @param data Bytes to send
 * @param file File to send (optional)
 */
bb_Blue::bb_Blue(QByteArray data, QFile *file) {
    #ifdef QT_DEBUG
    qDebug()<<"Bluetooth:"<<data.toHex();
    #endif
    this->data=data;
    this->file=file;
    posData=this->data.begin();
    c_infoDevice *device = c_infoDevice::getInstance();
    length=device->buffBlue;

    sizeDo=0;
    buffProcess=0;
    sizeT = uint32_t(this->data.size());
    this->file=nullptr;
    if(file!=nullptr){
        this->file = file;
        sizeT += uint32_t(this->file->size());
    }
}

/**
 * @brief bb_Blue::~bb_Blue Destruct the object
 */
bb_Blue::~bb_Blue(){
    if(file==nullptr) return;
    file->close();
    file->flush();
    delete file;
}

/**
 * @brief bb_Blue::getBytes Obtain the bytes to send
 * @param output Data to send
 * @return Progress to task
 */
float bb_Blue::getBytes(QByteArray *output){    
    if(sizeDo==sizeT) return 100;

    for(uint i=0; i<length; i++){
        uint8_t bData;
        if(posData != data.end()) bData=*(posData++);
        else if(file!=nullptr && !file->atEnd()) file->read((char*) &bData, 1);
        else break;
        output->append(bData);
        sizeDo++;
    }

    //Calculate progress
    return (float(sizeDo)/float(sizeT))*100.0;
}

/**
 * @brief bb_Blue::resetPosition Reset this layer
 */
void bb_Blue::resetPosition(){
    posData=data.begin();
    if(file!=nullptr) file->seek(0);
    sizeDo=0;
    buffProcess=0;
}

/**
 * @brief bb_Blue::getDone Check if task is done
 * @return True if the task is done
 */
bool bb_Blue::getDone(){
    return sizeDo==sizeT;
}
