/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "a_application.h"
#include "CRC/crc.h"

#include <QFileInfo>
#include <QRegularExpression>

#ifdef QT_DEBUG
#include <QDebug>
#endif

/**
 * @brief a_Application::~a_Application Destruct layer
 */
a_Application::~a_Application(){
    if(datalink!=nullptr) delete datalink;
    else delete blue;
}

/**
 * @brief a_Application::setBluetooth Set type of data Bluetooth
 * @param mac Address physical address
 * @param pin Password only numeric
 */
void a_Application::setBluetooth(QByteArray mac, uint32_t pin){
    QByteArray data;

    //Add PIN
    for(int i=0; i<3*8; i+=8)
        data.prepend((pin>>i) & 0xFF);

    //MAC address and Type payload
    data.prepend(mac);
    data.prepend((char) 0x00);

    //CRC
    data.prepend(crc::calculate(data));

    //Create next layer
    if (datalink!=nullptr) delete datalink;
    if (this->blue!=nullptr) delete blue;
    datalink=new ba_DataLink(data);
    blue=nullptr;
}

/**
 * @brief a_Application::setFile Set type of data File
 * @param file File to send
 * @param blue Next layer. True if bluetooth. False if datalink.
 */
void a_Application::setFile(QFile *file, bool blue){
    QFileInfo fileInfo(file->fileName());
    QByteArray data;

    //Generate Payload
    data.append(fileInfo.fileName().toUtf8());
    data.append(char(0x00));

    //Header Application and create dataLink
    generateHeader(0x01, blue, data, file);
}

/**
 * @brief a_Application::setMessage Set type of data Message
 * @param message Messsage to send
 * @param blue Next layer. True if bluetooth. False if datalink.
 */
void a_Application::setMessage(QString message, bool blue){
    QByteArray data;

    //Generate Payload
    data.append(message.toUtf8());

    //Header Application and create dataLink
    generateHeader(0x02, blue, data);
}

/**
 * @brief a_Application::setWifi Set type of data Wifi
 * @param name Name of the network SSID
 * @param sec Type of security
 * @param hidden True if network is hidden
 * @param password Password of the network
 * @param blue Next layer. True if bluetooth. False if datalink.
 */
void a_Application::setWifi(QString name, TypeSec sec, bool hidden, QString password, bool blue){
    QByteArray data;

    //Generate Payload
    uint8_t typeConnection = 0x00;
    if(hidden) typeConnection = 0x80;

    if(sec==TypeSec::OPEN) typeConnection |= 0x00;
    else if(sec==TypeSec::WPA2) typeConnection |= 0x01;
    else if(sec==TypeSec::WPA3) typeConnection |= 0x02;

    data.append(typeConnection);
    data.append(name.toUtf8());
    data.append((char) 0x00);
    data.append(password.toUtf8());

    //Header Application and create dataLink
    generateHeader(0x03, blue, data);
}

/**
 * @brief a_Application::setSms Set type of data Sms
 * @param phone Number of the phone to send
 * @param message Message to send
 * @param blue Next layer. True if bluetooth. False if datalink.
 */
void a_Application::setSms(QString phone, QString message, bool blue){
    QByteArray data;

    //Generate Payload
    data.append(phone.toUtf8());
    data.append((char) 0x00);
    data.append(message.toUtf8());

    //Header Application and create dataLink
    generateHeader(0x04, blue, data);
}

/**
 * @brief a_Application::setEmail Set type of data Email
 * @param to Email to send
 * @param cc Carbon copy to secondary recipients
 * @param subject Subject of email
 * @param bcc Blind carbon copy to tertiary recipients
 * @param message Message to send
 * @param blue Next layer. True if bluetooth. False if datalink.
 */
void a_Application::setEmail(QString to, QString cc, QString subject, QString bcc, QString message, bool blue){
    QByteArray data;

    //Generate Payload
    data.append(to.toUtf8());
    data.append((char) 0x00);
    data.append(cc.toUtf8());
    data.append((char) 0x00);
    data.append(bcc.toUtf8());
    data.append((char) 0x00);
    data.append(subject.toUtf8());
    data.append((char) 0x00);
    data.append(message.toUtf8());

    //Header Application and create dataLink
    generateHeader(0x05, blue, data);
}

/**
 * @brief a_Application::setBitcoin Set type of data Bitcoin
 * @param address Address wallet to send
 * @param amount Total amount to send in BTC
 * @param message Aditional message to send
 * @param blue Next layer. True if bluetooth. False if datalink.
 */
void a_Application::setBitcoin(QString address, double amount, QString message, bool blue){
    QByteArray data;

    //Generate Payload
    data.append(address.toUtf8());
    data.append((char) 0x00);
    data.append(message.toUtf8());
    data.append((char) 0x00);
    data.append(QString::number(amount, 'f', 20).remove(QRegularExpression("[0]*$")).toUtf8());

    //Header Application and create dataLink
    generateHeader(0x06, blue, data);
}

/**
 * @brief a_Application::getBytes Get next Bytes to send
 * @param data Pointer of Array bytes to send
 * @return Total progress
 */
float a_Application::getBytes(QByteArray *data){
    if(datalink!=nullptr) return datalink->getBytes(data);
    return blue->getBytes(data);
}

/**
 * @brief a_Application::resetPosition Reset position to start
 */
void a_Application::resetPosition(){
    if(datalink!=nullptr) return datalink->resetPosition();
    blue->resetPosition();
}

/**
 * @brief a_Application::getDone Check if task is done
 * @return True if it is done
 */
bool a_Application::getDone(){
    if(datalink!=nullptr) return datalink->getDone();
    return blue->getDone();
}

/**
 * @brief a_Application::forceEnd The task is deleted and it will notify to phone
 * @param data Pointer of Array bytes to send
 * @return True if successfully delete
 */
bool a_Application::forceEnd(QByteArray *data){
    if(datalink!=nullptr) return datalink->forceEnd(data);
    return true;
}

/**
 * @brief a_Application::generateHeader Generate neccesary header and next layer
 * @param typeFrame Byte with the number of type application data
 * @param blue True if next layer is bluetooth, false if next layer is datalink
 * @param data Bytearray to send
 * @param file File to send (not required)
 */
void a_Application::generateHeader(uint8_t typeFrame, bool blue, QByteArray data, QFile *file){
    uint size=data.size();
    QByteArray header;

    //CRC Payload
    header.prepend(crc::calculate(data, file));

    //Size payload
    if (file!=nullptr) size += file->size();
    for(int i=0; i<4*8; i+=8)
        header.prepend((size>>i) & 0xFF);

    //Type Frame
    header.prepend(typeFrame);

    //CRC Header (Size + Type + CRC payload)
    header.prepend(crc::calculate(header));

    //Add headers
    data.prepend(header);

    //Create next layer
    if (datalink!=nullptr) delete datalink;
    if (this->blue!=nullptr) delete this->blue;
    datalink=nullptr;
    this->blue=nullptr;
    if(file==nullptr){
        if(blue) this->blue= new bb_Blue(data);
        else datalink = new ba_DataLink(data);
    }else{
        if(blue) this->blue= new bb_Blue(data, file);
        else datalink = new ba_DataLink(data, file);
    }

    #ifdef QT_DEBUG
    //Print output mode debug
    QString output=data.toHex();
    if(file!=nullptr){
        while(!file->atEnd()){
            uint8_t data;
            file->read((char*) &data, 1);
            output+=QStringLiteral("%1").arg(data, 2, 16, QLatin1Char('0'));
        }
        file->seek(0);
    }
    qDebug()<<"Application:"<<output;
    #endif
}
