/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef PULSE_H
#define PULSE_H

#include <QObject>
#include <QFile>
#include <QMutex>
#include <QThread>
#include <QProgressBar>
#include <QWaitCondition>
#include <QSerialPort>

#include "deviceexception.h"
#include "task.h"

typedef DeviceException::PulseError PulseError;
typedef Task::StateTask StateTask;

/**
 * @brief The Pulse class connect to port COM and send all information generate
 * in other layers
 */
class Pulse : public QThread{
    Q_OBJECT

public:
    static Pulse* getInstance();
    static void delInstance();

    bool conn(QString con);
    void disconn();

    QString getPort();
    QStringList listPorts();
    void addTask(Task *task);
    void removeTask(int index);
    void repeatTask(int index, bool repeat);

signals:
    void connectDevice(bool connect);
    void waiting(bool wait);
    void error(DeviceException error);
    void deleteTask(int task);
    void write(QByteArray *data);

private slots:
    void portError(QSerialPort::SerialPortError exception);
    void readData();
    void writeData(QByteArray *data);

private:
    /**
     * @brief The StatePort enum
     * @enum connPulse The device is connect but not ready to send (with Errors)
     * @enum disconnPulse The device is disconnected
     * @enum readyTask The device is connect and ready to send
     * @enum waitTask The device is send a Task
     */
    enum StatePort{
        connPulse,
        disconnPulse,
        readyTask,
        waitTask
    };

    //Instance Facade
    static Pulse* instance;

    //Control thread
    QMutex mutex;

    //Serial Port and state of Serial Port
    StatePort statePort=disconnPulse;
    bool deleteActTask=false, writeA=false;
    QSerialPort *serial=nullptr;

    //Waiting phone retired
    bool waitRetired=false;

    //Task and actualPosition
    QVector<Task*> lTask;
    bool preEnd=false;

    explicit Pulse(QObject *parent=nullptr) : QThread(parent){}
    ~Pulse();
    void run() override;

    QString getToSymb(char symb);
    PulseError intToError(int error);
};

#endif // PULSE_H
