/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef C_INFODEVICE_H
#define C_INFODEVICE_H

#include <QObject>
#include "RS/listrs.h"

/**
 * @brief The c_infoDevice class contain all information
 * of the device
 */
class c_infoDevice : public QObject{
    Q_OBJECT
public:
    //MAC address bluetooth
    QString mac;

    //Size solenoid (Z, Y, X), sizeBuff
    uint sol[3], buffPulse, buffBlue;

    void generateRS();

    static c_infoDevice* getInstance();
    static void delInstance();

private:
    static c_infoDevice* instance;
};

#endif // C_INFODEVICE_H
