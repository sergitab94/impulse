/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef BA_DATALINK_H
#define BA_DATALINK_H

#include <QObject>
#include <QFile>

#include "c_infodevice.h"
#include "RS/listrs.h"

typedef listRS::infSol infSol;

/**
 * @brief The ba_DataLink class is a layer to
 * prepare to send data to solenoids
 */
class ba_DataLink : public QObject{
    Q_OBJECT
public:
    ba_DataLink(QByteArray data, QFile *file=nullptr);
    ~ba_DataLink();
    void resetPosition();
    bool getDone();
    float getBytes(QByteArray *output);
    bool forceEnd(QByteArray *output);

private:
    const int sizeFrame=50*3;   //Byte size frame (multiplied by 3)
    int bytesWait;              //Number of bytes to wait
    bool emptyBuffer;           //Check if empty Buffer;
    int sizeBuff;               //Device size Buffer Pulse
    double sizeT;               //Size total of data
    int bitRight=8;             //Position bits pending
    bool isEnd=false;           //True if you have finished sending
    float progress;             //Progress state

    //Calibrate
    QByteArray::iterator posInit=nullptr;
    QByteArray initBytes;

    //Data and iterator
    QByteArray::iterator posData=nullptr;
    QByteArray data;

    //File
    QFile *file=nullptr;

    //Frame data
    QByteArray::iterator posFrame=nullptr;
    QByteArray frame;

    //Error Frame
    QByteArray::iterator posError=nullptr;
    QByteArray error;

    //Reserve Data (from other frames)
    QVector<QByteArray> reserve;

    //Device data info
    QList<infSol> oSol;

    //Delete Frame Prepare
    bool frameDeletePrepare=false;

    void createCalibrate(c_infoDevice *dev);
    void createError(infSol iSol);
    float createFrame();
    void getByteArray(int length, int m, QByteArray *output);
    QByteArray::iterator copyByteSol(QByteArray *input, uint *solPr, int sol, QByteArray *output);
    qint8 grayToInt(qint8 input);
    void orderPulses(QList<infSol> *list);
};

#endif // BA_DATALINK_H
