/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QFile>
#include <QProgressBar>

#include "a_application.h"

class Task : public QObject {
    Q_OBJECT

public:
    enum TypeTask {
        File  = 1,
        Message = 2,
        Wifi = 3,
        Sms = 4,
        Email = 5,
        Bitcoin = 6
    };
    Q_ENUM(TypeTask)

    enum StateTask {
        waitSend,
        waitQuitPhone,
        endTask
    };
    Q_ENUM(StateTask)

    typedef a_Application::TypeSec TypeSec;

    Task(QObject *parent=nullptr):QObject(parent){};
    ~Task();
    void setFile(QFile *file, bool repeat, bool blue);
    void setMessage(QString message, bool repeat, bool blue);
    void setWifi(QString name, TypeSec sec, bool hidden, QString password, bool repeat, bool blue);
    void setSms(QString phone, QString message, bool repeat, bool blue);
    void setEmail(QString to, QString cc, QString subject, QString bcc, QString Message, bool repeat, bool blue);
    void setBitcoin(QString address, double amount, QString message, bool repeat, bool blue);

    bool getRepeat(){return repeat;}
    void setRepeat(bool repeat){this->repeat=repeat;}
    QString getType(){return taskToStr(type);}
    bool wrongTask();
    static QString taskToStr(TypeTask type);

    void getBytes(int task, QByteArray *data);
    void resetPosition();
    StateTask getState();
    bool forceEnd(QByteArray *data);

    //Bluetooth
    bool isBlue();
    void changeBlue(bool state);
    void resetBlueProximity();

signals:
    void progress(const int index, const float progress);

private:
    //Layer initial
    a_Application *application=nullptr;
    a_Application *app_blue=nullptr;

    //Task Characteristics
    TypeTask type;
    bool repeat;

    //Ended main application
    bool startBlue, emitBlue, endedBlue;

    //Pin 6 digits random
    uint32_t pin;

    void nextLayer(bool blue);
    void resetBlue();
};

#endif // TASK_H
