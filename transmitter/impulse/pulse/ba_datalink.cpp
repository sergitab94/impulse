/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "ba_datalink.h"

#include <QtMath>
#include "deviceexception.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

/**
 * @brief ba_DataLink::ba_DataLink
 * @param data ByteArray of data to send
 * @param file File to send (optional)
 */
ba_DataLink::ba_DataLink(QByteArray data, QFile *file){
    c_infoDevice *dev = c_infoDevice::getInstance();
    sizeBuff=dev->buffPulse;

    //Size in Bytes
    this->data=data;
    posData = this->data.begin();
    sizeT = this->data.size();

    if(file!=nullptr){
        this->file = file;
        sizeT += this->file->size();
    }

    //Get list Pulses, generate error and order
    listRS::getInfo(&oSol);
    createError(oSol[0]);
    orderPulses(&oSol);

    //Ending recover
    bytesWait = sizeBuff/2;
    emptyBuffer=false;

    //Create calibrate and error
    createCalibrate(dev);
}

/**
 * @brief ba_DataLink::~ba_DataLink Destruct the object
 */
ba_DataLink::~ba_DataLink(){
    if(file==nullptr) return;
    file->close();
    file->flush();
    delete file;
}

/**
 * @brief ba_DataLink::getDone Check if the task is done
 * @return
 */
bool ba_DataLink::getDone(){
    return emptyBuffer;
}

/**
 * @brief ba_DataLink::resetPosition The task reset the position
 */
void ba_DataLink::resetPosition(){
    posInit = initBytes.begin();
    posData = data.begin();
    if(file!=nullptr) file->seek(0);
    bitRight = 8;
    posFrame = nullptr;
    isEnd = false;
    emptyBuffer = false;
    reserve.clear();
}

/**
 * @brief ba_DataLink::getBytes The next bytes to send to device
 * @param output Pointer with the data to send
 * @return Progress of the task
 */
float ba_DataLink::getBytes(QByteArray *output){
    output->clear();

    //Add calibrate
    if(posInit<initBytes.end()){
        for(int i=0; i<sizeBuff; i++) output->append(*(posInit++));
        return 0;
    }

    //Wait to empty buffer to device
    if(isEnd && posFrame==frame.end()){
        output->fill(0x00, bytesWait);
        emptyBuffer=true;
        return 100;
    }

    if(posFrame==nullptr) progress=createFrame();

    //Add Frame
    for(int i=0; i<sizeBuff; i++) {
        output->append(*(posFrame++));
        if(posFrame==frame.end()) {
            if(isEnd) break;
            progress=createFrame();
        }
    }

    return progress;
}

/**
 * @brief ba_DataLink::forceEnd The next bytes to send to device with force end
 * @param output Pointer with the data to send
 * @return True if the task is ended
 */
bool ba_DataLink::forceEnd(QByteArray *output){
    output->clear();
    int i=0;
    if(posInit!=initBytes.end()){
        for(int i=0; i<sizeBuff; i++) output->append(*(posInit++));
        return false;
    }

    if(posFrame!=nullptr && posFrame!=frame.end()){
        if(!frameDeletePrepare){
            infSol zDev=oSol[0];
            int itr, rmvPulses, sRsv=0;
            for(int x=0; x<oSol.size(); x++){
                zDev=oSol.at(x);
                if(!reserve.isEmpty()) sRsv=reserve.at(x).size();
                if(zDev.pos==0) break;
            }
            itr = posFrame-frame.begin();
            rmvPulses = (frame.size()/3)+sRsv;
            rmvPulses -= itr/3;
            rmvPulses %= zDev.sPulse;
            rmvPulses *= 3;
            rmvPulses += itr;
            frame.truncate(rmvPulses);
            posFrame = frame.begin()+itr;
            frameDeletePrepare=true;
        }
        for(; i<sizeBuff; i++) {
            output->append(*(posFrame++));
            if(posFrame>=frame.end()) break;
        }
    }

    for(; i<sizeBuff; i++) {
        output->append(*(posError++));
        if(posError==error.end())  return true;
    }

    return false;
}

/**
 * @brief ba_DataLink::createCalibrate Generate calibration of device
 * @param dev Device to generate
 */
void ba_DataLink::createCalibrate(c_infoDevice *dev){
    uint *sol=dev->sol;

    //Add first (z, y, x; 000)
    for(int i=0; i<3; i++) initBytes.append(char(0x00));

    if(sol[0]==1){
        //Signal Detect (100, 100, 000, 000) *3
        for(int i=0; i<3; i++) {
            initBytes.append(char(0x01));
            for(int z=0; z<2; z++) initBytes.append(char(0x00));
            initBytes.append(char(0x01));
            for(int z=0; z<8; z++) initBytes.append(char(0x00));
        }
    } else {
        //Signal Detect (Z00, Z00, 100, 100) *3
        for(int i=0; i<3; i++) {
            for(int x=0; x<2; x++){
                initBytes.append(qPow(2, sol[0])-1);
                for(int z=0; z<2; z++) initBytes.append(char(0x00));
            }

            for(int x=0; x<2; x++){
                initBytes.append(char(0x01));
                for(int z=0; z<2; z++) initBytes.append(char(0x00));
            }
        }
    }

    //For Solenoid
    for(int i=0; i<3; i++){
        //For Size
        for(unsigned int z=0; z<sol[i]; z++){
            if(sol[i]==1){
                //For Pulse (100, 100, 000, 000)
                initBytes.append(char(0x01));
                for(int z=0; z<2; z++) initBytes.append(char(0x00));
                initBytes.append(char(0x01));
                for(int z=0; z<8; z++) initBytes.append(char(0x00));
            } else {
                //For Pulse (ZYX, ZYX, 100, 100)
                for(int x=0; x<6; x++){
                    if(i==(x%3))initBytes.append(qPow(2, sol[i])-1);
                    else initBytes.append(char(0x00));
                }

                for(int x=0; x<6; x++){
                    if(i==(x%3))initBytes.append(char(0x01));
                    else initBytes.append(char(0x00));
                }
            }
        }
    }

    initBytes.chop(6); //Remove last bytes (100, 100) or (000, 000)
    for (int i=0; i<12; i++) initBytes.append(char(0x00));

    //Padding calibrate to buffers
    quint16 bytesSize = qCeil(qreal(initBytes.length())/sizeBuff)*sizeBuff;
    initBytes = initBytes.rightJustified(bytesSize, 0x00);
    posInit = initBytes.begin();
}

/**
 * @brief ba_DataLink::createError Generate error for the current device
 */
void ba_DataLink::createError(infSol iSol){
    error.clear();

    //Error pulse
    int maxPulse=grayToInt(qPow(2, (iSol.n*iSol.m)/iSol.sPulse)-1);
    int rsBit=(iSol.sPulse*iSol.k)/iSol.n;
    error.fill(0x00, (iSol.sPulse*3));

    for(int x=iSol.sPulse-1; x>=rsBit; x--){
        error.remove(3*x, 1);
        error.insert(3*x, maxPulse);
    }

    for (int i=0; i<3; i++) error.append(char(0x00));
    posError=error.begin();
}

/**
 * @brief ba_DataLink::createFrame Generate the frame to send
 * @return Progress of the frame
 */
float ba_DataLink::createFrame(){
    uint solPr[3] = {0, 1 , 2};
    double sizeDo;
    float progress;

    frame.clear();
    frame.fill(0x00, sizeFrame);

    for(int x=0; x<oSol.size(); x++){
        infSol i=oSol[x];
        if(i.sPulse==0) continue;

        //Add frame partials from other frame
        if(reserve.length()>0){
            QByteArray rsData=reserve.at(x);
            copyByteSol(&rsData, solPr, i.pos, &frame);
        }

        //Add frames
        int nFrames=qFloor((sizeFrame-solPr[i.pos]+i.pos)/(3.0*i.sPulse));
        for(int y=0; y<nFrames; y++){
            QByteArray rsData;
            getByteArray(i.k, i.m, &rsData);
            listRS::getData(&rsData, i.pos);
            copyByteSol(&rsData, solPr, i.pos, &frame);
            #ifdef QT_DEBUG
            qDebug().nospace()<<"Solenoid "<<i.pos<<": "<<rsData.toHex();
            #endif
        }
    }

    //Clean reserve
    reserve.clear();

    //Add current frames partials
    for(int x=0; x<oSol.size(); x++){
        infSol i=oSol[x];
        if(i.sPulse==0) continue;

        sizeDo=posData-data.begin();
        if (file!=nullptr) sizeDo+=file->pos();

        if((sizeDo/sizeT)>=1){
            reserve.append(QByteArray());
            continue;
        }

        QByteArray rsData;
        getByteArray(i.k, i.m, &rsData);
        listRS::getData(&rsData, i.pos);
        #ifdef QT_DEBUG
        qDebug().nospace()<<"Solenoid "<<i.pos<<": "<<rsData.toHex();
        #endif

        QByteArray::iterator it = copyByteSol(&rsData, solPr, i.pos, &frame);
        if(it>=rsData.end()) reserve.append(QByteArray());
        else reserve.append(QByteArray(rsData.mid(it-rsData.begin())));
    }

    //Calculate progress
    sizeDo=posData-data.begin();
    if (file!=nullptr) sizeDo+=file->pos();
    progress=(sizeDo/sizeT)*100.0;
    if(progress>=100) isEnd=true;

    //Check if reserve is empty
    for(int i=0; i<reserve.length(); i++){
        if(!reserve[i].isEmpty()){
            isEnd=false;
            break;
        }
    }

    posFrame=frame.begin();

    return progress;
}

/**
 * @brief ba_DataLink::getByteArray
 * @param length Number of bytes totals
 * @param m Maximum number to send for solenoid
 * @param output Data to send
 */
void ba_DataLink::getByteArray(int length, int m, QByteArray *output){
    output->clear();
    for(int i=0; i<length; i++){
        uint8_t out = 0x00;
        do {
            uint8_t mask = 0x00;
            uint8_t bData;
            if (bitRight>=0) {
                if(posData != data.end()) bData=*posData;
                else if(file!=nullptr && !file->atEnd()) {
                    file->read((char*) &bData, 1);
                    file->seek(file->pos()-1);
                } else bData=0x00;

                bitRight -= m;
                if (bitRight>0){
                    mask = (int(qPow(2, m))-1) << bitRight;
                    out |= (mask & bData) >> bitRight;
                } else {
                    mask = (int(qPow(2, m))-1) >> qAbs(bitRight);
                    out |= (mask & bData) << qAbs(bitRight);
                }
            } else {
                if(posData != data.end()) bData=*(++posData);
                else if(file!=nullptr && !file->atEnd()) file->read((char*) &bData, 1);
                else bData=0x00;

                bitRight += 8;
                mask = (int(qPow(2, m))-1) << bitRight;
                out |= (mask & bData) >> bitRight;
            }
        } while(bitRight<0);
        output->append(out);
    }
}

/**
 * @brief ba_DataLink::copyByteSol Copy bytes to size of solenoid
 * @param input ByteArray of data to send
 * @param solPr Pointer of solenoid in the frame
 * @param sol Position of the Solenoid (Z=0, Y=1, X=2)
 * @param output Bytes to send
 * @return Position of the iterator to continue
 */
QByteArray::iterator ba_DataLink::copyByteSol(QByteArray *input, uint *solPr, int sol, QByteArray *output){
    QByteArray::iterator it=input->begin();
    for(; (it<input->end()) && (solPr[sol]<=uint(sizeFrame+sol-3)); ++it, solPr[sol]+=3){
        output->remove(solPr[sol], 1);
        output->insert(solPr[sol], grayToInt(*it));
    }
    return it;
}

/**
 * @brief ba_DataLink::grayToInt Convert gray scale to integer number
 * @param input Gray input
 * @return Integer output
 */
qint8 ba_DataLink::grayToInt(qint8 input){
    int power = 1;
    while(true) {
        int j = input >> power;
        input ^= j;
        if (j==0) return qint8(input);
        power <<= 1;
    }
}

/**
 * @brief ba_DataLink::orderPulses Order list for size solenoids
 * @param list List to order all solenoids
 */
void ba_DataLink::orderPulses(QList<infSol> *list){
    QList<infSol> data(*list);
    list->clear();

    for(int x=0; x<data.size(); x++){
        int z=-1, res=0;
        for(int y=0; y<data.size(); y++){
            if(data[y].sPulse>=res && y!=z){
                z=y;
                res=data[y].sPulse;
            }
        }
        list->append(infSol{data[z].pos, data[z].m, data[z].n, data[z].k, data[z].sPulse});
        data[z].sPulse=-1;
    }
}
