/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "c_infodevice.h"

c_infoDevice* c_infoDevice::instance = nullptr;

/**
 * @brief c_infoDevice::generateRS It prepare Reed-Solomon
 */
void c_infoDevice::generateRS(){
    listRS::createRS(sol);
}

/**
 * @brief c_infoDevice::getInstance Obtain the unique instance of the Singeltone
 * @return Instance of the class
 */
c_infoDevice* c_infoDevice::getInstance(){
    if (instance == nullptr) instance = new c_infoDevice();
    return instance;
}

/**
 * @brief c_infoDevice::delInstance Destroy the unique instance of the Singeltone
 */
void c_infoDevice::delInstance(){
    listRS::deleteRS();
    if (instance != nullptr){
        delete instance;
        instance = nullptr;
    }
}
