/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef DEVICEEXCEPTION_H
#define DEVICEEXCEPTION_H

#include <QException>

/**
 * @brief The DeviceException class contain the differents types of
 * the exceptions
 */
class DeviceException : QException{
public:
    enum class PulseError {
        FileNotExist,
        portCom,
        unknown,
        hardware,       //1,2,3
        power,          //4
        proximity,      //5
        move,           //6
        connectionBlue, //7
        transferBlue,   //8
        unknownBlue     //9
    };

    DeviceException(PulseError error):lError(error){};
    PulseError getError(){return lError;};
    void raise() const override { throw *this; }
    DeviceException *clone() const override { return new DeviceException(*this); }

private:
    PulseError lError;
};

#endif // DEVICEEXCEPTION_H
