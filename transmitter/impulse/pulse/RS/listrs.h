/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef LISTRS_H
#define LISTRS_H

#include <QObject>
#include "rs.h"

/**
 * @brief The listRS class create a list of Reed-Solomon
 */
class listRS : public QObject{
    Q_OBJECT
public:
    struct infSol{
        int pos, m, n, k, sPulse;
    };

    //Z=0, Y=1, X=2
    static void createRS(uint *sol);
    static void deleteRS();

    static void getInfo(QList<infSol> *info);
    static void getData(QByteArray* data, uint sol);

private:
    static listRS* instance;
    RS* rs[3];

    listRS(unsigned int *sol);
    ~listRS();
};

#endif // LISTRS_H
