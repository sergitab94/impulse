/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef POLYGF_H
#define POLYGF_H

#include <QObject>
#include <QVector>

#include "gf.h"

/**
 * @brief The polyGF class create a polynomial in a Galois field
 */
class polyGF: public QObject{
    Q_OBJECT

public:
    polyGF(GF* field);
    polyGF(GF* field, qint32 coefficients);
    polyGF(GF* field, qint32 coefficients, quint32 degree);
    polyGF(GF* field, QVector<qint32>* coefficients);
    ~polyGF();

    QVector<qint32>* getCoefficients(){return &coefficients;};
    qint32 getCoefficients(qint32 degree){return coefficients[coefficients.size()-degree-1];};
    bool isZero(){return (coefficients.size()==1 && coefficients.at(0)==0);};
    int getDegree(){return coefficients.size()-1;};

    void incrementDegree(quint32 iDegree);

    polyGF* operator+(polyGF* b);
    polyGF* operator*(polyGF* b);
    polyGF* operator*(qint32 b);
    polyGF* operator/(polyGF* b);
    polyGF* operator%(polyGF* b);
    QVector<polyGF*> div(polyGF* b);

    QString print();
    static QString print(QVector<qint32> info);

private:
    GF* field;
    //First (0) is MSB and end is LSB (n)
    QVector<qint32> coefficients;

};

#endif // POLYGF_H
