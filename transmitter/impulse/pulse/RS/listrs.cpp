/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "listrs.h"

listRS* listRS::instance = nullptr;

/**
 * @brief listRS::createRS Generate Reed-Solomon of all solenoids
 * @param sol List of solenoid with size of bytes
 */
void listRS::createRS(uint *sol){
    if (instance == nullptr) instance = new listRS(sol);
}

/**
 * @brief listRS::deleteRS Destruct instance of list Reed-Solomon
 */
void listRS::deleteRS(){
    if (instance != nullptr){
        delete instance;
        instance=nullptr;
    }
}

/**
 * @brief listRS::getInfo Get list of information the solenoids
 * @param info List to export solenoid (Z, Y, X)
 */
void listRS::getInfo(QList<infSol> *info){
    for(int x=0; x<3; x++){
        if(instance->rs[x]==nullptr) {
            info->append(infSol{x, 0, 0, 0, 0});
        } else {
            RS::rsInfo data=instance->rs[x]->getInfo();
            info->append(infSol{x, data.m, data.n, data.k, data.sPulse});
        }
    }
}

/**
 * @brief listRS::getData Proccess Reed-Solomon of solenoid in distint position
 * @param data Data to proccess (redundancy output in this variable)
 * @param sol Position of solenoid (Z=0, Y=1, X=2)
 */
void listRS::getData(QByteArray *data, uint sol){
    if (instance->rs[sol]!=nullptr)
        instance->rs[sol]->get(data);
}

/**
 * @brief listRS::listRS Constructor the list of Reed-Solomon
 * @param res List of solenoid with size of bytes
 */
listRS::listRS(unsigned int *res){
    for(int i=0; i<3; i++){
        if(res[i]==0) rs[i]=nullptr;
        else rs[i]=new RS(res[i]);
    }
}

/**
 * @brief listRS::~listRS Destruct the list of Reed-Solomon
 */
listRS::~listRS(){
    for(int i=0; i<3; i++) delete instance->rs[i];
}
