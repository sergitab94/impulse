/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "polygf.h"

/**
 * @brief polyGF::polyGF Create polynomial void
 * @param field Galois field
 */
polyGF::polyGF(GF* field){
    this->field=field;
    this->coefficients=QVector<qint32>();
}

/**
 * @brief polyGF::polyGF Create polynomial with a coefficient
 * @param field Galois field
 * @param coefficients Coefficient of the polynomial
 */
polyGF::polyGF(GF* field, qint32 coefficients){
    this->field=field;
    this->coefficients=QVector<qint32>();
    this->coefficients.append(coefficients);
}

/**
 * @brief polyGF::polyGF Create a polynomial with a degree of the coefficient
 * @param field Galois field
 * @param coefficients Coefficient of the polynomial
 * @param degree Degree of the coefficient
 */
polyGF::polyGF(GF* field, qint32 coefficients, quint32 degree){
    this->field=field;
    this->coefficients=QVector<qint32>(degree+1, 0);
    this->coefficients[0]=coefficients;    
}

/**
 * @brief polyGF::polyGF Create polynomial with a list of coefficients
 * @param field Galois field
 * @param coefficients Coefficients of the polynomial
 */
polyGF::polyGF(GF* field, QVector<qint32>* coefficients){
    this->field=field;
    int length=coefficients->size();

    if(length>1 && coefficients->at(0)==0){
        int firstNonZero=1;
        while((firstNonZero<length) && (coefficients->at(firstNonZero)==0)) firstNonZero++;

        if(firstNonZero==length) this->coefficients={0};
        else this->coefficients=QVector<qint32>(coefficients->mid(firstNonZero));
    }else this->coefficients=QVector<qint32>(*coefficients);
}

/**
 * @brief polyGF::~polyGF Destruct of polynomial
 */
polyGF::~polyGF(){
    coefficients.clear();
}

/**
 * @brief polyGF::incrementDegree Increment degree of the polynomial
 * @param iDegree Number to increment to degree
 */
void polyGF::incrementDegree(quint32 iDegree){
    for(quint32 i=0; i<iDegree; i++) coefficients.append(0);
}

/**
 * @brief polyGF::operator + Add one polynomial to other
 * @param b Polynomial to add to current
 * @return Polynomial add
 */
polyGF* polyGF::operator+(polyGF* b){
    if(isZero()) return new polyGF(field, b->getCoefficients());
    if(b->isZero()) return new polyGF(field, &coefficients);

    QVector<qint32> small=QVector<qint32>(coefficients);
    QVector<qint32> large=QVector<qint32>(*(b->getCoefficients()));

    if(small.size()>large.size()){
        small=QVector<qint32>(*(b->getCoefficients()));
        large=QVector<qint32>(coefficients);
    }

    QVector<qint32> sumDiff=QVector<qint32>(large.size(), 0);
    int32_t lengthDiff=large.size()-small.size();

    for(int32_t i=0; i<lengthDiff; i++) sumDiff[i]=large[i];

    for(int32_t i=lengthDiff; i<large.size(); i++)
        sumDiff[i]=field->add(small[i-lengthDiff], large[i]);

    return new polyGF(field, &sumDiff);
}

/**
 * @brief polyGF::operator * Multiply one polynomial to other
 * @param b Polynomial to multiply to current
 * @return Polynomial multiply
 */
polyGF* polyGF::operator*(polyGF* b){
    if (isZero() || b->isZero()) return new polyGF(field, 0);

    int aLength = coefficients.size();
    int bLength = b->getCoefficients()->size();
    QVector<qint32>* bCoeff=b->getCoefficients();
    QVector<qint32> product=QVector<qint32>(aLength+bLength-1, 0);


    for (int i=0; i<aLength; i++)
        for (int j=0; j<bLength; j++)
            product.replace(i+j, field->add(product.at(i+j),
                         field->multiply(coefficients.at(i), bCoeff->at(j))));

    return new polyGF(field, &product);
}

/**
 * @brief polyGF::operator * Multiply one polynomial to a number
 * @param b Number to multiply to current
 * @return Polynomial multiply
 */
polyGF* polyGF::operator*(qint32 b){
    if (b==0) return new polyGF(field, 0);
    if (b==1) return new polyGF(field, new QVector<qint32>(coefficients));

    int32_t size = coefficients.size();
    QVector<qint32> product(0, size);
    for (int32_t i = 0; i < size; i++)
        product[i] = field->multiply(coefficients[i], b);

    return new polyGF(field, &product);
}

/**
 * @brief polyGF::operator / Divide one polynomial to other
 * @param b Polynomial to divide to current
 * @return Polynomial divide
 */
polyGF* polyGF::operator/(polyGF* b){
    QVector<polyGF*> results=div(b);
    delete results.at(1);
    return results.at(0);
}

/**
 * @brief polyGF::operator % Rest of the division ot the one polynomial to other
 * @param b Polynomial to divide to current
 * @return Polynomial rest
 */
polyGF* polyGF::operator%(polyGF* b){
    QVector<polyGF*> results=div(b);
    delete results.at(0);
    return results.at(1);
}

/**
 * @brief polyGF::div Divide one polynomial to other
 * @param b Polynomial to divide to current
 * @return First value return the divide. Second value return the rest.
 */
QVector<polyGF*> polyGF::div(polyGF* b){
    if (b->isZero()) return QVector<polyGF*>();

    polyGF *quotient=new polyGF(field, 0);
    polyGF *remainder= new polyGF(field, &coefficients);

    int32_t denominator = b->getCoefficients(b->getDegree());
    int32_t inverse =field->inverse(denominator);

    while(remainder->getDegree()>=b->getDegree() && !remainder->isZero()) {
        int32_t degreeDiff=remainder->getDegree()-b->getDegree();
        int32_t scale = field->multiply(remainder->getCoefficients((remainder->getDegree())),
                                        inverse);

        polyGF *iteratorQuotient= new polyGF(field, scale, degreeDiff);
        polyGF *term = (*b)*(iteratorQuotient);
        quotient=(*quotient)+iteratorQuotient;

        remainder=(*remainder)+(term);
        delete iteratorQuotient;
        delete term;
    }

    QVector<polyGF*> result=QVector<polyGF*>();
    result.append(quotient);
    result.append(remainder);
    return result;

}

/**
 * @brief polyGF::print Print Polynomial
 * @return String to be read by humans
 */
QString polyGF::print(){
    return print(coefficients);
};

/**
 * @brief polyGF::print Print Polynomial
 * @param info Coefficients of polynom
 * @return String to be read by humans
 */
QString polyGF::print(QVector<qint32> info){
    QString endData="";
    for(int i=0; i<info.size(); i++) endData += QStringLiteral("%1,").arg(info.at(i), 0, 16);
    return endData.remove(endData.size()-1, 1);
}
