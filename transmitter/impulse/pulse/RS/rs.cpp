/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "rs.h"

const QVector<RS::rsInfo> RS::type = {
    //    poly, m,  n,  k, sizePulse
    rsInfo{ 11, 3,  7,  5, 21},
    rsInfo{ 19, 4, 15, 11, 30},
    rsInfo{ 11, 3,  7,  5,  7},
    rsInfo{ 19, 4, 15, 11, 15},
    rsInfo{ 37, 5, 31, 25, 31},
    rsInfo{ 67, 6, 50, 40, 50},
    rsInfo{137, 7, 50, 40, 50},
    rsInfo{285, 8, 50, 40, 50}
};

/**
 * @brief RS::RS Create Reed-Solomon for solenoid
 * @param size
 */
RS::RS(unsigned int size){
    tRS=&type[size-1];
    field=new GF(tRS->m, tRS->poly);
    generator=buildGenerator(tRS->n-tRS->k);
}

/**
 * @brief RS::~RS Destruct Reed-Solomon
 */
RS::~RS(){
    delete generator;
    delete field;
}

/**
 * @brief RS::getInfo Information of Reed-Solomon
 * @return rsInfo contain all information necessary for fragment data
 */
RS::rsInfo RS::getInfo(){
    return *tRS;
}

/**
 * @brief RS::get Generate the redundancy and append it to ByteArray
 * @param rs Data to append redundancy
 */
void RS::get(QByteArray *rs){
    int sizeRS=tRS->n-tRS->k;

    QVector<qint32> infoCoefficients;
    for(int32_t x = 0; x < rs->size(); x++) infoCoefficients.append(rs->at(x));
    for(int i=0; i<sizeRS; i++) infoCoefficients.append(0);
    polyGF* polyRS=polyGF(field, &infoCoefficients)%generator;

    QVector<qint32> *coefRS=polyRS->getCoefficients();

    coefRS->insert(0, sizeRS-coefRS->size(), 0);
    for(int i=0; i<coefRS->size(); i++)
        rs->append(coefRS->at(i));

    if(tRS->n!=tRS->sPulse)
        fragment(rs, tRS->m, (tRS->n*tRS->m)/tRS->sPulse);
}

/**
 * @brief RS::buildGenerator Build a generator with correct degree
 * @param degree Degree of the generator
 * @return Polynomium generator
 */
polyGF* RS::buildGenerator(quint32 degree){
    polyGF *lastGenerator = new polyGF(field, 1);

    for(quint32 d=0; d<degree; d++) {
        QVector<qint32> temp;
        temp.append(1);
        temp.append(field->exp(d));
        polyGF *data= new polyGF(field, &temp);
        lastGenerator = (*lastGenerator)*(data);
    }

    return lastGenerator;
}

/**
 * @brief RS::fragment Fragment all data
 * @param data Data to fragment (and it is the output)
 * @param iBit Bits size input
 * @param oBit Bits size output
 */
void RS::fragment(QByteArray *data, uint iBit, uint oBit){
    QByteArray info = QByteArray(*data);
    data->clear();

    qint8 mask=qPow(2, oBit)-1;
    int endByte=(iBit/oBit)-1;

    for(int i=0; i<info.size(); i++)
        for(int j=endByte; j>=0; j--)
            data->append(((mask<<(j*oBit)) & info[i])>>(j*oBit));
}
