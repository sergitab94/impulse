/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef GF_H
#define GF_H

#include <QObject>

/**
 * @brief The GF class is a implementation of galois field
 */
class GF : public QObject {
    Q_OBJECT
public:
    GF(quint8 m, quint32 primitive);

    qint32 add(qint32 a, qint32 b);
    qint32 substract(qint32 a, qint32 b){return add(a, b);};
    qint32 multiply(qint32 a, qint32 b);
    qint32 divide(qint32 a, qint32 b);
    qint32 exp(qint32 a);
    qint32 log(qint32 a);
    qint32 inverse(qint32 a);

private:
    const quint8 size;
    qint32 expTable[510]={0};
    qint32 logTable[256]={0};
};

#endif
