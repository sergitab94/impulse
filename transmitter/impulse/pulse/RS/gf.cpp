/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "gf.h"
#include <QtMath>

/**
 * @brief GF::GF Creator of class
 * @param m 2^m is the size of the Galois Field
 * @param primitive It is the generator of the Galois Field
 */
GF::GF(quint8 m, quint32 primitive) : size(qPow(2,m)-1){
    qint32 x=1, mask=qPow(2,m);
    for (qint32 j=0; j<size; j++) {
        expTable[j]=x;
        expTable[j+size]=x;
        x <<= 1;
        if (x >= mask) x^=primitive;
    }
    for (qint32 i=0; i<size; i++) logTable[expTable[i]]=i;
}

/**
 * @brief GF::add Add a and b
 * @param a First number to add
 * @param b Second number to add
 * @return Result of the add
 */
qint32 GF::add(qint32 a, qint32 b){
    return a^b;
}

/**
 * @brief GF::multiply Multiply a and b
 * @param a Fist number to multiply
 * @param b Second number to multiply
 * @return Result of the muyltiply
 */
qint32 GF::multiply(qint32 a, qint32 b){
    if (a==0 || b==0) return 0;
    if (a==1) return b;
    if (b==1) return a;
    return expTable[logTable[a] + logTable[b]];
}

/**
 * @brief GF::divide Divide a number
 * @param a Dividend number
 * @param b Divisor number
 * @return Result of the divide
 */
qint32 GF::divide(qint32 a, qint32 b) {
    if (a==0 || b==0) return 0;

    return expTable[(logTable[a] + size - logTable[b]) % size];
}

/**
 * @brief GF::exp Exponent a number
 * @param a Number to exponent
 * @return Result of the exponent
 */
qint32 GF::exp(qint32 a) {
    return expTable[a];
}

/**
 * @brief GF::inverse Invert a number
 * @param a Number to invert
 * @return Result of the invert
 */
qint32 GF::inverse(qint32 a) {
    if (a == 0) return 0;
    return expTable[size-logTable[a]];
}
