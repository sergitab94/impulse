/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef RS_H
#define RS_H

#include <QObject>
#include <QtMath>

#include "gf.h"
#include "polygf.h"

/**
 * @brief The RS class Encode Reed-Solomon and add to end
 */
class RS: public QObject{
Q_OBJECT
public:
    struct rsInfo {
        const quint64 poly;
        const quint16 m, n, k;
        const qint16 sPulse;
    };

    RS(unsigned int size);
    virtual ~RS();

    void get(QByteArray *rs);
    rsInfo getInfo();

private:
    const static QVector<rsInfo> type;

    //Reed Solomon
    const rsInfo *tRS;
    polyGF *generator;
    GF *field;

    polyGF* buildGenerator(quint32 degree);
    void fragment(QByteArray *data, uint iBit, uint oBit);
};

#endif // RS_H
