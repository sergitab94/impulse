QT += core widgets gui network serialport printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

VERSION = 1.0.1-beta

TARGET = Impulse

DEFINES += VERSION_STRING=\\\"$${VERSION}\\\"

SOURCES += \
    interface/debugtable.cpp \
    interface/debugwindow.cpp \
    interface/infodialog.cpp \
    interface/mainwindow.cpp \
    interface/tab.cpp \
    interface/tabtask.cpp \
    interface/taskwidgetdelegate.cpp \
    main.cpp \
    others/checkupdate.cpp \
    others/debugfile.cpp \
    others/settings.cpp \
    others/tablist.cpp \
    pulse/a_application.cpp \
    pulse/RS/gf.cpp \
    pulse/RS/listrs.cpp \
    pulse/RS/polygf.cpp \
    pulse/RS/rs.cpp \
    pulse/ba_datalink.cpp \
    pulse/bb_blue.cpp \
    pulse/c_infodevice.cpp \
    pulse/CRC/crc.cpp \
    pulse/pulse.cpp \
    pulse/task.cpp \
    qcustomplot/qcustomplot.cpp

HEADERS += \
    interface/debugtable.h \
    interface/debugwindow.h \
    interface/infodialog.h \
    interface/mainwindow.h \
    interface/tab.h \
    interface/tabtask.h \
    interface/taskwidgetdelegate.h \
    others/checkupdate.h \
    others/debugfile.h \
    others/settings.h \
    others/tablist.h \
    pulse/a_application.h \
    pulse/RS/gf.h \
    pulse/RS/listrs.h \
    pulse/RS/polygf.h \
    pulse/RS/rs.h \
    pulse/ba_datalink.h \
    pulse/bb_blue.h \
    pulse/c_infodevice.h \
    pulse/CRC/crc.h \
    pulse/deviceexception.h \
    pulse/pulse.h \
    pulse/task.h \
    qcustomplot/qcustomplot.h

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

TRANSLATIONS += \
    tr/es/tr.ts

RESOURCES += \
    rc/rc.qrc \
    tr/tr.qrc

RC_ICONS = rc/logo.ico

RC_FILE = rc/resource.rc
