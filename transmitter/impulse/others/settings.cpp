/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "settings.h"

#include <QStandardPaths>
#include <QSettings>

Settings* Settings::instance = nullptr;

/**
 * @brief Settings::getInstance Return the instance of the Singelton
 * @param parent
 * @return
 */
Settings* Settings::getInstance(QObject *parent) {
    if (instance == nullptr) instance = new Settings(parent);
    return instance;
}

/**
 * @brief Settings::delInstance Delete the instance of the Singelton
 */
void Settings::delInstance() {
    if (instance != nullptr) delete instance;
}

/**
 * @brief Settings::Settings Construct of the object
 * @param parent
 */
Settings::Settings(QObject *parent) : QObject(parent) {
    fileSettings = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/config.ini";
    load();
}

/**
 * @brief Settings::~Settings Destruct the object
 */
Settings::~Settings(){
    QSettings settings(fileSettings, QSettings::IniFormat);
    settings.setValue("night", night);
    settings.setValue("bluetooth", bluetooth);
    settings.setValue("tab", listToString(ntab));
    settings.sync();
}

/**
 * @brief Settings::load Load values of the file and convert to atribute of class
 */
void Settings::load() {
    QSettings settings(fileSettings, QSettings::IniFormat);
    night = settings.value("night", false).toBool();
    bluetooth = settings.value("bluetooth", true).toBool();
    QString data = settings.value("tab", "").toString();
    ntab=stringToList(data);
    if(ntab.length()==0){
        ntab << (TypeTask) 1 << (TypeTask) 2 << (TypeTask) 3;
        ntab << (TypeTask) 4 << (TypeTask) 5 << (TypeTask) 6;
    }
}

/**
 * @brief Settings::listToString Convert a QList in a QString separated in commas
 * @param list List to Convert
 * @return String converted in numbers and commas
 */
QString Settings::listToString(QList<TypeTask> list){
    QString  data = "";
    foreach(TypeTask i, list) data += QString("%1,").arg((int) i);
    return data.left(data.length()-1);
}

/**
 * @brief Settings::stringToList Convert a QString separated in commas in a QList
 * @param data String to convert in numbers and commas
 * @return List to converted
 */
QList<TypeTask> Settings::stringToList(QString data){
    QList<TypeTask> list;
    QStringList slist = data.split(",");
    foreach(QString i, slist ){
        bool conv;
        int l=i.toInt(&conv);
        if(conv && l<7 && l>0) list.push_back((TypeTask) l);
        else return QList<TypeTask>();
    }
    return list;
}
