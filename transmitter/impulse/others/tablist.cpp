/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "tablist.h"

/**
 * @brief TabList::addTab Add tab to QList
 * @param tab TypeTask to add
 */
void TabList::addTab(TypeTask tab) {
    ltab.push_back(tab);
}

/**
 * @brief TabList::changeTab Delete if exist or add if not exist
 * @param tab TypeTask to add
 * @return Position of the task add
 */
int TabList::changeTab(TypeTask tab) {
    int index=1;
    bool show=true;
    TypeTask i;

    foreach(i, ltab){
        if(i==tab) show=false;
        if(i>=tab) break;
        index++;
    }

    if(!show){
        ltab.removeOne(i);
        return index*-1;
    }

    ltab.insert((index-1), tab);
    return index;
}

/**
 * @brief TabList::getList Obtain the list
 * @return The list that was added
 */
QList<TypeTask> TabList::getList(){
    return ltab;
}
