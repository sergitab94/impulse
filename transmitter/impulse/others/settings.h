/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include "pulse/task.h"

#include <QString>

typedef Task::TypeTask TypeTask;

/**
 * @brief The Settings class obtain the settings from the file
 */
class Settings : QObject {
    Q_OBJECT

public:
    QList<TypeTask> ntab;
    bool night, bluetooth;

    static Settings* getInstance(QObject *parent=nullptr);
    static void delInstance();

private:
    static Settings* instance;
    QString fileSettings;

    Settings(QObject *parent=nullptr);
    ~Settings();

    void load();
    QString listToString(QList<TypeTask> list);
    QList<TypeTask> stringToList(QString data);
};

#endif // PREFERENCES_H
