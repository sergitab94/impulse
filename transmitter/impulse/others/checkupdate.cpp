/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "checkupdate.h"

#include <QMessageBox>
#include <QDesktopServices>
#include <QtNetwork>

/**
 * @brief CheckUpdate::CheckUpdate Constructor
 * @param parent
 */
CheckUpdate::CheckUpdate(QWidget *parent) : QWidget(parent){
    api=QUrl("https://gitlab.com/api/v4/projects/10969716/releases");
    release=QUrl("https://gitlab.com/sergitab94/impulse/-/releases");
}

/**
 * @brief CheckUpdate::start Check if need update
 */
void CheckUpdate::start(){
    if(qnam!=nullptr) {
        disconnect(qnam, &QNetworkAccessManager::finished, this, &CheckUpdate::replyFinished);
        delete qnam;
    }
    qnam = new QNetworkAccessManager(this);
    connect(qnam, &QNetworkAccessManager::finished, this, &CheckUpdate::replyFinished);
    qnam->get(QNetworkRequest(api));
}

/**
 * @brief CheckUpdate::replyFinished Call when the server reply and check if it's new version
 * @param reply The reply of the server
 */
void CheckUpdate::replyFinished(QNetworkReply *reply){
    if(reply->error()){
        if (QNetworkReply::ContentNotFoundError == reply->error()){
            QMessageBox::critical(this, tr("Error"), tr("Server not available"));
        }else QMessageBox::critical(this, tr("Error"), tr("Network failed"));
        return;
    }
    QString strReply = (QString) reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject obj = jsonResponse.array()[0].toObject();
    if(obj.value("name").toString() == VERSION_STRING){
        QMessageBox::information(this, tr("Information"), tr("Without updates"));
        return;
    }
    if (QMessageBox::question(this, tr("Update"), tr("There is a new update. Do you want to update?"))==QMessageBox::Yes)
        QDesktopServices::openUrl(release);
}
