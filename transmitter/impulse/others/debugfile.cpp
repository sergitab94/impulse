/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "debugfile.h"

#include <QFileDialog>
#include <QTextStream>
#include <QDialog>
#include <QRegularExpression>

/**
 * @brief DebugFile::openFile The file to parse
 * @param w Neccessary to open a .csv
 * @param p Number of the pulse to analyze
 * @param t Time of the pulse to analyze
 * @param x Magnetic field in axis X (MicroTeslas)
 * @param y Magnetic field in axis Y (MicroTeslas)
 * @param z Magnetic field in axis Z (MicroTeslas)
 * @param info Information of device line to line
 * @return
 */
bool DebugFile::openFile(QMainWindow *w, QVector<double> *p, QVector<double> *t,
                         QVector<double> *x, QVector<double> *y, QVector<double> *z,
                         QString *info){
    QString debugFile = QFileDialog::getOpenFileName(w, tr("Open debug file"),"","*.csv");
    if(debugFile=="") return false;
    QFile file(debugFile);
    if (!file.open(QIODevice::ReadOnly)) return false;
    QTextStream stream(&file);
    double init=0, avg=0, max=0, min=0;
    QVector<QString> pref={tr("Model"), tr("Manufacturer"), tr("API Android"), tr("App version"),
                          tr("Model of sensor"), tr("Vendor of sensor"), tr("Version of sensor"),
                          tr("Maximum delay (ms)"), tr("Minimum delay (ms)"), tr("Maximum range (µT)"),
                          tr("Resolution"), tr("Power (mA)")};

    QVector<QString> data={"Model", "Manufacturer", "API Android", "App version",
                          "Model of sensor", "Vendor of sensor", "Version of sensor",
                          "Maximum delay", "Minimum delay", "Maximum range",
                          "Resolution", "Power"};

    while (!stream.atEnd()){
        QString l=stream.readLine();
        if(l.at(0)=='#'){
            for(int i=0;i<data.size();i++){
                if(l.contains(QRegularExpression("^#"+data[i]+":"))) {
                    *info+="-" + pref[i] + ": ";
                    *info+=l.right(l.length()-data[i].length()-2) + "\n";
                    break;
                }
            }
            continue;
        } else if(l=="Timestamp,X,Y,Z") continue;
        QStringList list = l.split(QRegularExpression(","));
        bool ok = false;
        if(list.size()!=4) return false;
        double time=list[0].toDouble(&ok);
        if(!ok) return false;
        if(init==1) min=time-*t->rbegin();
        if(init>0){
            double diff = time-*t->rbegin();
            max=qMax(diff, max);
            min=qMin(diff, min);
            avg+=(diff-avg)/init;
        }
        t->append(time);
        p->append(init++);
        x->append(list[1].toDouble(&ok));
        if(!ok) return false;
        y->append(list[2].toDouble(&ok));
        if(!ok) return false;
        z->append(list[3].toDouble(&ok));
        if(!ok) return false;
    }
    *info+="-" + tr("Average (ns)") + ": " + QString::number(avg, 'd', 4) + "\n";
    *info+="-" + tr("Minimum (ns)") + ": " + QString::number(min, 'd', 0) + "\n";
    *info+="-" + tr("Maximum (ns)") + ": " + QString::number(max, 'd', 0);
    return true;
}
