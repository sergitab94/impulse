<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../../others/checkupdate.cpp" line="53"/>
        <location filename="../../others/checkupdate.cpp" line="54"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../others/checkupdate.cpp" line="53"/>
        <source>Server not available</source>
        <translation>Servidor no disponible</translation>
    </message>
    <message>
        <location filename="../../others/checkupdate.cpp" line="54"/>
        <source>Network failed</source>
        <translation>Red falló</translation>
    </message>
    <message>
        <location filename="../../others/checkupdate.cpp" line="61"/>
        <source>Information</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../../others/checkupdate.cpp" line="61"/>
        <source>Without updates</source>
        <translation>Sin actualizaciones</translation>
    </message>
    <message>
        <location filename="../../others/checkupdate.cpp" line="64"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../../others/checkupdate.cpp" line="64"/>
        <source>There is a new update. Do you want to update?</source>
        <translation>Hay nuevas actualizaciones ¿Desea actualizar?</translation>
    </message>
</context>
<context>
    <name>DebugFile</name>
    <message>
        <location filename="../../others/debugfile.cpp" line="42"/>
        <source>Open debug file</source>
        <translation>Abrir fichero de desarrollo</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="48"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="48"/>
        <source>Manufacturer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="48"/>
        <source>API Android</source>
        <translation>API Android</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="48"/>
        <source>App version</source>
        <translation>Versión App</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="49"/>
        <source>Model of sensor</source>
        <translation>Modelo del sensor</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="49"/>
        <source>Vendor of sensor</source>
        <translation>Vendedor del sensor</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="49"/>
        <source>Version of sensor</source>
        <translation>Versión del sensor</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="50"/>
        <source>Maximum delay (ms)</source>
        <translation>Máximo retardo (ms)</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="50"/>
        <source>Minimum delay (ms)</source>
        <translation>Mínimo retardo (ms)</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="50"/>
        <source>Maximum range (µT)</source>
        <translation>Máximo rango (µT)</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="51"/>
        <source>Resolution</source>
        <translation>Resolución</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="51"/>
        <source>Power (mA)</source>
        <translation>Intensidad (mA)</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="91"/>
        <source>Average (ns)</source>
        <translation>Media (ns)</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="92"/>
        <source>Minimum (ns)</source>
        <translation>Mínimo (ns)</translation>
    </message>
    <message>
        <location filename="../../others/debugfile.cpp" line="93"/>
        <source>Maximum (ns)</source>
        <translation>Máximo (ns)</translation>
    </message>
</context>
<context>
    <name>DebugTable</name>
    <message>
        <location filename="../../interface/debugtable.cpp" line="42"/>
        <source>No.</source>
        <translation>No.</translation>
    </message>
    <message>
        <location filename="../../interface/debugtable.cpp" line="43"/>
        <source>Timestamp (ns)</source>
        <translation>Timestamp (ns)</translation>
    </message>
    <message>
        <location filename="../../interface/debugtable.cpp" line="44"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../../interface/debugtable.cpp" line="45"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../../interface/debugtable.cpp" line="46"/>
        <source>Z</source>
        <translation>Z</translation>
    </message>
</context>
<context>
    <name>DebugWindow</name>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="31"/>
        <source>Impulse</source>
        <translation>Impulse</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="31"/>
        <source>Debug Window</source>
        <translation>Ventana de desarrollador</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="85"/>
        <location filename="../../interface/debugwindow.cpp" line="148"/>
        <source>Export to .png</source>
        <translation>Exportar a .png</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="143"/>
        <source>File</source>
        <translation>Fichero</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="144"/>
        <source>Open debug file</source>
        <translation>Abrir fichero desarrollador</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="154"/>
        <source>Back</source>
        <translation>Atrás</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="158"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="159"/>
        <source>Night mode</source>
        <translation>Modo noche</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="165"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="179"/>
        <source>Show/hide X coordinates</source>
        <translation>Mostrar/ocultar coordenadas X</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="187"/>
        <source>Show/hide Y coordinates</source>
        <translation>Mostrar/ocultar coordenadas Y</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="195"/>
        <source>Show/hide Z coordinates</source>
        <translation>Mostrar/ocultar coordenadas Z</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="217"/>
        <source>Sample Nº</source>
        <translation>Nº de muestra</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="218"/>
        <source>MicroTeslas (µT)</source>
        <translation>MicroTesla (µT)</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="358"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../interface/debugwindow.cpp" line="358"/>
        <source>Error to charge file</source>
        <translation>Error al cargar fichero</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../../interface/infodialog.cpp" line="33"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../../interface/infodialog.cpp" line="45"/>
        <source>About the application</source>
        <translation>Sobre la aplicación</translation>
    </message>
    <message>
        <location filename="../../interface/infodialog.cpp" line="52"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../../interface/infodialog.cpp" line="55"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="36"/>
        <source>Impulse</source>
        <translation>Impulse</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="44"/>
        <location filename="../../interface/mainwindow.cpp" line="198"/>
        <source>Disconnect</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="84"/>
        <location filename="../../interface/mainwindow.cpp" line="271"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="84"/>
        <source>Not allow multiple instance</source>
        <translation>No se permiten múltiples instancias</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="149"/>
        <source>Without devices</source>
        <translation>Sin dispositivos</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="189"/>
        <location filename="../../interface/mainwindow.cpp" line="209"/>
        <location filename="../../interface/mainwindow.cpp" line="211"/>
        <location filename="../../interface/mainwindow.cpp" line="232"/>
        <location filename="../../interface/mainwindow.cpp" line="234"/>
        <source>Connect</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="209"/>
        <source>Waiting to other phone.</source>
        <translation>Esperando a otro móvil.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="232"/>
        <source>Please, not move device!!</source>
        <translation>¡¡Por favor, no mueva el dipositivo!!</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="251"/>
        <source>Port error.</source>
        <translation>Error de puerto.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="252"/>
        <source>Unknown fault.</source>
        <translation>Fallo desconocido.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="253"/>
        <source>Hardware failed.</source>
        <translation>Hardware falló.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="254"/>
        <source>Power fault.</source>
        <translation>Sin alimentador.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="255"/>
        <source>Phone removed.</source>
        <translation>Móvil retirado.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="256"/>
        <source>Fail connection.</source>
        <translation>Conexión falló.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="257"/>
        <source>Transfer bluetooth forced stop.</source>
        <translation>Transferencia bluetooth parada.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="258"/>
        <source>Bluetooth unknown fault.</source>
        <translation>Fallo bluetooth desconocido.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="279"/>
        <source>Put the phone over device and wait for the sending to restart.</source>
        <translation>Sitúe el móvil sobre el dispositivo y espere a que el envío se reinicie.</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="300"/>
        <source>Send Bluetooth</source>
        <translation>Enviar por Bluetooth</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="307"/>
        <source>&amp;Device</source>
        <translation>&amp;Dispositivo</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="310"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="313"/>
        <source>Task</source>
        <translation>Tarea</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="319"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="321"/>
        <source>Mode Night</source>
        <translation>Modo Noche</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="329"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="331"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="335"/>
        <source>Contact</source>
        <translation>Contactar</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="339"/>
        <source>Translate</source>
        <translation>Traducir</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="343"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="349"/>
        <location filename="../../interface/mainwindow.cpp" line="354"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="350"/>
        <source>Close program</source>
        <translation>Cerrar programa</translation>
    </message>
    <message>
        <location filename="../../interface/mainwindow.cpp" line="355"/>
        <source>File not available. Task deleted.</source>
        <translation>Archivo no disponible. Tarea borrada.</translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="../../interface/tab.cpp" line="69"/>
        <source>Repeat</source>
        <translation>Repetir</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="74"/>
        <source>Send</source>
        <translation>Enviar</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="92"/>
        <source>Open…</source>
        <translation>Abrir…</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="95"/>
        <source>Open File</source>
        <translation>Abrir Fichero</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="95"/>
        <source>All files (*.*)</source>
        <translation>Todos los ficheros (*.*)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="110"/>
        <location filename="../../interface/tab.cpp" line="117"/>
        <location filename="../../interface/tab.cpp" line="158"/>
        <location filename="../../interface/tab.cpp" line="162"/>
        <location filename="../../interface/tab.cpp" line="255"/>
        <location filename="../../interface/tab.cpp" line="260"/>
        <location filename="../../interface/tab.cpp" line="266"/>
        <location filename="../../interface/tab.cpp" line="271"/>
        <location filename="../../interface/tab.cpp" line="276"/>
        <location filename="../../interface/tab.cpp" line="336"/>
        <location filename="../../interface/tab.cpp" line="341"/>
        <location filename="../../interface/tab.cpp" line="346"/>
        <location filename="../../interface/tab.cpp" line="351"/>
        <location filename="../../interface/tab.cpp" line="428"/>
        <location filename="../../interface/tab.cpp" line="435"/>
        <location filename="../../interface/tab.cpp" line="444"/>
        <location filename="../../interface/tab.cpp" line="451"/>
        <location filename="../../interface/tab.cpp" line="459"/>
        <location filename="../../interface/tab.cpp" line="466"/>
        <location filename="../../interface/tab.cpp" line="471"/>
        <location filename="../../interface/tab.cpp" line="549"/>
        <location filename="../../interface/tab.cpp" line="554"/>
        <location filename="../../interface/tab.cpp" line="559"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="110"/>
        <location filename="../../interface/tab.cpp" line="117"/>
        <source>File not available</source>
        <translation>Fichero no disponible</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="138"/>
        <location filename="../../interface/tab.cpp" line="311"/>
        <location filename="../../interface/tab.cpp" line="401"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="158"/>
        <location filename="../../interface/tab.cpp" line="346"/>
        <location filename="../../interface/tab.cpp" line="466"/>
        <source>Message is void</source>
        <translation>Mensaje vacío</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="162"/>
        <source>Only readable characters</source>
        <translation>Sólo carácteres legibles</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="183"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="189"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="193"/>
        <source>WPA 2</source>
        <translation>WPA 2</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="194"/>
        <source>WPA 3</source>
        <translation>WPA 3</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="195"/>
        <source>Without password</source>
        <translation>Sin contraseña</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="199"/>
        <source>Hidden</source>
        <translation>Oculto</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="202"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="255"/>
        <source>Name is void</source>
        <translation>Nombre vacío</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="260"/>
        <source>Name only readable characters</source>
        <translation>Nombre sólo carácteres legibles</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="266"/>
        <source>Password too short (at least 8 character)</source>
        <translation>Contraseña demasiada corta (al menos 8 carácteres)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="276"/>
        <source>Password only accepts printable ASCII characters</source>
        <translation>Contraseña sólo acepta carácteres imprimibles ASCII</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="303"/>
        <source>Phone</source>
        <translation>Móvil</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="336"/>
        <source>Phone is void</source>
        <translation>Móvil está vacío</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="341"/>
        <source>Phone invalid</source>
        <translation>Móvil inválido</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="351"/>
        <source>Message only accepts printable ASCII characters</source>
        <translation>Mensaje sólo acepta caracteres imprimibles ASCII</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="373"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="380"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="387"/>
        <source>Subject</source>
        <translation>Asunto</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="394"/>
        <source>Bcc</source>
        <translation>Cco</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="428"/>
        <source>To is void</source>
        <translation>Para está vacío</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="435"/>
        <source>To no contain a e-mail (of various, separate for &apos;,&apos;)</source>
        <translation>Para no contiene e-mail (para varios, separar con &apos;,&apos;)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="444"/>
        <source>Cc not contain e-mail (of various, separate for &apos;,&apos;)</source>
        <translation>Cc no contiene e-mail (para varios, separar con &apos;,&apos;)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="471"/>
        <source>Message only accepts readable character</source>
        <translation>Mensaje sólo acepta carácteres legibles</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="496"/>
        <source>Recipient</source>
        <translation>Receptor</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="504"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="451"/>
        <source>Subject only accepts readable characters</source>
        <translation>Asunto sólo acepta carácteres legibles</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="209"/>
        <source>Repeat password</source>
        <translation>Repetir contraseña</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="271"/>
        <source>Passwords not equal</source>
        <translation>Contraseñas no coinciden</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="459"/>
        <source>Bcc not contain e-mail (of various, separate for &apos;,&apos;)</source>
        <translation>Cco no contiene e-mail (para varios, separar con &apos;,&apos;)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="509"/>
        <source>Bitcoins</source>
        <translation>Bitcoins</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="521"/>
        <source>BTC</source>
        <translation>BTC</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="522"/>
        <source>dBTC (1⁻¹)</source>
        <translation>dBTC (1⁻¹)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="523"/>
        <source>cBTC (1⁻²)</source>
        <translation>cBTC (1⁻²)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="524"/>
        <source>mBTC (1⁻³)</source>
        <translation>mBTC (1⁻³)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="525"/>
        <source>μBTC (1⁻⁶)</source>
        <translation>μBTC (1⁻⁶)</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="549"/>
        <source>Recipient invalid</source>
        <translation>Receptor inválido</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="554"/>
        <source>Description only accepts printable ASCII characters</source>
        <translation>Descripción sólo acepta carácteres imprimibles ASCII</translation>
    </message>
    <message>
        <location filename="../../interface/tab.cpp" line="559"/>
        <source>Amount invalid</source>
        <translation>Cantidad no válida</translation>
    </message>
</context>
<context>
    <name>TabTask</name>
    <message>
        <location filename="../../interface/tabtask.cpp" line="36"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../interface/tabtask.cpp" line="36"/>
        <source>Repeat</source>
        <translation>Repetir</translation>
    </message>
    <message>
        <location filename="../../interface/tabtask.cpp" line="36"/>
        <source>Progress</source>
        <translation>Progreso</translation>
    </message>
</context>
<context>
    <name>Task</name>
    <message>
        <location filename="../../pulse/task.cpp" line="96"/>
        <source>File</source>
        <translation>Fichero</translation>
    </message>
    <message>
        <location filename="../../pulse/task.cpp" line="97"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location filename="../../pulse/task.cpp" line="98"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
    <message>
        <location filename="../../pulse/task.cpp" line="99"/>
        <source>SMS</source>
        <translation>SMS</translation>
    </message>
    <message>
        <location filename="../../pulse/task.cpp" line="100"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../../pulse/task.cpp" line="101"/>
        <source>Bitcoin</source>
        <translation>Bitcoin</translation>
    </message>
</context>
</TS>
