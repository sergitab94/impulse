/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "interface/mainwindow.h"

#include <QApplication>
#include <QSharedMemory>
#include <QTranslator>
#include <QLibraryInfo>

/**
 * @brief qMain It is the main function to execute in C and C++
 * @param argc Number of arguments
 * @param argv Vector of arguments
 * @return Number of error
 */
int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    QTranslator qtTranslate, myTranslate;
    if(qtTranslate.load(":/tr/qtbase.qm")) app.installTranslator(&qtTranslate);
    if(myTranslate.load(":/tr/tr.qm")) app.installTranslator(&myTranslate);

    QSharedMemory shared("={lj=3<PhsT)ZB_ab<l/Ju(O4'e*_c[[,(;1Tf@GObcBL^v'K'W4b:OEM+usR*Wd");
    if(!shared.create(512, QSharedMemory::ReadWrite)) MainWindow::multipleInstance();

    MainWindow w;
    w.show();
    return app.exec();
}
