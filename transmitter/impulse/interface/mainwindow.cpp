/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "mainwindow.h"
#include "infodialog.h"

#include <QtWidgets>

/**
 * @brief MainWindow::MainWindow Constructor
 */
MainWindow::MainWindow() : QMainWindow(), tabL(new TabList()){
    Pulse *pulse = Pulse::getInstance();
    connect(pulse, &Pulse::connectDevice, this, &MainWindow::connectDevice);
    connect(pulse, &Pulse::waiting, this, &MainWindow::waiting);
    connect(pulse, &Pulse::error, this, &MainWindow::error);
    connect(pulse, &Pulse::deleteTask, this, &MainWindow::deleteTask);

    setWindowTitle(tr("Impulse"));

    sett=Settings::getInstance();

    tabL=new TabList();
    tabW=new QTabWidget();
    createInterface();
    setCentralWidget(tabW);
    state = new QLabel(tr("Disconnect"));
    statusBar()->addWidget(state);

    defaultStyle = qApp->style()->objectName();
    night(sett->night);

    setMinimumSize(600, 300);
    setMaximumSize(900, 450);
    QSize qsize(600,300);

    QList<QScreen*> screens = QGuiApplication::screens();
    QScreen* screen = screens.first();

    setGeometry(QStyle::alignedRect(
        Qt::LeftToRight, Qt::AlignCenter, qsize,
        screen->geometry()
    ));
}

/**
 * @brief MainWindow::~MainWindow Destruct the object
 */
MainWindow::~MainWindow(){
    Settings::delInstance();
    Pulse::delInstance();
    if(thError!=nullptr){
        thError->terminate();
        thError->wait();
    }
    delete thError;
    delete deviceAct;
    delete tabL;
    delete tabW;
    delete state;
}

/**
 * @brief MainWindow::multipleInstance Show dialog for multiple instances
 */
void MainWindow::multipleInstance(){
    QMessageBox::critical(nullptr, tr("Error"), tr("Not allow multiple instance"));
    exit(0);
}

/**
 * @brief MainWindow::night Change mode night
 * @param check True if mode night is power on
 */
void MainWindow::night(bool check){
    sett->night=check;
    if (!check){
        qApp->setPalette(qApp->style()->standardPalette());
        qApp->setStyle(QStyleFactory::create(defaultStyle));
        return;
    }
    qApp->setStyle(QStyleFactory::create("Fusion"));
    QPalette darkPalette;
    QColor color1(53, 53, 53);
    QColor color2(128, 128, 128);
    QColor color3(42, 130, 218);

    darkPalette.setColor(QPalette::Window, color1);
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, color1);
    darkPalette.setColor(QPalette::AlternateBase, color1);
    darkPalette.setColor(QPalette::ToolTipBase, color3);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, color1);
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::Link, color3);
    darkPalette.setColor(QPalette::Highlight, color3);
    darkPalette.setColor(QPalette::HighlightedText, Qt::black);

    darkPalette.setColor(QPalette::Active, QPalette::Button, color2.darker());
    darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, color2);
    darkPalette.setColor(QPalette::Disabled, QPalette::WindowText, color2);
    darkPalette.setColor(QPalette::Disabled, QPalette::Light, color1);

    qApp->setPalette(darkPalette);
}

/**
 * @brief MainWindow::deviceList Show menu for devices COM connected
 */
void MainWindow::deviceList(){
    Pulse *pulse = Pulse::getInstance();
    QString conn=pulse->getPort();
    QStringList list=pulse->listPorts();
    deviceAct->clear();
    unsigned int i=0;
    foreach(QString l, list){
        QAction *subMenu = new QAction(l, this);
        subMenu->setCheckable(true);
        if(l==conn) subMenu->setChecked(true);
        connect(subMenu, &QAction::triggered, this, [=](bool check){
            QString port=qobject_cast<QAction*>(sender())->text();
            if(check) check=pulse->conn(port);
            else pulse->disconn();
        });

        deviceAct->addAction(subMenu);
        i++;
    }
    if(i==0){
        QAction *nullDev = new QAction(tr("Without devices"), this);
        nullDev->setEnabled(false);
        deviceAct->addAction(nullDev);
    }
}

/**
 * @brief MainWindow::tabShows Disabled or enable differents tabs
 */
void MainWindow::tabShows(){
    QAction *button = qobject_cast<QAction*>(sender());
    TypeTask type = (TypeTask) button->objectName().toInt();
    int pos=tabL->changeTab(type);
    if(pos>0) tabW->insertTab(pos, new Tab(tabT, type, blue, this), Task::taskToStr(type));
    else tabW->removeTab(pos*-1);
    sett->ntab=tabL->getList();
}

/**
 * @brief MainWindow::contact Generate a email
 */
void MainWindow::contact(){
    QString recept = "sergio.tabares@uva.es";
    QString subject = "[Impulse] - Feedback";
    QString body = "\n\nVersion: " + QString(VERSION_STRING);
    QDesktopServices::openUrl(QUrl("mailto:?to=" + recept + "&subject=" + subject + "&body=" + body, QUrl::TolerantMode));
}

/**
 * @brief MainWindow::closeApp Close interface
 */
void MainWindow::closeApp(){
    qApp->quit();
}

/**
 * @brief MainWindow::conn Change the inteface when it is connected/disconnected
 * @param check
 */
void MainWindow::connectDevice(bool check){
    if(check) state->setText(tr("Connect"));
    else{
        if(thError!=nullptr) {
            thError->terminate();
            thError->wait();
        }
        delete thError;
        thError = nullptr;

        state->setText(tr("Disconnect"));
        tabT->clear();
    }
    tabW->setDisabled(!check);
}

/**
 * @brief MainWindow::waiting The device is waiting to other phone
 * @param wait True if is waiting
 */
void MainWindow::waiting(bool wait){
    if(wait) state->setText(tr("Connect") + " - " + tr("Waiting to other phone."));
    else {
        state->setText(tr("Connect"));
        msgWarning.close();
    }
}

/**
 * @brief MainWindow::errorConn Show the interface the different errors
 * @param error Type of error (It's in DeviceException::PulseError)
 */
void MainWindow::error(DeviceException error){
    PulseError e=error.getError();
    QString msg;
    //Show info down
    if(e==PulseError::move){         //6
        if(thError!=nullptr) {
            thError->terminate();
            thError->wait();
        }
        if(thError==nullptr || thError->isFinished()){
            delete thError;
            thError=QThread::create([this]{
                state->setText(tr("Connect") + " - " + tr("Please, not move device!!"));
                QThread::sleep(20);
                state->setText(tr("Connect"));
            });
        }
        thError->start();
        return;
    }

    if(thError!=nullptr) {
        thError->terminate();
        thError->wait();
    }
    delete thError;
    thError = nullptr;

    QWidget *oldModal = QApplication::activeModalWidget();
    if(oldModal!=nullptr) oldModal->close();

    if(e==PulseError::portCom) msg=tr("Port error.");                               //Without number
    else if(e==PulseError::unknown) msg=tr("Unknown fault.");                       //Without number
    else if(e==PulseError::hardware) msg=tr("Hardware failed.");                    //1,2,3
    else if(e==PulseError::power) msg=tr("Power fault.");                           //4
    else if(e==PulseError::proximity) msg=tr("Phone removed.");                     //5
    else if(e==PulseError::connectionBlue) msg=tr("Fail connection.");              //7
    else if(e==PulseError::transferBlue) msg=tr("Transfer bluetooth forced stop."); //8
    else if(e==PulseError::unknownBlue) msg=tr("Bluetooth unknown fault.");         //9

    switch(e){
    //Show msgBox and wait
    case PulseError::FileNotExist:   //Without number
        msgFile.show();
        break;

    //Disconnect and show msgBox
    case PulseError::portCom:        //Without number
    case PulseError::unknown:        //Without number
    case PulseError::hardware:       //1,2,3
    case PulseError::power:          //4
        QMessageBox::critical(this, tr("Error"), msg);
        break;

    //Show msgBox and pause transmit
    case PulseError::proximity:      //5
    case PulseError::connectionBlue: //7
    case PulseError::transferBlue:   //8
    case PulseError::unknownBlue:    //9
        msg+=" " + tr("Put the phone over device and wait for the sending to restart.");
        msgWarning.setText(msg);
        msgWarning.show();
        break;

    default:;
    }
}

/**
 * @brief MainWindow::deleteTask Delete an expecific task
 * @param task Number of the task to delete
 */
void MainWindow::deleteTask(int task){
    tabT->deleteTask(task);
}

/**
 * @brief MainWindow::createInterface Generate interface to the start
 */
void MainWindow::createInterface(){
    blue = new QAction(tr("Send Bluetooth"), this);
    blue->setCheckable(true);
    blue->setChecked(sett->bluetooth);
    connect(blue, &QAction::triggered, this, [=](bool check){sett->bluetooth=check; });

    QList<TypeTask> ntab = sett->ntab;

    deviceAct = menuBar()->addMenu(tr("&Device"));
    connect(deviceAct, &QMenu::aboutToShow, this, &MainWindow::deviceList);

    QMenu *view = menuBar()->addMenu(tr("&View"));

    tabT = new TabTask(tabW, this);
    tabW->addTab(tabT, tr("Task"));
    for (int i=TypeTask::File; i<=TypeTask::Bitcoin; i++) {
        createTab(view, (TypeTask) i, blue, ntab);
    }
    tabW->setDisabled(true);

    QMenu *settings = menuBar()->addMenu(tr("Settings"));

    QAction *night = new QAction(tr("Mode Night"), this);
    night->setCheckable(true);
    night->setChecked(sett->night);
    settings->addAction(night);
    connect(night, &QAction::triggered, this, &MainWindow::night);

    settings->addAction(blue);

    QMenu *help = menuBar()->addMenu(tr("Help"));

    QAction *update = new QAction(tr("Update"), this);
    help->addAction(update);
    connect(update, &QAction::triggered, new CheckUpdate(), &CheckUpdate::start);

    QAction *contact = new QAction(tr("Contact"), this);
    help->addAction(contact);
    connect(contact, &QAction::triggered, this, &MainWindow::contact);

    QAction *translate = new QAction(tr("Translate"), this);
    help->addAction(translate);
    connect(translate, &QAction::triggered, this, [=](){  QDesktopServices::openUrl(QUrl("https://gitlab.com/sergitab94/impulse")); });

    QAction *about = new QAction(tr("About"), this);
    help->addAction(about);
    connect(about, &QAction::triggered, this, [=](){InfoDialog debug(this);} );

    msgWarning.setIcon(QMessageBox::Warning);
    msgWarning.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    msgWarning.setWindowTitle(tr("Warning"));
    msgWarning.addButton(tr("Close program"), QMessageBox::DestructiveRole);
    connect(&msgWarning, &QMessageBox::buttonClicked, this, &MainWindow::closeApp);

    msgFile.setIcon(QMessageBox::Warning);
    msgFile.setWindowTitle(tr("Warning"));
    msgFile.setText(tr("File not available. Task deleted."));
}

/**
 * @brief MainWindow::createTab Generate all tabs availables and subMenus
 * @param menu Where it generate the sub-menus
 * @param type Type the task
 * @param blue Configuration bluetooth
 * @param ntab List of task visibles
 */
void MainWindow::createTab(QMenu *menu, TypeTask type, QAction *blue, QList<TypeTask> ntab){
    bool stat=ntab.removeOne(type);
    QAction *subMenu = new QAction(Task::taskToStr(type), this);
    subMenu->setObjectName(QString("%1").arg(type));
    subMenu->setCheckable(true);
    subMenu->setChecked(stat);
    menu->addAction(subMenu);
    connect(subMenu, &QAction::triggered, this, &MainWindow::tabShows);
    if(stat){
        tabW->addTab(new Tab(tabT, type, blue, this), Task::taskToStr(type));
        tabL->addTab(type);
    }
}
