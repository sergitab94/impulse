/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef DIALOGWINDOW_H
#define DIALOGWINDOW_H

#include <QDialog>
#include "mainwindow.h"

/**
 * @class InfoDialog
 * @brief The InfoDialog class show the information of application
 */
class InfoDialog : public QDialog {
    Q_OBJECT

public:
    InfoDialog(MainWindow *win);

private slots:
    void toDebug();

private:
    int debugClick=10;
    MainWindow *window;
};

#endif // DIALOGWINDOW_H
