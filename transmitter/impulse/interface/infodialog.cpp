/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "infodialog.h"
#include "debugwindow.h"
#include <QGridLayout>

/**
 * @brief InfoDialog::InfoDialog Create a Dialog with all information of "About"
 * @param win It is the mainwindow
 */
InfoDialog::InfoDialog(MainWindow *win) : QDialog(win) {
    window=win;
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    setWindowTitle(tr("About"));
    QGridLayout gLayout;


    QPixmap pixmap(":/img/logo");
    QIcon ButtonIcon(pixmap);
    QPushButton *button = new QPushButton(ButtonIcon, "");
    button->setIconSize(QSize(90, (90*pixmap.rect().size().height())/pixmap.rect().size().width()));
    button->setFlat(true);
    connect(button, &QPushButton::clicked, this, &InfoDialog::toDebug);
    gLayout.addWidget(button, 0, 0, 3, 1);

    QLabel *title = new QLabel(tr("About the application"));
    QFont font = title->font();
    font.setPointSize(20);
    font.setBold(true);
    title->setFont(font);
    gLayout.addWidget(title, 0, 1, 1, 2);

    QLabel *autorLabel = new QLabel(tr("Author") + ": Sergio Tabarés Medina");
    gLayout.addWidget(autorLabel, 1, 1);

    QLabel *versionLabel = new QLabel(tr("Version") + ": " + VERSION_STRING);
    versionLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    versionLabel->setAlignment(Qt::AlignRight);
    gLayout.addWidget(versionLabel, 1, 2);

    QString data;
    QFile def(":/tr/about.lic");
    def.open(QIODevice::ReadOnly);
    data=def.readAll();
    def.close();

    QLabel *licenses = new QLabel(data);
    licenses->setTextFormat(Qt::TextFormat::RichText);
    licenses->setOpenExternalLinks(true);
    licenses->setWordWrap(true);
    licenses->setContentsMargins(5, 0, 5, 0);

    QScrollArea *scrollArea = new QScrollArea();
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setWidgetResizable(true);
    scrollArea->setWidget(licenses);

    gLayout.addWidget(scrollArea, 2, 1, 1, 2);

    setMinimumSize(480, 220);
    setMaximumSize(480, 220);

    setLayout(&gLayout);
    exec();
}

/**
 * @brief InfoDialog::toDebug Enable enter in mode Debug
 */
void InfoDialog::toDebug(){
    if(debugClick>1){
        debugClick--;
        return;
    }
    DebugWindow *wdg = new DebugWindow(window);
    Pulse::getInstance()->disconn();
    wdg->show();
    window->close();
    close();
}

