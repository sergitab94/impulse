/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QObject>

#include "tab.h"
#include "tabtask.h"

#include "others/settings.h"
#include "others/tablist.h"
#include "others/checkupdate.h"

#include "pulse/pulse.h"
#include "pulse/task.h"

typedef Task::TypeTask TypeTask;
typedef DeviceException::PulseError PulseError;


/**
 * @class MainWindow
 * @brief The MainWindow class create the main window. It is
 * the first window that the user see.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();
    bool getNight(){ return sett->night; }
    QString getDefaultStyle(){ return defaultStyle; }
    static void multipleInstance();

public slots:
    void night(bool check);

private slots:
    void deviceList();
    void tabShows();
    void contact();
    void closeApp();

    void connectDevice(bool check);
    void waiting(bool wait);
    void error(DeviceException error);
    void deleteTask(int task);

private:
    Settings *sett;
    QString defaultStyle;
    QMenu *deviceAct;
    QAction *blue;
    TabList *tabL;
    QTabWidget *tabW;
    TabTask *tabT;
    QLabel *state;

    QThread *thError=nullptr;
    QMessageBox msgWarning;
    QMessageBox msgFile;

    void createInterface();
    void createTab(QMenu *menu, TypeTask type, QAction *bluetooth, QList<TypeTask> ntab);
};

#endif // MAINWINDOW_H
