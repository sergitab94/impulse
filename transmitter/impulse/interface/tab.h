/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef TABWIDGET_H
#define TABWIDGET_H

#include <QDialog>
#include <QtWidgets>
#include "pulse/task.h"
#include "tabtask.h"

typedef Task::TypeTask TypeTask;
typedef Task::TypeSec TypeSec;

/**
 * @brief The Tab class create all tabs of different Tasks
 */
class Tab : public QWidget{
    Q_OBJECT

public:
    Tab(TabTask *task, TypeTask type, QAction * blue, QWidget *parent=nullptr);
    ~Tab();

private:
    //Local elements
    QLabel *error;
    QCheckBox *repeat;
    QPushButton *send;

    //External Layout
    QWidget *parent;
    QGridLayout *layout;
    TabTask *tabTask;
    QAction *blue;

    //Patterns
    static QRegularExpression redable, print, numSpace, email, wallet;

    QHBoxLayout* sendTask();
    void pFile();
    void pMessage();
    void pWifi();
    void pSms();
    void pEmail();
    void pBitcoin();

private slots:
    void sFile();
    void sMessage();
    void sChangeCombo(int index);
    void sWifi();
    void sSms();
    void sEmail();
    void sBitcoin();
};

#endif // TABWIDGET_H
