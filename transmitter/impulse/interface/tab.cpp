/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "tab.h"
/**
 * @brief Initialize patterns
 */
QRegularExpression Tab::redable("[\\x00-\\x1F\\x7F\\xA0]");
QRegularExpression Tab::print("[^[:print:]]");
QRegularExpression Tab::numSpace("[^0-9\\+ ]");
QRegularExpression Tab::email("^[0-9a-z\\._-]+@[a-z0-9\\.-_]+\\.[a-z]{2,}");
QRegularExpression Tab::wallet("^[a-zA-Z0-9]{14,74}$");

/**
 * @brief Tab::Tab Constructor of the class
 * @param task Task to create of tab
 * @param type Type of task to create
 * @param blue Action of settings bluetooth
 * @param parent Parent of object
 */
Tab::Tab(TabTask *task, TypeTask type, QAction *blue, QWidget *parent) : QWidget(parent){
    tabTask=task;
    this->blue=blue;
    switch(type){
        case TypeTask::File: pFile(); break;
        case TypeTask::Message: pMessage(); break;
        case TypeTask::Wifi: pWifi(); break;
        case TypeTask::Sms: pSms(); break;
        case TypeTask::Email: pEmail(); break;
        case TypeTask::Bitcoin: pBitcoin(); break;
    }
}

/**
 * @brief Tab::~Tab Destruct object
 */
Tab::~Tab(){
    delete error;
    delete repeat;
    delete send;
}

/**
 * @brief Tab::sendTask Create the basic objects to interact with the Task
 * @return Layout with differnts buttons
 */
QHBoxLayout* Tab::sendTask(){
    QHBoxLayout *action = new QHBoxLayout;

    error = new QLabel;
    QFont myFont;
    myFont.setBold(true);
    error->setFont(myFont);
    QPalette palette = error->palette();
    palette.setColor(error->foregroundRole(), Qt::red);
    error->setPalette(palette);
    error->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    action->addWidget(error);

    repeat=new QCheckBox(tr("Repeat"));
    repeat->setChecked(false);
    repeat->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
    action->addWidget(repeat);

    send=new QPushButton(tr("Send"));
    send->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Maximum);
    action->addWidget(send);
    return action;
}

/**
 * @brief Tab::pFile Create interface of task File
 */
void Tab::pFile(){
    layout = new QGridLayout;
    setLayout(layout);

    QLineEdit *path = new QLineEdit;
    path->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    path->setReadOnly(true);
    layout->addWidget(path, 0, 0, Qt::AlignBottom);    

    QPushButton *file = new QPushButton(tr("Open…"));
    layout->addWidget(file, 0, 1, Qt::AlignBottom);
    connect(file, &QPushButton::clicked, this, [=](){
        path->setText(QFileDialog::getOpenFileName(this, tr("Open File"), ".", tr("All files (*.*)")));
    });

    layout->addLayout(sendTask(), 1, 0, 1, 2, Qt::AlignBottom | Qt::AlignRight);
    connect(send, &QPushButton::clicked, this, &Tab::sFile);
}

/**
 * @brief Tab::sFile Slot executed by pressing "send" in the Task File
 */
void Tab::sFile(){
    //Obtain
    QLineEdit *path = (QLineEdit*) layout->itemAtPosition(0, 0)->widget();
    QFile *file;
    if (path->text()==""){
        error->setText(tr("Error") + ": " + tr("File not available"));
        return;
    }
    file = new QFile(path->text());

    //Check
    if(!file->open(QFile::ReadOnly)) {
        error->setText(tr("Error") + ": " + tr("File not available"));
        return;
    }

    //Clear all
    path->clear();
    error->clear();

    //Create task
    Task *task = new Task();
    task->setFile(file, repeat->isChecked(), blue->isChecked());
    tabTask->addTask(task);
}

/**
 * @brief Tab::pMessage Create interface of task Message
 */
void Tab::pMessage(){
    layout=new QGridLayout;
    setLayout(layout);

    QLabel *lMessage = new QLabel(tr("Message") + ":");
    layout->addWidget(lMessage, 0, 0);

    QPlainTextEdit  *message = new QPlainTextEdit;
    layout->addWidget(message, 1, 0);

    layout->addLayout(sendTask(), 2, 0, Qt::AlignBottom | Qt::AlignRight);
    connect(send, &QPushButton::clicked, this, &Tab::sMessage);
}

/**
 * @brief Tab::sMessage Slot executed by pressing "send" in the Task Message
 */
void Tab::sMessage(){
    //Obtain
    QPlainTextEdit *message = (QPlainTextEdit*) layout->itemAtPosition(1,0)->widget();
    QString messagePlain = message->toPlainText();

    //Check
    if(messagePlain==""){
        error->setText(tr("Error") + ": " + tr("Message is void"));
        return;
    }
    if(messagePlain.contains(redable)) {
        error->setText(tr("Error") + ": " + tr("Only readable characters"));
        return;
    }

    //Clear all
    message->clear();
    error->clear();

    //Create task
    Task *task = new Task();
    task->setMessage(messagePlain, repeat->isChecked(), blue->isChecked());
    tabTask->addTask(task);
}

/**
 * @brief Tab::pWifi Create interface of task Wifi
 */
void Tab::pWifi(){
    layout = new QGridLayout;
    setLayout(layout);

    QLabel *lname = new QLabel(tr("Name") + ":");
    layout->addWidget(lname, 0, 0);

    QLineEdit *name = new QLineEdit;
    layout->addWidget(name, 0, 1);

    QLabel *ltype = new QLabel(tr("Type") + ":");
    layout->addWidget(ltype, 1, 0);

    QComboBox *type = new QComboBox;
    type->insertItem(0, tr("WPA 2"), TypeSec::WPA2);
    type->insertItem(1, tr("WPA 3"), TypeSec::WPA3);
    type->insertItem(2, tr("Without password"), TypeSec::OPEN);
    layout->addWidget(type, 1, 1);
    connect(type, SIGNAL(currentIndexChanged(int)), this, SLOT(sChangeCombo(int)));

    QCheckBox *hidden = new QCheckBox(tr("Hidden"));
    layout->addWidget(hidden, 1, 2);

    QLabel *lpass = new QLabel(tr("Password") + ":");
    layout->addWidget(lpass, 2, 0);

    QLineEdit *pass = new QLineEdit;
    pass->setEchoMode(QLineEdit::Password);
    layout->addWidget(pass, 2, 1);

    QLabel *lrpass = new QLabel(tr("Repeat password") + ":");
    layout->addWidget(lrpass, 2, 2);

    QLineEdit *rpass = new QLineEdit;
    rpass->setEchoMode(QLineEdit::Password);
    layout->addWidget(rpass, 2, 3);

    layout->addLayout(sendTask(), 3, 0, 1, 4, Qt::AlignBottom | Qt::AlignRight);
    connect(send, &QPushButton::clicked, this, &Tab::sWifi);
}

/**
 * @brief Tab::sChangeCombo enable or disable the lines that contain the password,
 * depending on the security chosen
 * @param index Position of the Combo selected
 */
void Tab::sChangeCombo(int index){
    QLineEdit *pass = (QLineEdit*) layout->itemAtPosition(2, 1)->widget();
    QLineEdit *rpass = (QLineEdit*) layout->itemAtPosition(2, 3)->widget();
    if(index==2){
        pass->clear();
        rpass->clear();
    }
    pass->setEnabled(index!=2);
    rpass->setEnabled(index!=2);
}

/**
 * @brief Tab::sWifi Slot executed by pressing "send" in the Task Wifi
 */
void Tab::sWifi(){
    //Obtain
    QLineEdit *name = (QLineEdit*) layout->itemAtPosition(0, 1)->widget();
    QComboBox *unit = (QComboBox*) layout->itemAtPosition(1, 1)->widget();
    QCheckBox *hidd = (QCheckBox*) layout->itemAtPosition(1, 2)->widget();
    QLineEdit *pass = (QLineEdit*) layout->itemAtPosition(2, 1)->widget();
    QLineEdit *rpass = (QLineEdit*) layout->itemAtPosition(2, 3)->widget();

    QString nameT = name->text();
    TypeSec typeSec  = (TypeSec) unit->itemData(unit->currentIndex()).toInt();
    bool hiddT = hidd->isChecked();
    QString passT = pass->text();
    QString rpassT = rpass->text();

    //Check
    if(nameT==""){
        error->setText(tr("Error") + ": " + tr("Name is void"));
        return;
    }

    if(nameT.contains(redable)) {
        error->setText(tr("Error") + ": " + tr("Name only readable characters"));
        return;
    }

    if(typeSec!=TypeSec::OPEN){
        if(passT.length()<8){
            error->setText(tr("Error") + ": " + tr("Password too short (at least 8 character)"));
            return;
        }

        if(passT!=rpassT){
            error->setText(tr("Error") + ": " + tr("Passwords not equal"));
            return;
        }

        if(passT.contains(print)){
            error->setText(tr("Error") + ": " + tr("Password only accepts printable ASCII characters"));
            return;
        }
    }

    //Clear all
    name->clear();
    unit->setCurrentIndex(0);
    hidd->setChecked(false);
    pass->clear();
    rpass->clear();
    error->clear();


    //Create task
    Task *task = new Task();
    task->setWifi(nameT, typeSec, hiddT, passT, repeat->isChecked(), blue->isChecked());
    tabTask->addTask(task);
}

/**
 * @brief Tab::pSms Create interface of task Sms
 */
void Tab::pSms(){
    layout = new QGridLayout;
    setLayout(layout);

    QLabel *lphone = new QLabel(tr("Phone")+":");
    lphone->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(lphone, 0, 0);

    QLineEdit *phone = new QLineEdit;
    phone->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(phone, 0, 1);

    QLabel *lmessage = new QLabel(tr("Message")+":");
    lmessage->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(lmessage, 1, 0, Qt::AlignTop);

    QTextEdit *message = new QTextEdit;
    message->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout->addWidget(message, 1, 1);

    layout->addLayout(sendTask(), 2, 0, 1, 2, Qt::AlignBottom | Qt::AlignRight);
    connect(send, &QPushButton::clicked, this, &Tab::sSms);
}

/**
 * @brief Tab::sSms Slot executed by pressing "send" in the Task Sms
 */
void Tab::sSms(){
    //Obtain
    QLineEdit *phone = (QLineEdit*) layout->itemAtPosition(0, 1)->widget();
    QTextEdit *message = (QTextEdit*) layout->itemAtPosition(1, 1)->widget();

    QString phoneT = phone->text();
    QString messageT = message->toPlainText();

    //Check
    if(phoneT==""){
        error->setText(tr("Error") + ": " + tr("Phone is void"));
        return;
    }

    if(phoneT.contains(numSpace)){
        error->setText(tr("Error") + ": " + tr("Phone invalid"));
        return;
    }

    if(messageT==""){
        error->setText(tr("Error") + ": " + tr("Message is void"));
        return;
    }

    if(messageT.contains(print)){
        error->setText(tr("Error") + ": " + tr("Message only accepts printable ASCII characters"));
        return;
    }

    //Clear all
    phone->clear();
    message->clear();
    error->clear();

    //Create task
    Task *task = new Task();
    task->setSms(phoneT, messageT, repeat->isChecked(), blue->isChecked());
    tabTask->addTask(task);
}

/**
 * @brief Tab::pEmail Create interface of task Email
 */
void Tab::pEmail(){
    layout = new QGridLayout;
    setLayout(layout);

    QLabel *lto = new QLabel(tr("To") + ":");
    layout->addWidget(lto, 0, 0);

    QLineEdit *to = new QLineEdit;
    to->setContentsMargins(0, 0, 5, 0);
    layout->addWidget(to, 0, 1);

    QLabel *lcc = new QLabel(tr("Cc") + ":");
    lcc->setContentsMargins(5, 0, 0, 0);
    layout->addWidget(lcc, 0, 2);

    QLineEdit *cc = new QLineEdit;
    layout->addWidget(cc, 0, 3);

    QLabel *lsub = new QLabel(tr("Subject") + ":");
    layout->addWidget(lsub, 1, 0);

    QLineEdit *sub = new QLineEdit;
    sub->setContentsMargins(0, 0, 5, 0);
    layout->addWidget(sub, 1, 1);

    QLabel *lbcc = new QLabel(tr("Bcc") + ":");
    lbcc->setContentsMargins(5, 0, 0, 0);
    layout->addWidget(lbcc, 1, 2);

    QLineEdit *bcc = new QLineEdit;
    layout->addWidget(bcc, 1, 3);

    QLabel *lmessage = new QLabel(tr("Message") + ":");
    layout->addWidget(lmessage, 2, 0, Qt::AlignTop);

    QTextEdit *message = new QTextEdit;
    layout->addWidget(message, 2, 1, 1, 3);

    layout->addLayout(sendTask(), 3, 0, 1, 4, Qt::AlignBottom | Qt::AlignRight);
    connect(send, &QPushButton::clicked, this, &Tab::sEmail);
}

/**
 * @brief Tab::sEmail Slot executed by pressing "send" in the Task Email
 */
void Tab::sEmail(){
    //Obtain
    QLineEdit *to = (QLineEdit*) layout->itemAtPosition(0, 1)->widget();
    QLineEdit *cc = (QLineEdit*) layout->itemAtPosition(0, 3)->widget();
    QLineEdit *sub = (QLineEdit*) layout->itemAtPosition(1, 1)->widget();
    QLineEdit *bcc = (QLineEdit*) layout->itemAtPosition(1, 3)->widget();
    QTextEdit *message = (QTextEdit*) layout->itemAtPosition(2, 1)->widget();

    QString toT=to->text().toLower(), ccT=cc->text().toLower(), subT=sub->text().toLower();
    QString bccT=bcc->text().toLower(), messageT=message->toPlainText();

    //Check

    if(toT==""){
        error->setText(tr("Error") + ": " + tr("To is void"));
        return;
    }

    QStringList list = toT.split(',');
    for(int i=0; i<list.size(); i++){
        if(!list[i].contains(email)){
            error->setText(tr("Error") + ": " + tr("To no contain a e-mail (of various, separate for ',')"));
            return;
        }
    }

    if(ccT!=""){
        QStringList list = ccT.split(',');
        for(int i=0; i<list.size(); i++){
            if(!list[i].contains(email)){
                error->setText(tr("Error") + ": " + tr("Cc not contain e-mail (of various, separate for ',')"));
                return;
            }
        }
    }

    if(subT.contains(redable)) {
        error->setText(tr("Error") + ": " + tr("Subject only accepts readable characters"));
        return;
    }

    if(bccT!=""){
        QStringList list = bccT.split(',');
        for(int i=0; i<list.size(); i++){
            if(!list[i].contains(email)){
                error->setText(tr("Error") + ": " + tr("Bcc not contain e-mail (of various, separate for ',')"));
                return;
            }
        }
    }

    if(messageT==""){
        error->setText(tr("Error") + ": " + tr("Message is void"));
        return;
    }

    if(messageT.contains(redable)) {
        error->setText(tr("Error") + ": " + tr("Message only accepts readable character"));
        return;
    }

    //Clear all
    to->clear();
    cc->clear();
    sub->clear();
    bcc->clear();
    message->clear();
    error->clear();

    //Create task
    Task *task = new Task();
    task->setEmail(toT, ccT, subT, bccT, messageT, repeat->isChecked(), blue->isChecked());
    tabTask->addTask(task);
}

/**
 * @brief Tab::pBitcoin Create interface of task Bitcoin
 */
void Tab::pBitcoin(){
    layout = new QGridLayout;
    setLayout(layout);

    QLabel *laddress = new QLabel(tr("Recipient")+":");
    laddress->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(laddress, 0, 0);

    QLineEdit *address = new QLineEdit;
    address->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    layout->addWidget(address, 0, 1, 1, 3);

    QLabel *lmessage = new QLabel(tr("Description") + ":");
    layout->addWidget(lmessage, 1, 0, Qt::AlignTop);
    QLineEdit *message = new QLineEdit;
    layout->addWidget(message, 1, 1, 1, 3);

    QLabel *lbitcoin = new QLabel(tr("Bitcoins")+":");
    lbitcoin->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(lbitcoin, 2, 0);

    QDoubleSpinBox *bitcoin = new QDoubleSpinBox;
    bitcoin->setDecimals(2);
    bitcoin->setMaximum(1000000);
    bitcoin->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
    bitcoin->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(bitcoin, 2, 1);

    QComboBox *unit = new QComboBox;
    unit->insertItem(0, tr("BTC"),  1);
    unit->insertItem(1, tr("dBTC (1⁻¹)"), 1E-1);
    unit->insertItem(2, tr("cBTC (1⁻²)"), 1E-2);
    unit->insertItem(3, tr("mBTC (1⁻³)"), 1E-3);
    unit->insertItem(4, tr("μBTC (1⁻⁶)"), 1E-6);
    unit->setCurrentIndex(4);
    unit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout->addWidget(unit, 2, 2, 1, 2);

    layout->addLayout(sendTask(), 4, 0, 1, 4, Qt::AlignBottom | Qt::AlignRight);
    connect(send, &QPushButton::clicked, this, &Tab::sBitcoin);
}

/**
 * @brief Tab::sBitcoin Slot executed by pressing "send" in the Task Bitcoin
 */
void Tab::sBitcoin(){
    //Obtain
    QLineEdit *address = (QLineEdit*) layout->itemAtPosition(0, 1)->widget();
    QLineEdit *message = (QLineEdit*)  layout->itemAtPosition(1, 1)->widget();
    QDoubleSpinBox *number = (QDoubleSpinBox*) layout->itemAtPosition(2, 1)->widget();
    QComboBox *unit = (QComboBox*) layout->itemAtPosition(2, 2)->widget();

    double amount = unit->itemData(unit->currentIndex()).toDouble()*number->value();
    QString addressT = address->text(), messageT = message->text();

    //Check
    if(!addressT.contains(wallet)){
        error->setText(tr("Error") + ": " + tr("Recipient invalid"));
        return;
    }

    if(messageT.contains(print)){
        error->setText(tr("Error") + ": " + tr("Description only accepts printable ASCII characters"));
        return;
    }

    if(amount==0){
        error->setText(tr("Error") + ": " + tr("Amount invalid"));
        return;
    }

    //Clear all
    address->clear();
    message->clear();
    number->setValue(0);
    unit->setCurrentIndex(4);
    error->clear();

    Task *task = new Task();
    task->setBitcoin(addressT, amount, messageT, repeat->isChecked(), blue->isChecked());
    tabTask->addTask(task);
}
