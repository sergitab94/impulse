/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef TABTASK_H
#define TABTASK_H

#include <QDialog>
#include <QTreeWidget>
#include "pulse/task.h"
#include "pulse/pulse.h"

/**
 * @brief The TabTask class is a list the task pending or executed
 */
class TabTask : public QWidget{
    Q_OBJECT

public:
    TabTask(QTabWidget *tabW, QWidget *parent=nullptr);
    ~TabTask();
    void addTask(Task *task);
    void deleteTask(int task);
    void clear();

private slots:
    void progressTask(int index, float progress);
    void updateTask(QTreeWidgetItem *item, int column);
    void deleteSelectedTask();

private:
    QTreeWidget *treeWidget;
    QPushButton *del;
    Pulse *pulse;
    QTabWidget *tabW;
};

#endif // TABTASK_H
