/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "tabtask.h"

#include <QtWidgets>
#include "taskwidgetdelegate.h"

/**
 * @brief TabTask::TabTask Creator of class
 * @param tabW Task to create of tab
 * @param parent Parent of object
 */
TabTask::TabTask(QTabWidget *tabW, QWidget *parent) : QWidget(parent), tabW(tabW){
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);

    QStringList headers;
    headers << tr("Type") << tr("Repeat") << tr("Progress");

    treeWidget = new QTreeWidget(this);
    treeWidget->setItemDelegate(new TaskWidgetDelegate(this));
    treeWidget->setHeaderLabels(headers);
    treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    treeWidget->setRootIsDecorated(false);
    treeWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QHeaderView *header = treeWidget->header();
    header->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    header->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    header->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    layout->addWidget(treeWidget, 0);
    connect(treeWidget, &QTreeWidget::itemChanged, this, &TabTask::updateTask);

    del = new QPushButton;
    del->setIcon(QIcon::fromTheme(":/qt-project.org/styles/commonstyle/images/standardbutton-delete-128.png"));
    del->setDisabled(true);
    layout->addWidget(del, 0, Qt::AlignRight);
    connect(del, &QPushButton::clicked, this, &TabTask::deleteSelectedTask);

    pulse=Pulse::getInstance();
}

/**
 * @brief TabTask::~TabTask Destruct of the object
 */
TabTask::~TabTask(){
    delete treeWidget;
    delete del;
}

/**
 * @brief TabTask::addTask Add task to the interface
 * @param task The Task to add
 */
void TabTask::addTask(Task *task) {
    QTreeWidgetItem *item = new QTreeWidgetItem();

    item->setText(0, task->getType());
    (task->getRepeat()) ? item->setCheckState(1, Qt::Checked) : item->setCheckState(1, Qt::Unchecked);
    item->setText(2, "0");

    treeWidget->addTopLevelItem(item);
    treeWidget->setCurrentItem(item);
    tabW->setCurrentIndex(0);
    connect(task, &Task::progress, this, &TabTask::progressTask);
    del->setDisabled(false);
    pulse->addTask(task);
}

/**
 * @brief TabTask::clear Clear all Tasks of the interface
 */
void TabTask::clear(){
    treeWidget->clear();
    tabW->setCurrentIndex(0);
}

/**
 * @brief TabTask::deleteTask Delete a specific task
 * @param task Number to task to delete
 */
void TabTask::deleteTask(int task){
    del->setDisabled(false);
    QTreeWidgetItem *current=treeWidget->topLevelItem(task);
    delete current;
    if(treeWidget->currentItem()==nullptr) del->setDisabled(true);
}

/**
 * @brief TabTask::progressTask Update the progress of the task
 * @param index Task to update
 * @param progress Percent of the progress task
 */
void TabTask::progressTask(int index, float progress){
    QTreeWidgetItem *item=treeWidget->topLevelItem(index);
    if(item!=nullptr) item->setText(2, QString::number(progress));
}

/**
 * @brief TabTask::updateTask Update the repeat Task
 * @param item Item to update the repeat
 * @param column column to update
 */
void TabTask::updateTask(QTreeWidgetItem *item, int column){
    if(column!=1) return;
    int pos=treeWidget->indexOfTopLevelItem(item);
    bool check=item->checkState(1)==Qt::Checked;
    pulse->repeatTask(pos, check);
}

/**
 * @brief TabTask::deleteTask Delete a specifici task
 */
void TabTask::deleteSelectedTask(){
    del->setDisabled(true);
    int pos=treeWidget->indexOfTopLevelItem(treeWidget->currentItem());
    pulse->removeTask(pos);
}
