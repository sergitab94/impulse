/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "debugwindow.h"
#include "others/debugfile.h"

/**
 * @brief DebugWindow::DebugWindow Constructor
 * @param wMain It is the parent MainWindow
 */
DebugWindow::DebugWindow(MainWindow *wMain) : QMainWindow(){
    this->wMain=wMain;

    setWindowTitle(tr("Impulse")+ " - " +tr("Debug Window"));
    createMenus();

    QGridLayout *layout = new QGridLayout();
    QWidget* widget = new QWidget();
    widget->setLayout(layout);
    setCentralWidget(widget);
    createLayout(layout);

    QSize qsize(640,480);
    setMinimumSize(qsize);

    QList<QScreen*> screens = QGuiApplication::screens();
    QScreen* screen = screens.first();

    setGeometry(QStyle::alignedRect(
        Qt::LeftToRight, Qt::AlignCenter, qsize,
        screen->geometry()
    ));
}

/**
 * @brief DebugWindow::~DebugWindow Destruct the object
 */
DebugWindow::~DebugWindow(){
    delete aBar;
    delete ePng;
    delete bX;
    delete bY;
    delete bZ;
    delete cP;
    delete debugTab;
    delete tableView;
    delete textEdit;
}

/**
 * @brief DebugWindow::openFile Open a file received from device in format .csv
 */
void DebugWindow::openFile(){
    QVector<double> p, t, x, y, z;
    QString info;
    if(!DebugFile::openFile(this, &p, &t, &x, &y, &z, &info)) return errorFile();
    disabledElements(false);
    bX->setChecked(true);
    bY->setChecked(true);
    bZ->setChecked(true);
    addData(p, t, x, y, z, info);
}

/**
 * @brief DebugWindow::exportPNG Generate a image forom the data receive
 */
void DebugWindow::exportPNG(){
    QString pngFile = QFileDialog::getSaveFileName(this, tr("Export to .png"), "", "*.png");
    if(!pngFile.endsWith(".png")) pngFile.append(".png");
    if(pngFile!="") cP->savePng(pngFile, 0, 0, 20);
}

/**
 * @brief DebugWindow::goBack Close this window and open MainWindow
 */
void DebugWindow::goBack(){
    wMain->show();
    close();
    delete this;
}

/**
 * @brief DebugWindow::night Change mode Night
 * @param check True if power on night mode
 */
void DebugWindow::night(bool check){
    wMain->night(check);
    plotNight(check);
}

/**
 * @brief DebugWindow::changeXYZ Change visibility for axis X, Y and Z
 */
void DebugWindow::changeXYZ(){
    cP->graph(0)->setVisible(bX->isChecked());
    cP->graph(1)->setVisible(bY->isChecked());
    cP->graph(2)->setVisible(bZ->isChecked());
    cP->replot();
}

/**
 * @brief DebugWindow::setRangeX Set maximum range axis X of plot
 * @param range Range visible for the plot in axis X
 */
void DebugWindow::setRangeX(QCPRange range){
    cP->xAxis2->setRange(range);
    if(range.lower<xMinMax[0]) cP->xAxis->setRangeLower(xMinMax[0]);
    if(range.upper>xMinMax[1]) cP->xAxis->setRangeUpper(xMinMax[1]);
}

/**
 * @brief DebugWindow::setRangeY Set maximum range axis Y of plot
 * @param range Range visible for the plot in axis Y
 */
void DebugWindow::setRangeY(QCPRange range){
    cP->yAxis2->setRange(range);
    if(range.lower < yMinMax[0]) cP->yAxis->setRangeLower(yMinMax[0]);
    if(range.upper > yMinMax[1]) cP->yAxis->setRangeUpper(yMinMax[1]);
}

/**
 * @brief DebugWindow::createMenus Create top menus
 */
void DebugWindow::createMenus(){
    menuBar()->setContextMenuPolicy(Qt::PreventContextMenu);
    QMenu *file = menuBar()->addMenu(tr("File"));
    QAction *open = new QAction(tr("Open debug file"), this);
    connect(open, &QAction::triggered, this, &DebugWindow::openFile);
    file->addAction(open);

    ePng = new QAction(tr("Export to .png"), this);
    ePng->setDisabled(true);
    file->addAction(ePng);
    connect(ePng, &QAction::triggered, this, &DebugWindow::exportPNG);
    file->addSeparator();

    QAction *back = new QAction(tr("Back"));
    file->addAction(back);
    connect(back, &QAction::triggered, this, &DebugWindow::goBack);

    QMenu *settings = menuBar()->addMenu(tr("Settings"));
    QAction *night = new QAction(tr("Night mode"), this);
    night->setCheckable(true);
    night->setChecked(wMain->getNight());
    settings->addAction(night);
    connect(night, &QAction::triggered, this, &DebugWindow::night);

    aBar = addToolBar(tr("Actions"));
    aBar->setMovable(false);
    aBar->setContextMenuPolicy(Qt::PreventContextMenu);
    aBar->setStyle(QStyleFactory::create("Fusion"));
    aBar->setStyleSheet("QToolButton:checked{color: white}"
                        "QToolButton:checked#bX{background-color: blue;}"
                        "QToolButton:checked#bY{background-color: red;}"
                        "QToolButton:checked#bZ{background-color: rgb(0, 200, 0);}");

    bX = new QAction("X");
    bX->setCheckable(true);
    QFont font = bX->font();
    font.setPointSize(15);
    bX->setFont(font);
    bX->setToolTip(tr("Show/hide X coordinates"));
    aBar->addAction(bX);
    aBar->widgetForAction(bX)->setObjectName("bX");
    connect(bX, &QAction::triggered, this, &DebugWindow::changeXYZ);

    bY = new QAction("Y");
    bY->setCheckable(true);
    bY->setFont(font);
    bY->setToolTip(tr("Show/hide Y coordinates"));
    aBar->addAction(bY);
    aBar->widgetForAction(bY)->setObjectName("bY");
    connect(bY, &QAction::triggered, this, &DebugWindow::changeXYZ);

    bZ = new QAction("Z");
    bZ->setCheckable(true);
    bZ->setFont(font);
    bZ->setToolTip(tr("Show/hide Z coordinates"));
    aBar->addAction(bZ);
    aBar->widgetForAction(bZ)->setObjectName("bZ");
    connect(bZ, &QAction::triggered, this, &DebugWindow::changeXYZ);
}

/**
 * @brief DebugWindow::createLayout Generate Layout main for debug window
 * @param layout: Main Layout of the window
 */
void DebugWindow::createLayout(QGridLayout *layout){
    cP = new QCustomPlot();

    cP->addGraph();
    cP->addGraph();
    cP->addGraph();
    cP->xAxis2->setVisible(true);
    cP->yAxis2->setVisible(true);
    cP->xAxis2->setTickLabels(false);
    cP->yAxis2->setTickLabels(false);
    connect(cP->xAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(setRangeX(QCPRange)));
    connect(cP->yAxis, SIGNAL(rangeChanged(QCPRange)), this, SLOT(setRangeY(QCPRange)));
    cP->xAxis->setLabel(tr("Sample Nº"));
    cP->yAxis->setLabel(tr("MicroTeslas (µT)"));
    cP->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
    plotNight(wMain->getNight());
    layout->addWidget(cP, 0, 0, 1, 2);
    layout->setRowStretch(0, 3);

    debugTab = new DebugTable(this);
    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
    proxyModel->setSourceModel(debugTab);
    tableView = new QTableView;
    tableView->setModel(proxyModel);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    tableView->verticalHeader()->hide();
    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    tableView->setSortingEnabled(true);
    tableView->sortByColumn(0, Qt::AscendingOrder);
    layout->addWidget(tableView, 1, 0);
    layout->setRowStretch(1, 2);
    layout->setColumnStretch(0, 3);

    textEdit = new QTextEdit();
    textEdit->setReadOnly(true);
    layout->addWidget(textEdit, 1, 1);
    layout->setColumnStretch(1, 2);

    disabledElements(true);
}

/**
 * @brief DebugWindow::addData Add new data in vectors
 * @param p Position of data. The first is 0, second is 1...
 * @param t Timestamp in miliseconds that receive data
 * @param x Data from axis X
 * @param y Data from axis Y
 * @param z Data from axis Z
 * @param info Text plain with information of device
 */
void DebugWindow::addData(QVector<double> &p, QVector<double> &t, QVector<double> &x,
                             QVector<double> &y, QVector<double> &z, QString &info){
    cP->graph(0)->setData(p, x);
    cP->graph(1)->setData(p, y);
    cP->graph(2)->setData(p, z);
    bool temp;
    xMinMax[0]=*p.begin();
    xMinMax[1]=*p.rbegin();
    yMinMax[0]=0; yMinMax[1]=0;
    for(int i=0; i<3; i++){
        yMinMax[0]=qMin(cP->graph(i)->getValueRange(temp).lower, yMinMax[0]);
        yMinMax[1]=qMax(cP->graph(i)->getValueRange(temp).upper, yMinMax[1]);
    }
    yMinMax[0]--;
    yMinMax[1]++;
    cP->xAxis->setRange(xMinMax[0], xMinMax[1]);
    cP->yAxis->setRange(yMinMax[0], yMinMax[1]);
    changeXYZ();
    debugTab->removeAll();
    debugTab->allInput(t, x, y, z);
    tableView->resizeColumnsToContents();
    textEdit->clear();
    textEdit->insertPlainText(info);
}

/**
 * @brief DebugWindow::plotNight Change the plot in mode night
 * @param check If Mode night is power on it is true
 */
void DebugWindow::plotNight(bool check){
    QColor backg=Qt::white, primary=Qt::black;
    QColor x0=Qt::blue, x1=QColor(0, 0, 255, 80);
    QColor y0=Qt::red, y1=QColor(255, 0, 0, 80);
    QColor z0=QColor(0, 200, 0), z1=QColor(0, 200, 0, 80);

    if(check){
        aBar->setStyle(QStyleFactory::create("Fusion"));
        backg=QColor(65,65,65); primary=Qt::white;
        x0=QColor(50, 170, 255), x1=QColor(50, 170, 255, 20);
        y0=QColor(255, 70, 70), y1=QColor(255, 70, 70, 20);
        z0=QColor(10, 210, 70), z1=QColor(10, 210, 70, 20);
    }
    cP->graph(0)->setPen(QPen(x0));
    cP->graph(1)->setPen(QPen(y0));
    cP->graph(2)->setPen(QPen(z0));
    cP->graph(0)->setBrush(QBrush(x1));
    cP->graph(1)->setBrush(QBrush(y1));
    cP->graph(2)->setBrush(QBrush(z1));
    cP->xAxis->setBasePen(QPen(primary, 1));
    cP->yAxis->setBasePen(QPen(primary, 1));
    cP->xAxis->setTickPen(QPen(primary, 1));
    cP->yAxis->setTickPen(QPen(primary, 1));
    cP->xAxis->setSubTickPen(QPen(primary, 1));
    cP->yAxis->setSubTickPen(QPen(primary, 1));
    cP->xAxis->setTickLabelColor(primary);
    cP->yAxis->setTickLabelColor(primary);
    cP->xAxis->setLabelColor(primary);
    cP->yAxis->setLabelColor(primary);
    cP->xAxis2->setBasePen(QPen(primary, 1));
    cP->yAxis2->setBasePen(QPen(primary, 1));
    cP->xAxis2->setTickPen(QPen(primary, 1));
    cP->yAxis2->setTickPen(QPen(primary, 1));
    cP->xAxis2->setSubTickPen(QPen(primary, 1));
    cP->yAxis2->setSubTickPen(QPen(primary, 1));
    cP->setBackground(backg);
    cP->rescaleAxes();
    cP->xAxis->setRangeLower(xMinMax[0]);
    cP->xAxis2->setRangeLower(xMinMax[0]);
    cP->replot();
}

/**
 * @brief DebugWindow::disabledElements Disable elements cental of windows
 * @param state Disable buttons, plot, table and information
 */
void DebugWindow::disabledElements(bool state){
    ePng->setDisabled(state);
    bX->setDisabled(state);
    bY->setDisabled(state);
    bZ->setDisabled(state);
    cP->setDisabled(state);
    tableView->setDisabled(state);
    textEdit->setDisabled(state);
}

/**
 * @brief DebugWindow::errorFile Reset all and disable elements
 */
void DebugWindow::errorFile(){
    bX->setChecked(false);
    bY->setChecked(false);
    bZ->setChecked(false);
    cP->xAxis->setRangeLower(0);
    cP->xAxis->setRangeUpper(5);
    cP->yAxis->setRangeLower(0);
    cP->yAxis->setRangeUpper(5);
    changeXYZ();
    debugTab->removeAll();
    textEdit->clear();
    disabledElements(true);
    QMessageBox::critical(0, tr("Error"), tr("Error to charge file"));
}
