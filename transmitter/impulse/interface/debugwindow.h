/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef DEBUGWIDGET_H
#define DEBUGWIDGET_H

#include "qcustomplot/qcustomplot.h"
#include "interface/mainwindow.h"
#include "debugtable.h"


/**
 * @class DebugWindow
 * @brief The DebugWindow class creates a new window
 * that contains all the information to debug the protocol
 * with data from phone Android
 */
class DebugWindow : public QMainWindow {
    Q_OBJECT

public:
    DebugWindow(MainWindow *wMain);
    ~DebugWindow();

private slots:
    void openFile();
    void exportPNG();
    void goBack();
    void night(bool check);
    void changeXYZ();
    void setRangeX(QCPRange range);
    void setRangeY(QCPRange range);

private:
    MainWindow *wMain;
    QToolBar *aBar;
    QAction *ePng, *bX, *bY, *bZ;
    QCustomPlot *cP;
    DebugTable *debugTab;
    QTableView *tableView;
    QTextEdit *textEdit;

    double xMinMax[2]={0, 0};
    double yMinMax[2]={0, 0};

    void createMenus();
    void createLayout(QGridLayout *layout);
    void addData(QVector<double> &p, QVector<double> &t, QVector<double> &x,
                    QVector<double> &y, QVector<double> &z, QString &info);
    void plotNight(bool check);    
    void disabledElements(bool state);
    void errorFile();
};

#endif // DEBUGWIDGET_H
