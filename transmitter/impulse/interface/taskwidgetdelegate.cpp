/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "taskwidgetdelegate.h"

#include <QApplication>
#include <QStyleFactory>

/**
 * @brief TaskWidgetDelegate::paint Indicates how the bar should be painted
 * @param painter Renders the delegate using the given painter
 * @param option The style for the item
 * @param index Index of the item to paint
 */
void TaskWidgetDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const{
    if (index.column() != 2) {
         QItemDelegate::paint(painter, option, index);
         return;
    }

    QStyleOptionProgressBar progressBarOption;
    progressBarOption.state=QStyle::State_Enabled;
    progressBarOption.direction=QApplication::layoutDirection();
    progressBarOption.rect=option.rect;
    progressBarOption.fontMetrics = QApplication::fontMetrics();
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.textAlignment = Qt::AlignCenter;
    progressBarOption.textVisible = true;
    progressBarOption.styleObject=QStyleFactory::create("Fusion");

    float progress = index.data(Qt::DisplayRole).toFloat();
    progressBarOption.progress = progress < 0 ? 0 : qRound(progress);
    progressBarOption.text = QString::number(progress, 'f', 2) + "%";

    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);
}
