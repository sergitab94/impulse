/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#include "debugtable.h"

/**
 * @brief DebugTab::DebugTab Construct table
 * @param parent Object creator
 */
DebugTable::DebugTable(QObject *parent) : QAbstractTableModel(parent){}

/**
 * @brief DebugTab::headerData Generate headers
 * @param section Column to create
 * @param orientation Orientation of table
 * @param role Format rendered in the table.
 * @return Name of row
 */
QVariant DebugTable::headerData(int section, Qt::Orientation orientation, int role) const{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0: return tr("No.");
            case 1: return tr("Timestamp (ns)");
            case 2: return tr("X");
            case 3: return tr("Y");
            case 4: return tr("Z");
            default: break;
        }
    }
    return QVariant();
}

/**
 * @brief DebugTab::data Storage data
 * @param index Row to process
 * @param role Format rendered in the table.
 * @return Data information
 */
QVariant DebugTable::data(const QModelIndex &index, int role) const{
    if(!index.isValid()) return QVariant();

    if(index.row()>=t.size() || index.row()<0) return QVariant();

    if(role==Qt::DisplayRole) {
        int pos=index.row();

        switch (index.column()) {
            case 0: return pos;
            case 1: return QString::number(t[pos], 'd', 0);
            case 2: return x[pos];
            case 3: return y[pos];
            case 4: return z[pos];
            default: break;
        }
    }
    return QVariant();
}

/**
 * @brief DebugTab::rowCount Count row data
 * @param parent Row count if is valid the parent
 * @return Row size
 */
int DebugTable::rowCount(const QModelIndex &parent) const{
    return parent.isValid() ? 0 : t.size();
}

/**
 * @brief DebugTab::columnCount Count column data
 * @param parent Column count if is valid the parent
 * @return Column size
 */
int DebugTable::columnCount(const QModelIndex &parent) const{
    return parent.isValid() ? 0 : 5;
}

/**
 * @brief DebugTab::flags Show flags
 * @param index Row index check flags
 * @return Flag of the index
 */
Qt::ItemFlags DebugTable::flags(const QModelIndex &index) const{
    if (!index.isValid()) return Qt::ItemIsEnabled;
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

/**
 * @brief DebugTab::allInput Input all data in Vectors
 * @param t Vector with time imput
 * @param x Vector with microTeslas in axis X
 * @param y Vector with microTeslas in axis Y
 * @param z Vector with microTeslas in axis Z
 */
void DebugTable::allInput(const QVector<double> &t, const QVector<double> &x, const QVector<double> &y, const QVector<double> &z){
    if(t.isEmpty()) return;
    int len=t.size();
    this->t=t;
    if(x.size()!=len) return;
    this->x=x;
    if(y.size()!=len) return;
    this->y=y;
    if(z.size()!=len) return;
    this->z=z;

    beginInsertRows(QModelIndex(), 0, len-1);
    endInsertRows();
}

/**
 * @brief DebugTab::removeAll Delete all data from table
 */
void DebugTable::removeAll(){
    const int size=t.size();
    if(size==0) return;
    beginRemoveRows(QModelIndex(), 0, t.size()-1);
    t.clear();
    x.clear();
    y.clear();
    z.clear();
    endRemoveRows();
}
