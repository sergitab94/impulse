/***************************************************************************
**                                                                        **
**  Impulse                                                               **
**  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

#ifndef DEBUGTABLE_H
#define DEBUGTABLE_H

#include <QAbstractTableModel>
#include <QVector>


/**
 * @class DebugTable
 * @brief The DebugTable class create a table for show all data from file
 * debug create in Mode Debug in Android
 */
class DebugTable : public QAbstractTableModel {
    Q_OBJECT

public:
    DebugTable(QObject *parent = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    void allInput(const QVector<double> &t, const QVector<double> &x, const QVector<double> &y, const QVector<double> &z);
    void removeAll();

private:
    QVector<double> t, x, y, z;
};

#endif // DEBUGTABLE_H
