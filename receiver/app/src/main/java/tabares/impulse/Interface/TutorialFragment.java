/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Interface;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.viewpager.widget.ViewPager;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import tabares.impulse.Others.Dialogs;
import tabares.impulse.Others.Files;
import tabares.impulse.Others.ProgressDialog;
import tabares.impulse.Others.TutorialSlides;
import tabares.impulse.Others.TutorialViewPager;
import tabares.impulse.R;
import tabares.pulse.DeviceException;
import tabares.pulse.Listeners;
import tabares.pulse.Pulse;

public class TutorialFragment extends Fragment {
    private Button next, actions;
    private TutorialViewPager viewPager;
    private static boolean task = false;

    private final int[] color = {
        Color.rgb(181, 3,   26),
        Color.rgb(112, 17,  146),
        Color.rgb(0,   85,  212),
        Color.rgb(56,  142, 60),
        Color.rgb(174, 62,  10),
        Color.rgb(49,  49,  49),
        Color.rgb(41,  130, 135)
    };

    /**
     * Called to have the fragment instantiate its user interface view.
     * @param inflater The LayoutInflater object that can be used to inflate any views in the
     *                 fragment
     * @param container The fragment should not add the view itself, but this can be used to
     *                  generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous
     *                           saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    @SuppressLint({"SourceLockedOrientationActivity"})
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        ConstraintLayout rl = view.findViewById(R.id.menuTutorial);
        viewPager = view.findViewById(R.id.viewpager);
        LinearLayout dots_layout = view.findViewById(R.id.dotsLayout);
        next = view.findViewById(R.id.tutorialNext);
        actions = view.findViewById(R.id.action);
        Button previous = view.findViewById(R.id.tutorialPrevious);
        Button end = view.findViewById(R.id.tutorialEnd);
        ArgbEvaluator evaluator = new ArgbEvaluator();

        if (getContext()==null || getActivity() == null) return view;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        WindowCompat.setDecorFitsSystemWindows(getActivity().getWindow(), false);
        getActivity().getWindow().setStatusBarColor(Color.argb(0,0,0, 0));
        getActivity().getWindow().setNavigationBarColor(Color.argb(0,0,0, 0));

        ViewCompat.setOnApplyWindowInsetsListener(view, (v, windowInsets) -> {
            Insets insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemGestures());


            rl.setPadding(insets.left,insets.top, insets.right, insets.bottom);
            viewPager.setPadding(insets.left,insets.top, insets.right, insets.bottom);
            return WindowInsetsCompat.CONSUMED;
        });

        ActionBar aBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (aBar != null) aBar.hide();

        TutorialSlides tutorialSlides = new TutorialSlides(getContext());
        createDots(dots_layout, 0, tutorialSlides.getCount());
        viewPager.setAdapter(tutorialSlides);
        viewPager.setBlockPage(1);

        //Listeners
        int tot = tutorialSlides.getCount();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                int colorUpdate = (Integer) evaluator.evaluate(v, color[i], color[i==6 ? i : i+1]);
                viewPager.setBackgroundColor(colorUpdate);
            }

            @Override
            public void onPageSelected(int i) {
                createDots(dots_layout, i, tot);
                tutorialSlides.animatedItem(i);
                if (i==0) {
                    previous.setVisibility(View.INVISIBLE);
                    next.setVisibility(View.VISIBLE);
                    end.setVisibility(View.INVISIBLE);
                    actions.setVisibility(View.INVISIBLE);
                    viewPager.setListener(null);
                }else if(tot-1==i){
                    previous.setVisibility(View.VISIBLE);
                    next.setVisibility(View.INVISIBLE);
                    end.setVisibility(View.VISIBLE);
                    actions.setVisibility(View.INVISIBLE);
                    viewPager.setListener(() -> finishTutorial());
                }else{
                    previous.setVisibility(View.VISIBLE);
                    next.setVisibility(View.VISIBLE);
                    end.setVisibility(View.INVISIBLE);
                    actionsTutorial(i);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) { }
        });

        previous.setOnClickListener(v -> viewPager.setCurrentItem(viewPager.getCurrentItem()-1));
        next.setOnClickListener(v -> viewPager.setCurrentItem(viewPager.getCurrentItem()+1));
        end.setOnClickListener(v -> finishTutorial());

        return view;
    }

    /**
     * Create dots of menu and update
     * @param dots_layout Layout where the points are placed
     * @param position Position in the sliders
     * @param length Number of sliders
     */
    private void createDots(LinearLayout dots_layout, int position, int length){
        if (dots_layout != null) dots_layout.removeAllViews();
        ImageView[] dots = new ImageView[length];
        Context context = getContext();
        if (context == null) return;

        assert dots_layout != null;
        for(int i=0; i<length; i++ ){
            dots[i]= new ImageView(context);
            if (i==position) dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.tutorial_dots_active));
            else dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.tutorial_dots_default));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(8,0,8,0);
            dots_layout.addView(dots[i], params);
        }
    }

    // =============================================================================
    // ACTIONS TUTORIAL
    // =============================================================================

    /**
     * Actions of the tutorial
     * @param pos Position in the sliders
     */
    private void actionsTutorial(int pos){
        switch (pos){
        case 1:
            if(viewPager.getBlockPage()==1) {
                actions.setText(getString(R.string.check));
                next.setVisibility(View.INVISIBLE);
                actions.setVisibility(View.VISIBLE);
                actions.setOnClickListener(v -> check());
                viewPager.setListener(this::check);
                return;
            }
            break;
        case 2:
            if(ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                actions.setText(getString(R.string.grant));
                viewPager.setBlockPage(2);
                next.setVisibility(View.INVISIBLE);
                actions.setVisibility(View.VISIBLE);
                actions.setOnClickListener(v -> {
                    if (!task) {
                        task = true;
                        permissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
                    }
                });

                viewPager.setListener(() ->{
                    if (!task) {
                        task = true;
                        permissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
                    }
                });
                return;
            }
            break;
        case 3:
            if(Files.checkPermission(getContext())) {
                viewPager.setBlockPage(3);
                actions.setText(getString(R.string.grant));
                next.setVisibility(View.INVISIBLE);
                actions.setVisibility(View.VISIBLE);
                actions.setOnClickListener(v -> {
                    if (!task) {
                        task = true;
                        permissionStorage.launch(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE});
                    }
                });

                viewPager.setListener(() -> {
                    if (!task) {
                        task = true;
                        permissionStorage.launch(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE});
                    }
                });
                return;
            }
            break;
        case 4:
            if(!Files.checkPermission(getContext()) && Files.checkPermissionComplete(getContext())){
                Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                viewPager.setBlockPage(4);
                actions.setText(getString(R.string.select));

                next.setVisibility(View.INVISIBLE);
                actions.setVisibility(View.VISIBLE);
                actions.setOnClickListener(v -> {
                    if (!task) {
                        task = true;
                        saveResult.launch(i);
                    }
                });
                viewPager.setListener(() -> {
                    if (!task) {
                        task = true;
                        saveResult.launch(i);
                    }
                });
                return;
            }
            break;
        case 5:
            if(viewPager.getBlockPage()==5) {
                actions.setText(getString(R.string.store));
                next.setVisibility(View.INVISIBLE);
                actions.setVisibility(View.VISIBLE);
                actions.setOnClickListener(v -> marketBTC());
                viewPager.setListener(this::marketBTC);
                return;
            }
        }
        nextSlide(pos);
    }

    /**
     * Check if Android phone is compatible
     */
    private void check(){
        if(task) return;
        task = true;
        try {
            Pulse.start(getContext(), new Listeners.PulseListenerDebug() {
                @Override public void dataMagnetic(float[] data, long time) {}
                @Override public void error(DeviceException exception) {}
                @Override public void state(Pulse.StateDev state) {}
                @Override public void progress(float progress) {}
            });
        } catch (Exception exception) {
            exception.printStackTrace();
            Dialogs.newInstance(getParentFragmentManager(), "Incompatible",
                new String[]{getString(R.string.incompatible), getString(R.string.incompatibleT)},
                new Dialogs.listenerYesNo() {
                    @Override
                    public void yes() {
                        task = false;
                        check();
                    }

                    @Override
                    public void no() {
                        requireActivity().finish();
                        System.exit(0);
                    }
                });
            return;
        }

        ProgressDialog prg;
        try {
            prg = new ProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        prg.show(getParentFragmentManager(), "progress");
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        Runnable endTask = () -> {
            boolean compatible;
            Dialogs.listenerYesNo incL = new Dialogs.listenerYesNo() {
                @Override
                public void yes() {
                    task = false;
                    check();
                }

                @Override
                public void no() {
                    requireActivity().finish();
                    System.exit(0);
                }
            };
            String[] incS = new String[]{getString(R.string.incompatible), getString(R.string.incompatibleT)};

            try {
                compatible = Pulse.getStats().isCompatible();
                Pulse.stop();
            } catch (Exception e) {
                e.printStackTrace();
                Dialogs.newInstance(getParentFragmentManager(), "Incompatible", incS, incL);
                prg.dismiss();
                return;
            }
            prg.dismiss();

            if(compatible){
                nextSlide();
                Dialogs.newInstance(getParentFragmentManager(), "Compatible",
                    new String[]{getString(R.string.compatible), getString(R.string.compatibleT)});
                return;
            }

            Dialogs.newInstance(getParentFragmentManager(), "Incompatible", incS, incL);
        };

        executor.scheduleAtFixedRate(new Runnable() {
            private int n=0;
            private static final float time=20;

            @Override
            public void run() {
                int percent=(int) Math.floor((n/time)*100);
                new Handler(Looper.getMainLooper()).post(() -> prg.setProgress(percent));

                if(n>=time){
                    if(n==time) new Handler(Looper.getMainLooper()).post(endTask);
                    executor.shutdown();
                    return;
                }
                n++;
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    /**
     * Grant and dialog for the permission Bluetooth
     */
    private final ActivityResultLauncher<String> permissionBlue = registerForActivityResult(
        new ActivityResultContracts.RequestPermission(), (p) -> {
            if(p) {
                nextSlide();
                return;
            }

            Dialogs.listenerYesNo dialogL = new Dialogs.listenerYesNo() {
                @Override
                public void yes() {
                    requireActivity().finish();
                    System.exit(0);
                }

                @Override
                public void no() {
                    permissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
                }
            };

            Dialogs.newInstance(getParentFragmentManager(), "Permission",
                    new String[]{getString(R.string.grantNecessary), getString(R.string.grantBlue)}, dialogL);
        });

    /**
     * Grant and dialog for the permission Storage
     */
    private final ActivityResultLauncher<String[]> permissionStorage =
        registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), permissions -> {
            for (Map.Entry<String, Boolean> entry : permissions.entrySet()) {
                if(!entry.getKey().equals(Manifest.permission.READ_EXTERNAL_STORAGE)) continue;

                if(entry.getValue()) {
                    actions.setVisibility(View.INVISIBLE);
                    next.setVisibility(View.VISIBLE);
                    viewPager.setBlockPage(4);
                    task = false;
                    continue;
                }

                Dialogs.listenerYesNo dialogL = new Dialogs.listenerYesNo() {
                    @Override
                    public void yes() {
                        actions.setVisibility(View.INVISIBLE);
                        next.setVisibility(View.VISIBLE);
                        viewPager.setBlockPage(5);
                        task = false;
                    }

                    @Override
                    public void no() {
                        permissionStorage.launch(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                                              Manifest.permission.WRITE_EXTERNAL_STORAGE});
                    }
                };

                Dialogs.newInstance(getParentFragmentManager(), "Permission",
                    new String[]{getString(R.string.grantNecessary), getString(R.string.grantStorage)}, dialogL);

            }
        });

    /**
     * Grant and dialog for the permission Folder
     */
    private final ActivityResultLauncher<Intent> saveResult = registerForActivityResult(
        new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    assert result.getData() != null;
                    Uri treeUri = result.getData().getData();
                    int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    requireContext().getContentResolver().takePersistableUriPermission(treeUri, takeFlags);
                    Files.savePath(requireContext(), treeUri);
                    nextSlide();
                }
                task = false;
            });

    /**
     * Open Google Play for download Electrum
     */
    private void marketBTC(){
        if(task) return;
        task = true;
        Dialogs.listenerYesNo dialogL = new Dialogs.listenerYesNo() {
            @Override
            public void yes() {
                String appPackageName = "org.electrum.electrum";
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException ignored) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                nextSlide();
            }

            @Override
            public void no() {
                nextSlide();
            }
        };

        Dialogs.newInstance(getParentFragmentManager(), "market",
            new String[]{ getString(R.string.applicationBTC), getString(R.string.applicationBTCText) }, dialogL);
    }

    /**
     * End the tutorial and start previous
     */
    private void finishTutorial(){
        if (getContext() == null) return;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean firstTime = settings.getBoolean("firstTime", true);

        if (getActivity() == null) return;
        if(firstTime) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("firstTime", false);
            editor.apply();

            getActivity().startActivity(new Intent(getContext(), MainActivity.class));
        }
        getActivity().finish();
    }

    /**
     * Block the next slide
     */
    private void nextSlide(){
        actions.setVisibility(View.INVISIBLE);
        next.setVisibility(View.VISIBLE);
        viewPager.setBlockNext();
        task = false;
    }

    /**
     * Block the slide with the indicated number
     * @param pos Number maximum to block number
     */
    private void nextSlide(int pos){
        actions.setVisibility(View.INVISIBLE);
        next.setVisibility(View.VISIBLE);
        viewPager.setBlockNextPage(pos);
        task = false;
    }
}