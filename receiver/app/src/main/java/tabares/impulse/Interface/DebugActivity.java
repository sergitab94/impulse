/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Interface;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.ViewModelProvider;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.List;
import java.util.concurrent.Semaphore;

import tabares.impulse.Others.Dialogs;
import tabares.impulse.Others.Files;
import tabares.impulse.R;
import tabares.impulse.View.DebugView;
import tabares.pulse.DeviceException;
import tabares.pulse.Pulse;

public class DebugActivity extends AppCompatActivity {
    private final Semaphore mutex = new Semaphore(1);
    private DebugView debugView;
    private LineChart chart;

    /**
     * Create the menu of Activity and add listeners
     * @param menu Interface for managing the items in a menu.
     * @return You must return true for the menu to be displayed; if you return false it will not be
     * shown.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem delete = menu.findItem(R.id.action_delete);
        MenuItem save = menu.findItem(R.id.action_save);
        MenuItem stat = menu.findItem(R.id.action_stat);

        delete.setOnMenuItemClickListener(s -> debugView.remove());
        save.setOnMenuItemClickListener(s -> debugView.save());
        stat.setOnMenuItemClickListener(s -> debugView.stat());

        if (Files.checkPermissionComplete(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), getString(R.string.writeD), Toast.LENGTH_LONG).show();
            save.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * One options item is selected from menu
     * @param item The menu item that was selected. This value cannot be null.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId()==android.R.id.home) {
            debugView.stop();
            finish();
        }
        return false;
    }

    /**
     * Called when the activity has detected the user's press of the back key.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        debugView.stop();
    }

    /**
     * Called when the activity is starting.
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                          down then this Bundle contains the data it most recently supplied
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        debugView = new ViewModelProvider(this).get(DebugView.class);
        requestPermissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);

        TextView percent = findViewById(R.id.percent);
        SwitchCompat sX = findViewById(R.id.switchX);
        SwitchCompat sY = findViewById(R.id.switchY);
        SwitchCompat sZ = findViewById(R.id.switchZ);
        LinearLayout blueS = findViewById(R.id.blueSearch);
        LinearLayout blueC = findViewById(R.id.blueConnect);
        chart = findViewById(R.id.chart);

        //Configure Chart
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);
        chart.setTouchEnabled(false);
        chart.setDrawGridBackground(false);
        chart.getXAxis().setDrawLabels(false);
        chart.getDescription().setEnabled(true);
        chart.getDescription().setText(getString(R.string.descriptionGraphic));

        //Colors
        final TypedValue value = new TypedValue();
        getTheme().resolveAttribute(R.attr.inverseBackground, value, true);
        int color = value.data;
        chart.getAxisLeft().setTextColor(color);
        chart.getAxisRight().setTextColor(color);
        chart.getLegend().setTextColor(color);
        chart.getDescription().setTextColor(color);

        LineDataSet set0 = addDataset(String.format("%s %s →", getString(R.string.line), getString(R.string.debugX)), Color.rgb(0,0,255));
        LineDataSet set1 = addDataset(String.format("%s %s ↑", getString(R.string.line), getString(R.string.debugY)), Color.rgb(255,0,0));
        LineDataSet set2 = addDataset(String.format("%s %s ↗", getString(R.string.line), getString(R.string.debugZ)), Color.rgb(0,200,0));

        chart.setData(new LineData(set0, set1, set2));

        sX.setOnCheckedChangeListener((b, c) -> set0.setVisible(c));
        sY.setOnCheckedChangeListener((b, c) -> set1.setVisible(c));
        sZ.setOnCheckedChangeListener((b, c) -> set2.setVisible(c));

        debugView.getStateDev().observe(this, (blue) -> {
            boolean sw = false;
            if(blue == Pulse.StateDev.prepareBlue) {
                chart.setVisibility(View.GONE);
                blueS.setVisibility(View.VISIBLE);
                blueC.setVisibility(View.GONE);
            } else if (blue==Pulse.StateDev.receiveBlue) {
                chart.setVisibility(View.GONE);
                blueS.setVisibility(View.GONE);
                blueC.setVisibility(View.VISIBLE);
            } else {
                sw = true;
                chart.setVisibility(View.VISIBLE);
                blueS.setVisibility(View.GONE);
                blueC.setVisibility(View.GONE);
            }

            sX.setEnabled(sw);
            sY.setEnabled(sw);
            sZ.setEnabled(sw);
        });
        debugView.getProgress().observe(this, percent::setText);
        debugView.getListData().observe(this, this::updateDiagram);
        debugView.getError().observe(this, this::error);

        debugView.getDialog().observe(this, s -> {
            if(s!=null) {
                Dialogs.newInstance(getSupportFragmentManager(), "MessageDialog", s.toArray(new String[0]));
                debugView.resetDialog();
            }
        });
    }

    /**
     * Call when a error is made
     * @param e {@link DeviceException} received from {@link Pulse}
     */
    private void error(DeviceException e){
        if(e.getPulseError()==DeviceException.PulseError.DeviceOff) {
            debugView.stop();
            Dialogs.newInstance(getSupportFragmentManager(), "error",
                new String[] {getString(R.string.error), getString(R.string.blueOff)}, deviceBluetooth);
        }
    }

    // =============================================================================
    // DIAGRAMS
    // =============================================================================

    /**
     * Add new data to diagram
     * @param pulse Data from pulse
     */
    private void updateDiagram(DebugView.PulseData pulse){
        LineData data = chart.getData();
        if(data==null) return;
        try {
            mutex.acquire();
            ILineDataSet set = data.getDataSetByIndex(0);
            int count = set.getEntryCount();
            long info = pulse.getPos();


            List<ILineDataSet> listSets = data.getDataSets();
            for (int x = 0; x < listSets.size(); x++) {
                listSets.get(x).addEntry(new Entry(info, pulse.getInfo()[x]));
                if (count > 500) listSets.get(x).removeFirst();
            }

            data.notifyDataChanged();
            chart.notifyDataSetChanged();
            chart.setVisibleXRangeMaximum(500);
            chart.moveViewToX(info);
        } catch (InterruptedException ignored) {
        } finally {
            mutex.release();
        }
    }

    /**
     * Generate dataset to diagram when it is created
     * @param info Legend of the dataset
     * @param color Color in the diagram
     * @return The dataset already created
     */
    private LineDataSet addDataset(String info, @ColorInt int color){
        LineDataSet set = new LineDataSet(null, info);
        set.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        set.setDrawFilled(true);
        set.setDrawCircles(false);
        set.setLineWidth(1.8f);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(color);
        set.setFillColor(color);
        set.setDrawValues(false);
        return set;
    }

    // =============================================================================
    // POWER ON DEVICE
    // =============================================================================

    /**
     * Listener of Dialog for activate Bluetooth
     */
    private final Dialogs.listenerYesNo deviceBluetooth = new Dialogs.listenerYesNo() {
        @Override
        public void yes() {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            onBluetooth.launch(enableBtIntent);
        }

        @Override
        public void no() {
            finish();
        }
    };

    /**
     * Allows you to turn on the bluetooth
     */
    private final ActivityResultLauncher<Intent> onBluetooth = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if(result.getResultCode() != Activity.RESULT_OK) {
                    Dialogs.newInstance(getSupportFragmentManager(), "powerOn",
                        new String[]{getString(R.string.error), getString(R.string.blueOff)}, deviceBluetooth);
                } else debugView.start();
            });

    // =============================================================================
    // PERMISSIONS
    // =============================================================================

    /**
     * Listener of Dialog for grant permission of location
     */
    private final Dialogs.listenerYesNo permissionBlue = new Dialogs.listenerYesNo() {
        @Override
        public void yes() {
            requestPermissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        @Override
        public void no() {
            finish();
        }
    };

    /**
     * Allows you to grant permission of location
     */
    private final ActivityResultLauncher<String> requestPermissionBlue =
        registerForActivityResult(new ActivityResultContracts.RequestPermission(), p-> {
            if (p) debugView.start();
            else {
                debugView.stop();
                Dialogs.newInstance(getSupportFragmentManager(), "localization",
                    new String[]{getString(R.string.grantNecessary), getString(R.string.grantBlue)}, permissionBlue);
            }
        });
}