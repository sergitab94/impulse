/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Interface;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsetsController;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.WindowCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;

import java.text.DecimalFormat;

import tabares.impulse.Others.Dialogs;
import tabares.impulse.Others.RipplePulseLayout;
import tabares.impulse.Others.Wifi;
import tabares.impulse.R;

import tabares.impulse.View.PulseView;
import tabares.pulse.DeviceException;
import tabares.pulse.Pulse;

public class PulseFragment extends Fragment {
    private PulseView pulseView;
    private static boolean refreshTheme = false;
    private long time = 0;

    private static boolean permission=true;
    private static transient Toast toast;

    private RipplePulseLayout mRipple;
    private TextView textPulse;
    private ProgressBar progressBar;

    /**
     * Called to have the fragment instantiate its user interface view.
     * @param inflater The LayoutInflater object that can be used to inflate any views in the
     *                 fragment
     * @param container The fragment should not add the view itself, but this can be used to
     *                  generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous
     *                           saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_pulse, container, false);
        mRipple = view.findViewById(R.id.layoutRipplePulse);
        textPulse = view.findViewById(R.id.textPulse);
        progressBar = view.findViewById(R.id.progressPulse);
        return view;
    }

    /**
     * Called immediately after {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} has
     * returned, but before any saved state has been restored in to the view.
     * @param view The View returned by {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}
     * @param savedInstanceState This fragment is being re-constructed from a previous saved state
     *                           as given here.
     */
    @Override
    @SuppressLint("SourceLockedOrientationActivity")
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        pulseView = new ViewModelProvider(requireActivity()).get(PulseView.class);

        SharedPreferences settings = null;
        if (getContext() != null) {
            settings = PreferenceManager.getDefaultSharedPreferences(getContext());
            if (settings.getBoolean("nightmode", false))
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        if (getActivity() != null){
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            WindowCompat.setDecorFitsSystemWindows(getActivity().getWindow(), false);
            getActivity().getWindow().setStatusBarColor(Color.argb(0,0,0, 0));
            getActivity().getWindow().setNavigationBarColor(Color.argb(0,0,0, 0));

            if (settings != null) {
                if (!settings.getBoolean("nightmode", false)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        WindowInsetsController wic = view.getWindowInsetsController();
                        wic.setSystemBarsAppearance(WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                                WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS);
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                    }
                }
            }

            ActionBar aBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (aBar != null) aBar.hide();
        }

        toast = Toast.makeText(getContext(), getString(R.string.pressBack), Toast.LENGTH_SHORT);

        ImageButton config = view.findViewById(R.id.config);
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        int sHeight = (resourceId>0)?getResources().getDimensionPixelSize(resourceId):0;
        ViewGroup.MarginLayoutParams lay = (ViewGroup.MarginLayoutParams) config.getLayoutParams();
        lay.setMargins(10, sHeight + 10, 10, 10);
        config.requestLayout();

        createListenersObservers(view);
    }

    /**
     * Called when the Fragment is no longer started.
     */
    @Override
    public void onStop() {
        super.onStop();
        if(!refreshTheme) pulseView.stop();
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     */
    @Override
    public void onResume() {
        super.onResume();
        if (refreshTheme){
            refreshTheme = false;
            return;
        }
        if(permission) requestPermissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    /**
     * Create the different listeners and observers
     * @param view View where the listeners are
     */
    private void createListenersObservers(View view){
        view.findViewById(R.id.config).setOnClickListener(s ->{
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
        });

        view.findViewById(R.id.pulse).setOnLongClickListener(s -> {
            pulseView.reset();
            return false;
        });

        Wifi wifi = new Wifi(this);
        LifecycleOwner cycle = getViewLifecycleOwner();

        pulseView.getStateDev().observe(cycle, this::messageInterface);
        pulseView.getProgress().observe(cycle, this::progressUpdate);
        pulseView.getDialog().observe(cycle, s -> {
            if(s==null) return;
            Dialogs.newInstance(getParentFragmentManager(), "tag", s.toArray(new String[0]));
            pulseView.resetDialog();
        });

        pulseView.getWifi().observe(cycle, s -> {
            if(s==null) return;
            connectWifi(wifi, s);
            pulseView.resetWifi();
        });
        pulseView.getIntent().observe(cycle, s -> {
            if(s==null) return;
            try {
                startActivity(s);
            } catch (Exception e){
                Dialogs.newInstance(getParentFragmentManager(), "activity", new String[]{
                    getString(R.string.error),
                    getString(R.string.failIntent)
                });
            }
            pulseView.resetIntent();
        });
        pulseView.getException().observe(cycle, this::exception);
    }

    /**
     * The theme is changed and the view needs to be updated.
     */
    public static void refreshTheme(){
        refreshTheme = true;
    }

    public boolean back(){
        long now = System.currentTimeMillis() / 1000L;
        if ((now-time)<=3) {
            toast.cancel();
            pulseView.stop();
            return true;
        }
        toast.show();
        time = now;
        return false;
    }

    // =============================================================================
    // PERMISSIONS
    // =============================================================================

    /**
     * Dialog for grant the permission bluetooth
     */
    private final Dialogs.listenerYesNo permissionBlue = new Dialogs.listenerYesNo() {
        @Override
        public void yes() {
            requestPermissionBlue.launch(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        @Override
        public void no() {
            requireActivity().finish();
            System.exit(0);
        }
    };

    /**
     * Grant the permission bluetooth
     */
    private final ActivityResultLauncher<String> requestPermissionBlue =
        registerForActivityResult(new ActivityResultContracts.RequestPermission(), p-> {
            if (p) pulseView.start();
            else Dialogs.newInstance(getParentFragmentManager(), "tag",
                    new String[]{getString(R.string.grantNecessary), getString(R.string.grantBlue)}, permissionBlue);
            permission=p;
        });

    // =============================================================================
    // POWER ON DEVICE
    // =============================================================================

    /**
     * Listener of Dialog for activate Bluetooth
     */
    private final Dialogs.listenerYesNo deviceBluetooth = new Dialogs.listenerYesNo() {
        @Override
        public void yes() {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            onBluetooth.launch(enableBtIntent);
        }

        @Override
        public void no() {
            requireActivity().finish();
            System.exit(0);
        }
    };

    /**
     * Allows you to turn on the bluetooth
     */
    private final ActivityResultLauncher<Intent> onBluetooth = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if(result.getResultCode() != Activity.RESULT_OK)
                    Dialogs.newInstance(getParentFragmentManager(), "tag",
                        new String[] {getString(R.string.error), getString(R.string.blueOff)}, deviceBluetooth);
            });

    // =============================================================================
    // EXCEPTION
    // =============================================================================

    /**
     * Show in interface a Error with toast or Dialog (depend of the severity of the problem)
     * @param e Exception received from {@link DeviceException}
     */
    private void exception(DeviceException e){
        if(e==null) return;
        Log.e("Impulse", "Error: " + e);
        switch (e.getPulseError()){
            case IncorrectRS:
                Toast.makeText(getContext(), getString(R.string.Rs), Toast.LENGTH_LONG).show();
                break;
            case IncorrectCRC:
                Toast.makeText(getContext(), getString(R.string.Crc), Toast.LENGTH_LONG).show();
                break;
            case FailedPulse:
                Dialogs.newInstance(getParentFragmentManager(), "error",
                    new String[] {getString(R.string.error), getString(R.string.failP)});
                break;
            case MoveDevice:
                Toast.makeText(getContext(), getString(R.string.move), Toast.LENGTH_LONG).show();
                break;
            case DeviceOff:
                Dialogs.newInstance(getParentFragmentManager(), "error",
                    new String[] {getString(R.string.error), getString(R.string.blueOff)}, deviceBluetooth);
                break;
            case SensorNotFound:
                Dialogs.newInstance(getParentFragmentManager(), "error",
                    new String[] {getString(R.string.incompatible), getString(R.string.sensorNotFound)}, powerOff);
                break;
            case FailedBluetooth:
                Dialogs.newInstance(getParentFragmentManager(), "error",
                    new String[] {getString(R.string.error), getString(R.string.blueFail)});
                break;
            case WithoutStorage:
                Dialogs.newInstance(getParentFragmentManager(), "error",
                    new String[] {getString(R.string.error), getString(R.string.notStorage)});
                break;
            case DeleteTask:
                Dialogs.newInstance(getParentFragmentManager(), "error",
                        new String[] {getString(R.string.error), getString(R.string.taskDelete)});
        }
        pulseView.resetException();
    }

    /**
     * Listener to close application for error
     */
    private final Dialogs.listenerYes powerOff = () -> {
        requireActivity().finishAffinity();
        System.exit(0);
    };

    // =============================================================================
    // INTERFACE
    // =============================================================================

    /**
     * Change the interface depending the different state of device
     * @param stateDev Enumerator with the different state of device from {@link Pulse.StateDev}
     */
    private void messageInterface(Pulse.StateDev stateDev){
        switch (stateDev){
            case prepareBlue:
                mRipple.changeType(RipplePulseLayout.RIPPLE_TYPE_STROKE);
                mRipple.startRippleAnimation();
                textPulse.setText(getText(R.string.searchBlue));
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case preparePulse:
                mRipple.changeType(RipplePulseLayout.RIPPLE_TYPE_FILL);
                mRipple.startRippleAnimation();
                textPulse.setText(getText(R.string.searchPulse));
                progressBar.setVisibility(View.INVISIBLE);
                break;
            case receiveBlue:
                mRipple.stopRippleAnimation();
                textPulse.setText(getText(R.string.receiveBlue));
                progressBar.setVisibility(View.VISIBLE);
                Dialogs.dismiss();
                break;
            case receivePulse:
                mRipple.stopRippleAnimation();
                textPulse.setText(getText(R.string.receivePulse));
                progressBar.setVisibility(View.VISIBLE);
                Dialogs.dismiss();
                break;
            case error:
                mRipple.stopRippleAnimation();
                textPulse.setText(getText(R.string.errorPulse));
                progressBar.setVisibility(View.INVISIBLE);
                break;
        }
        progressBar.setProgress(0);
    }

    /**
     * Change the progress in progress bar and down text
     * @param prg Contain the progress from 0 to 100
     */
    private void progressUpdate(float prg){
        progressBar.setProgress((int) prg);
        textPulse.setText(getString(R.string.progress, new DecimalFormat("0.00").format(prg)));
    }

    /**
     * Connect to Wifi network and show a Dialog
     * @param wifi Class with the frame initialized
     * @param suggestion Class with the different suggestion independent of the Android version. You
     *                   can consult it in {@link Wifi.WifiSuggestion}
     */
    private void connectWifi(Wifi wifi, Wifi.WifiSuggestion suggestion){
        if(wifi.connect(this, suggestion)){
            if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q) {
                Dialogs.newInstance(getParentFragmentManager(), "wifi", new String[]{
                        getString(R.string.wifiSave),
                        getString(R.string.wifiSaveT, suggestion.getSSID())
                });
            } else if(Build.VERSION.SDK_INT==Build.VERSION_CODES.Q){
                Dialogs.newInstance(getParentFragmentManager(), "wifi", new String[]{
                        getString(R.string.wifiSave),
                        getString(R.string.wifiSaveN, suggestion.getSSID())
                });
            }
        }
    }
}