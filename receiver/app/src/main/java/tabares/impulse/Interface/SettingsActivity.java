/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Interface;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.PreferenceScreen;
import androidx.preference.SwitchPreferenceCompat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import tabares.impulse.Others.Dialogs;
import tabares.impulse.Others.Files;
import tabares.impulse.Others.Wifi;
import tabares.impulse.R;

public class SettingsActivity extends AppCompatActivity {

    /**
     * Called to do initial creation of a fragment.
     * @param savedInstanceState If the fragment is being re-created from a previous saved state,
     *                           this is the state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new PF())
                .commit();
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    /**
     * Notify to {@link PulseFragment} that need a recreation of its fragment.
     */
    public void refreshTheme() {
        PulseFragment.refreshTheme();
    }

    /**
     * Generate a preference fragment include in the application compact
     */
    public static class PF extends PreferenceFragmentCompat implements Preference.OnPreferenceClickListener, Preference.OnPreferenceChangeListener {
        private int debugMode = 10;
        private Toast developToast;
        private Preference save, resetWifi, version, license, translate, email;

        /**
         * Called when the fragment is visible to the user and actively running.
         */
        @Override
        public void onResume() {
            super.onResume();
            if (getContext() == null) return;
            try {
                save.setEnabled(true);
                save.setSummary(Files.getPathHuman(getContext()));
            } catch (Exception e) {
                if(Files.checkPermission(getContext())) {
                    Log.e("Impulse" ,"fileError: " + e);
                    save.setEnabled(false);
                    save.setSummary(getString(R.string.notPermission));
                } else {
                    save.setEnabled(true);
                    save.setSummary(getString(R.string.notPath));
                }
            }
        }

        /**
         * Called during {@link #onCreate(Bundle)} to supply the preferences for this fragment.
         * @param savedInstanceState If the fragment is being re-created from a previous saved
         *                           state, this is the state.
         * @param rootKey If non-null, this preference fragment should be rooted at the
         *                PreferenceScreen with this key.
         */
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.fragment_settings, rootKey);
            PreferenceScreen pref = getPreferenceScreen();

            SwitchPreferenceCompat nightMode = pref.findPreference("nightmode");
            save = pref.findPreference("save");
            resetWifi = pref.findPreference("resetWifi");
            version = pref.findPreference("version");
            license = pref.findPreference("license");
            translate = pref.findPreference("translate");
            email = pref.findPreference("email");

            if(nightMode!=null) nightMode.setOnPreferenceChangeListener(this);
            if(save!=null) save.setOnPreferenceClickListener(this);
            if(version!=null)  version.setOnPreferenceClickListener(this);
            if(license!=null) license.setOnPreferenceClickListener(this);
            if(translate!=null) translate.setOnPreferenceClickListener(this);
            if(email!=null) email.setOnPreferenceClickListener(this);

            if(Wifi.isReset()) {
                resetWifi.setOnPreferenceClickListener(this);
                resetWifi.setVisible(true);
            }

            if (getContext()==null) return;
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean develop = settings.getBoolean("develop", false);
            if (develop) debugMode = -1;
        }

        /**
         * Called when a Preference has been clicked.
         * @param prf The Preference that was clicked.
         * @return True if the click was handled.
         */
        @Override
        public boolean onPreferenceClick(Preference prf) {
            if (prf.equals(save)) save();
            else if (prf.equals(version)) develop();
            else if (prf.equals(license)) licenses();
            else if (prf.equals(translate)) startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/sergitab94/impulse")));
            else if (prf.equals(email)) mail();
            else if (prf.equals(resetWifi)) wifi();
            else return false;
            return true;
        }

        /**
         * Called when the preference save has been clicked.
         */
        private void save() {
            if(Files.checkPermission(getContext())){
                String[] data = {getString(R.string.incompatible), getString(R.string.sdIncompatible)};
                Dialogs.newInstance(getParentFragmentManager(), "save", data);
                return;
            }

            Intent n = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            n.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            n.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            saveResult.launch(n);
        }

        /**
         * Open the file manager to select the save folder
         */
        ActivityResultLauncher<Intent> saveResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    assert result.getData() != null;
                    Uri treeUri = result.getData().getData();
                    int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION
                               | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    requireContext().getContentResolver().takePersistableUriPermission(treeUri, takeFlags);
                    Files.savePath(requireContext(), treeUri);
                }
            });

        /**
         * Called when the preference version has been clicked. It enter in mode developer.
         */
        private void develop() {
            int dur = Toast.LENGTH_SHORT;
            if (developToast != null) developToast.cancel();
            Context context = getContext();
            if (context==null) return;
            if (debugMode > 1) {
                developToast = Toast.makeText(context, String.format(getString(R.string.othersD), debugMode), dur);
                debugMode--;
            } else if (debugMode == 1) {
                developToast = Toast.makeText(context, getString(R.string.firstD), dur);
                debugMode--;
            } else {
                developToast = Toast.makeText(context, getString(R.string.developD), dur);
                startActivity(new Intent(context, DebugActivity.class));
                if (debugMode == -1) return;
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("develop", true);
                editor.apply();
                debugMode = -1;
            }
            developToast.show();
        }

        /**
         * Called when the preference license has been clicked.
         */
        private void licenses() {
            InputStream in_s = getResources().openRawResource(R.raw.about);
            String data;

            try {
                byte[] b = new byte[in_s.available()];
                if(in_s.read(b)>0) data = new String(b);
                else data = getString(R.string.licenseError);
            } catch (IOException ignored) {
                data = getString(R.string.licenseError);
            }

            final TextView message = new TextView(getContext());
            message.setText(HtmlCompat.fromHtml(data, HtmlCompat.FROM_HTML_MODE_LEGACY));
            message.setMovementMethod(LinkMovementMethod.getInstance());
            message.setTextSize(17);
            message.setPadding(40, 20, 0, 0);

            new AlertDialog.Builder(requireActivity())
                .setTitle(getString(R.string.about))
                .setView(message)
                .setPositiveButton(android.R.string.ok, null)
                .create()
                .show();
        }

        /**
         * Called when the preference mail has been clicked.
         */
        private void mail() {
            String text = "\n";
            Sensor sens = null;
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);

            if (getContext() != null) {
                SensorManager mSensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
                sens = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            }

            text += "\nApp version: " + getString(R.string.app_ver);
            text += "\nAPI Android: " + Build.VERSION.SDK_INT;
            text += "\nModel: " + Build.MODEL;
            text += "\nManufacturer: " + Build.MANUFACTURER;
            text += "\nCountry: " + Locale.getDefault().getISO3Language() + "-" + Locale.getDefault().getISO3Country();

            if (sens == null) text += "\nWithout sensor";
            else {
                text += "\nModel of sensor: " + sens.getName();
                text += "\nVendor of sensor: " + sens.getVendor();
                text += "\nVersion of sensor: " + sens.getVersion();
                text += "\nMaximum delay (ms): " + sens.getMaxDelay();
                text += "\nMinimum delay (ms): " + sens.getMinDelay();
                text += "\nMaximum range (uT): " + sens.getMaximumRange();
                text += "\nResolution (uT): " + sens.getResolution();
                text += "\nPower (mA): " + sens.getPower();
            }

            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.putExtra(Intent.EXTRA_EMAIL, "sergio.tabares@uva.es");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[Impulse] - Feedback");
            emailIntent.putExtra(Intent.EXTRA_TEXT, text);

            try {
                startActivity(emailIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getContext(), getString(R.string.mailErrorD), Toast.LENGTH_LONG).show();
            }
        }

        /**
         * Called when the preference wifi has been clicked. This preference is only visible in
         * version android 29 (Android Q)
         */
        private void wifi(){
            Fragment frg = this;
            Dialogs.newInstance(getParentFragmentManager(), "wifiReset",
                new String[]{getString(R.string.resetWifi), getString(R.string.resetWifiDialog)},
                new Dialogs.listenerYesNo() {
                    @Override
                    public void yes() {
                        Wifi.resetAll(frg);
                    }

                    @Override
                    public void no() { }
                });
        }

        /**
         * Called when a Preference has been changed by the user.
         * @param preference The changed Preference.
         * @param newValue The new value of the Preference.
         * @return True to update the state of the Preference with the new value.
         */
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if(getActivity()==null) return false;
            ((SettingsActivity) getActivity()).refreshTheme();
            getActivity().getWindow().setWindowAnimations(R.style.WindowAnim);
            getActivity().recreate();

            if ((boolean)newValue) AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            else AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            return true;
        }
    }
}
