/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.View;

import android.app.Application;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import tabares.impulse.Others.Files;
import tabares.impulse.R;
import tabares.pulse.DeviceException;
import tabares.pulse.Listeners;
import tabares.pulse.Pulse;
import tabares.pulse.PulseStats;

public class DebugView extends AndroidViewModel implements Listeners.PulseListenerDebug {
    private final MutableLiveData<Pulse.StateDev> stateDev = new MutableLiveData<>();
    private final MutableLiveData<PulseData> pData = new MutableLiveData<>();
    private final MutableLiveData<String> progress = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<String>> dialog = new MutableLiveData<>();
    private final MutableLiveData<DeviceException> error = new MutableLiveData<>();

    private final NumberFormat ddf = NumberFormat.getNumberInstance();
    private OutputStream outputStream = null;
    private int frame = 0;
    private static final String TAG = "Impulse";

    /**
     * Class with the data for pulse
     */
    public static class PulseData {
        private final long pos;
        private final float[] info;

        /**
         * Constructor of the class
         * @param pos Position of the pulse (First start 0)
         * @param info Magnetic field in all axis
         */
        public PulseData(long pos, float[] info){
            this.pos=pos;
            this.info= info;
        }

        public long getPos() {
            return pos;
        }

        public float[] getInfo() {
            return info;
        }
    }

    /**
     * Constructor of the class
     * @param application Base class for maintaining global application state
     */
    public DebugView(@NonNull Application application) {
        super(application);
        ddf.setMinimumFractionDigits(2);
        ddf.setMaximumFractionDigits(2);
    }

    // =============================================================================
    // GETTERS OF LIVEDATA
    // =============================================================================
    public LiveData<Pulse.StateDev> getStateDev(){
        return stateDev;
    }

    public LiveData<PulseData> getListData(){
        return pData;
    }

    public LiveData<String> getProgress(){
        return progress;
    }

    public LiveData<ArrayList<String>> getDialog(){
        return dialog;
    }

    public LiveData<DeviceException> getError() {
        return error;
    }

    /**
     * Reset LiveData for Dialog
     */
    public void resetDialog() {
        dialog.postValue(null);
    }

    /**
     * Callback when click button remove
     * @return True if click is processed
     */
    public boolean remove(){
        Context context = getApplication().getApplicationContext();
        if (outputStream == null) {
            Pulse.stop();
            try { Pulse.start(context, this); }
            catch (DeviceException ignored) { }
            return false;
        }
        boolean ret=true;
        try {
            Pulse.stop();
            outputStream.close();
            outputStream=Files.getFile(context,"Debug",".temp");
            outputStream.write(getStart(context));
            outputStream.write("\nTimestamp,X,Y,Z\n".getBytes());
            Pulse.start(context, this);
        } catch (Exception e) {
            Log.w(TAG, "ExceptionRemove", e);
            ret=false;
        }

        ret= ret && start(context);
        Toast.makeText(context, context.getString(R.string.deleteD), Toast.LENGTH_LONG).show();
        return ret;
    }

    /**
     * Callback when click button statistic
     * @return True if click is processed
     */
    public boolean stat(){
        Context context = getApplication().getApplicationContext();
        ArrayList<String> data = new ArrayList<>();
        data.add(context.getString(R.string.infoD));
        PulseStats stat = Pulse.getStats();
        data.add(context.getString(R.string.avgD, stat.getAvg()) + "<br/>" +
                context.getString(R.string.maxD, stat.getMax()) + "<br/>" +
                context.getString(R.string.minD, stat.getMin()) + "<br/>" +
                context.getString(R.string.amountD, stat.getInit()) + "<br/>" +
                context.getString(R.string.startD, stat.getStartUnix()) + "<br/>" +
                context.getString(R.string.endD, stat.getEndUnix()));
        dialog.postValue(data);
        return true;
    }

    /**
     * Callback when click button save
     * @return True if click is processed
     */
    public boolean save(){
        Context context = getApplication().getApplicationContext();

        if (outputStream == null) return false;
        String to;

        try {
            Pulse.stop();
            String time = (new Date()).getTime() + ".csv";
            outputStream.flush();
            outputStream.close();
            Files.renameFile(context, "Debug", ".temp", time);
            to = Files.getPathHuman(context) + "/Debug/" + time;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, context.getString(R.string.savingE), Toast.LENGTH_LONG).show();
            return false;
        }
        Toast.makeText(context, context.getString(R.string.saveD, to), Toast.LENGTH_LONG).show();

        return start(context);
    }

    /**
     * Initialize the file and save the information
     */
    public void start(){
        start(getApplication().getApplicationContext());
    }

    /**
     * Initialize the file and stop Pulse
     * @param context Context of the application
     * @return True if correctly initialized
     */
    private boolean start(Context context){
        try {
            outputStream = Files.getFile(context, "Debug", ".temp");
            outputStream.write(getStart(context));
            outputStream.write("\nTimestamp,X,Y,Z\n".getBytes());
            outputStream.flush();
            Pulse.start(context, this);
        } catch (IOException e) {
            if(Objects.equals(e.getMessage(), "I/O not permission")) Toast.makeText(context, context.getString(R.string.writeD), Toast.LENGTH_LONG).show();
            else Toast.makeText(context, context.getString(R.string.savingE), Toast.LENGTH_LONG).show();
            outputStream = null;
            try { Pulse.start(context, this); }
            catch (DeviceException ignored) { }
            return false;
        } catch (DeviceException e) {
            dialog.postValue(new ArrayList<String>(){{
                    add(context.getString(R.string.error));
                    add(context.getString(R.string.deviceE));
            }});
            return false;
        }
        return true;
    }

    /**
     * Generate headers of the file
     * @param context Context of the application
     * @return Header of the file
     */
    private byte[] getStart(Context context) {
        SensorManager mSensorManager;
        String info = "";
        Sensor sens;

        if (context==null) return new byte[]{};
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sens = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        info += "#Model:" + Build.MODEL;
        info += "\n#Manufacturer:" + Build.MANUFACTURER;
        info += "\n#API Android:" + Build.VERSION.SDK_INT;
        info += "\n#App version:" + context.getString(R.string.app_ver);
        if (sens != null) {
            info += "\n#Gyroscope information#";
            info += "\n#Model of sensor:" + sens.getName();
            info += "\n#Vendor of sensor:" + sens.getVendor();
            info += "\n#Version of sensor:" + sens.getVersion();
            info += "\n#Maximum delay:" + sens.getMaxDelay();
            info += "\n#Minimum delay:" + sens.getMinDelay();
            info += "\n#Maximum range:" + sens.getMaximumRange();
            info += "\n#Resolution:" + sens.getResolution();
            info += "\n#Power:" + sens.getPower();
        }
        return info.getBytes();
    }

    /**
     * Delete file and stop Pulse
     */
    public void stop(){
        Context context = getApplication().getApplicationContext();
        Pulse.stop();
        if (outputStream == null) return;
        try {
            outputStream.close();
            Files.deleteFile(context,"Debug",".temp");
            outputStream=null;
        } catch (Exception ignored) { }
    }

    /**
     * Listen the errors and add to MutableLiveData
     * @param exception Error of the device of the {@link DeviceException}
     */
    @Override
    public void error(DeviceException exception) {
        Log.e(TAG, exception.toString());
        error.postValue(exception);
    }

    /**
     * Listen the progress and add to MutableLiveData
     * @param progress Number with progress
     */
    @Override
    public void progress(float progress) {
        this.progress.postValue(String.format("%7s%%", ddf.format(progress)));
    }

    /**
     * Listen the state of the device and add to MutableLiveData
     * @param state State of the device of the {@link Pulse.StateDev}
     */
    @Override
    public void state(Pulse.StateDev state) {
        stateDev.postValue(state);
    }

    /**
     * Receive a data magnetic. It add a magnetic field to MutableLiveData and to the file
     * @param data Magnetic fields in (X→, Y↑, Z↗)
     * @param time Timestamp in nanoseconds
     */
    @Override
    public void dataMagnetic(float[] data, long time) {
        PulseData dataPulse = new PulseData(frame++, data);
        pData.postValue(dataPulse);

        if (outputStream == null) return;
        try {
            outputStream.write((time + "," + data[0] + "," + data[1] + "," + data[2] + "\n").getBytes());
        } catch (IOException e) {
            try {
                outputStream.close();
            } catch (IOException ignored) { }
            outputStream = null;
        }
    }
}