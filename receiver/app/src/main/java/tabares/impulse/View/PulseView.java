/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.View;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import tabares.impulse.Others.Files;
import tabares.impulse.Others.Wifi;
import tabares.impulse.R;
import tabares.pulse.DeviceException;
import tabares.pulse.Listeners;
import tabares.pulse.Pulse;

public class PulseView extends AndroidViewModel implements Listeners.PulseListener {
    private final MutableLiveData<Pulse.StateDev> stateDev = new MutableLiveData<>();
    private final MutableLiveData<Float> progressFloat = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<String>> dialog = new MutableLiveData<>();
    private final MutableLiveData<Wifi.WifiSuggestion> wifi = new MutableLiveData<>();
    private final MutableLiveData<Intent> intent = new MutableLiveData<>();
    private final MutableLiveData<DeviceException> exception = new MutableLiveData<>();

    private OutputStream file;

    /**
     * Constructor of the object
     * @param application Base class for maintaining global application state
     */
    public PulseView(@NonNull Application application) {
        super(application);
    }

    // =============================================================================
    // GETTERS OF LIVEDATA
    // =============================================================================
    public LiveData<Pulse.StateDev> getStateDev(){
        return stateDev;
    }

    public LiveData<Float> getProgress(){
        return progressFloat;
    }

    public LiveData<ArrayList<String>> getDialog(){
        return dialog;
    }

    public LiveData<Wifi.WifiSuggestion> getWifi(){
        return wifi;
    }

    public LiveData<Intent> getIntent(){
        return intent;
    }

    public LiveData<DeviceException> getException(){
        return exception;
    }

    // =============================================================================
    // RESET OF LIVEDATA
    // =============================================================================
    public void resetDialog() {
        dialog.postValue(null);
    }

    public void resetWifi() {
        wifi.postValue(null);
    }

    public void resetIntent() {
        intent.postValue(null);
    }

    public void resetException(){
        exception.postValue(null);
    }

    /**
     * Initialize file and start Pulse
     */
    public void start(){
        Context context = getApplication().getApplicationContext();
        try { file = Files.getFile(context, ".temp"); }
        catch (IOException e) { e.printStackTrace(); }

        try{ Pulse.start(context, this); }
        catch (DeviceException e){ exception.postValue(e); }
    }

    /**
     * Destroy pulse and delete file
     */
    public void stop(){
        Pulse.stop();
        try {
            if(file!=null) Files.deleteFile(getApplication().getApplicationContext(), ".temp");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reset file and protocol pulse
     */
    public void reset(){
        Context context = getApplication().getApplicationContext();
        Pulse.stop();

        try { file = Files.getFile(context, ".temp"); }
        catch (IOException e) { e.printStackTrace(); }

        try{ Pulse.start(context, this); }
        catch (DeviceException exception){ exception.printStackTrace(); }

        Toast.makeText(context, context.getString(R.string.reset), Toast.LENGTH_LONG).show();
    }

    /**
     * Reset file and change MutableLiveData
     * @param e Error of the device of the {@link DeviceException}
     */
    @Override
    public void error(DeviceException e) {
        try {
            if(file!=null) file.write("".getBytes());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        exception.postValue(e);
    }

    /**
     * Listen the progress and add to MutableLiveData
     * @param progress Number with progress
     */
    @Override
    public void progress(float progress) {
        progressFloat.postValue(progress);
    }

    /**
     * Listen the state of the device and add to MutableLiveData
     * @param state State of the device of the {@link Pulse.StateDev}
     */
    @Override
    public void state(Pulse.StateDev state) {
        stateDev.postValue(state);
    }

    /**
     * Information received for protocol
     * @param type Type of task to process {@link Pulse.TypeTask}
     * @param args Information received for the task
     */
    @Override
    public void information(Pulse.TypeTask type, String[] args) {
        switch (type){
            case File: file(args); return;
            case Message: message(args); return;
            case Wifi: wifi(args); return;
            case Sms: sms(args); return;
            case Email: email(args); return;
            case Bitcoin: bitcoin(args);
        }
    }

    /**
     * Write data in the file
     * @param raw write bytes in file
     */
    @Override
    public void fileWrite(byte raw) {
        if(file==null) {
            Pulse.notStorage();
            return;
        }
        try {
            file.write(raw);
        } catch (IOException e) {
            Pulse.notStorage();
            e.printStackTrace();
        }
    }

    /**
     * Process file (Close file and rename)
     * @param args Contain the name of file
     */
    private void file(String[] args){
        Context context = getApplication().getApplicationContext();
        try {
            if(file==null) return;
            file.flush();
            file.close();
            Files.renameFile(context, ".temp", args[0]);
            file=Files.getFile(context, ".temp");
            dialog.postValue(new ArrayList<String>(){{
                add(context.getString(R.string.file));
                add(context.getString(R.string.fileCreate, Files.getPathHuman(context) + "/" + args[0]));
            }});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process message
     * @param args Contain the message
     */
    private void message(String[] args){
        Context context = getApplication().getApplicationContext();
        dialog.postValue(new ArrayList<String>(){{
            add(context.getString(R.string.message));
            add(args[0]);
        }});
    }

    /**
     * Process wireless
     * @param args Contain (in order) Hide, {@link Pulse.TypeSec}, SSID and pass (optional)
     */
    private void wifi(String[] args){
        boolean hide = Boolean.getBoolean(args[0]);
        Pulse.TypeSec sec = Pulse.TypeSec.valueOf(args[1]);
        Wifi.WifiSuggestion wifiSuggestion;

        if(sec == Pulse.TypeSec.OPEN) wifiSuggestion = new Wifi.WifiSuggestion(hide, args[2]);
        else wifiSuggestion = new Wifi.WifiSuggestion(hide, sec, args[2], args[3]);

        wifi.postValue(wifiSuggestion);
    }

    /**
     * Process SMS
     * @param args Contain (in order) Number to send and message
     */
    private void sms(String[] args){
        Intent end = new Intent(Intent.ACTION_SENDTO);
        end.setData(Uri.parse("smsto:" + args[0]));
        end.putExtra("sms_body", args[1]);
        intent.postValue(end);
    }

    /**
     * Process Email
     * @param args Contain (in order) Email(s) to send, CC(s), Bcc(s), Subject and Message to send.
     *             If contain several, it should separate for commas
     */
    private void email(String[] args){
        Intent end = new Intent(Intent.ACTION_SENDTO);
        end.setData(Uri.parse("mailto:"));
        end.putExtra(Intent.EXTRA_EMAIL, args[0].split(","));
        end.putExtra(Intent.EXTRA_CC, args[1].split(","));
        end.putExtra(Intent.EXTRA_BCC, args[2].split(","));
        end.putExtra(Intent.EXTRA_SUBJECT, args[3]);
        end.putExtra(Intent.EXTRA_TEXT, args[4]);
        intent.postValue(end);
    }

    /**
     * Process Bitcoin
     * @param args Contain (in order) Wallet, message, total amount in BTC
     */
    private void bitcoin(String[] args){
        String scheme = "bitcoin:"+args[0];
        Uri.Builder bld = new Uri.Builder();
        if(!args[1].equals("")) bld.appendQueryParameter("message", args[1]);
        bld.appendQueryParameter("amount", args[2]);
        intent.postValue(new Intent(Intent.ACTION_VIEW, Uri.parse(scheme + bld)));
    }
}
