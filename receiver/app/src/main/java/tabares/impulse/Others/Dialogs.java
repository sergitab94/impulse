/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Others;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.io.Serializable;

public class Dialogs {
    private static final String DATA = "DATA";
    private static final String LISTENER = "LISTENER";
    private static DialogsCustom instance = null;

    /**
     * Listener of Dialog with buttons "Yes" and "Not"
     */
    public interface listenerYesNo extends Serializable{
        void yes();
        void no();
    }

    /**
     * Listener of Dialog with button "Yes"
     */
    public interface listenerYes extends Serializable{
        void yes();
    }

    /**
     * Create a Dialog without listener, but with the button "Yes"
     * @param manager Transaction for show the Dialog
     * @param tag Optional tag name for the fragment, to later retrieve the fragment
     * @param data Contain Title in the first position and message in the second position
     */
    public static void newInstance(FragmentManager manager, String tag, String[] data) {
        newInstance(manager, tag, data, (listenerYes) null);
    }

    /**
     * Create a Dialog with listener and with the button "Yes"
     * @param manager Transaction for show the Dialog
     * @param tag Optional tag name for the fragment, to later retrieve the fragment
     * @param data Contain Title in the first position and message in the second position
     * @param listener Execute by pressing the "Yes" button
     */
    public static void newInstance(FragmentManager manager, String tag, String[] data, listenerYes listener) {
        if(instance!=null) instance.dismiss();
        instance = new DialogsCustom();
        Bundle args = new Bundle();
        args.putStringArray(DATA, data);
        args.putSerializable(LISTENER, listener);
        instance.setArguments(args);

        FragmentTransaction ft = manager.beginTransaction();
        ft.add(instance, tag);
        ft.commit();
    }

    /**
     * Create a Dialog with listener and with the button "No" and "Yes"
     * @param manager Transaction for show the Dialog
     * @param tag Optional tag name for the fragment, to later retrieve the fragment
     * @param data Contain Title in the first position and message in the second position
     * @param listener Execute by pressing the "No" or "Yes" button
     */
    public static void newInstance(FragmentManager manager, String tag, String[] data, listenerYesNo listener) {
        if(instance!=null) instance.dismiss();
        instance = new DialogsCustom();
        Bundle args = new Bundle();
        args.putStringArray(DATA, data);
        args.putSerializable(LISTENER, listener);
        instance.setArguments(args);

        FragmentTransaction ft = manager.beginTransaction();
        ft.add(instance, tag);
        ft.commit();
    }

    /**
     * Dismiss dialog if the it is visible
     */
    public static void dismiss(){
        if(instance!=null) instance.dismiss();
        instance = null;
    }

    /**
     * Create DialogFragment for the class Dialog
     */
    public static class DialogsCustom extends DialogFragment{

        /**
         * Build a custom Dialog container.
         * @param savedInstanceState The last saved instance state of the Fragment, or null if this
         *                           is a freshly created Fragment.
         * @return Return a new Dialog instance to be displayed by the Fragment.
         */
        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            Activity activity = getActivity();
            Bundle args = getArguments();
            if (args==null) return createVoid();
            String[] data = args.getStringArray(DATA);
            Object listener = args.getSerializable(LISTENER);
            if (data==null || activity==null) return createVoid();

            AlertDialog alert;
            if (listener==null) {
                alert = new AlertDialog.Builder(activity)
                    .setTitle(data[0])
                    .setMessage(HtmlCompat.fromHtml(data[1], HtmlCompat.FROM_HTML_MODE_LEGACY))
                    .setPositiveButton(android.R.string.ok, (d, w) -> instance=null)
                    .create();
            } else if(listener instanceof listenerYes){
                alert = new AlertDialog.Builder(activity)
                    .setTitle(data[0])
                    .setMessage(HtmlCompat.fromHtml(data[1], HtmlCompat.FROM_HTML_MODE_LEGACY))
                    .setPositiveButton(android.R.string.ok, (d, w) -> {
                        ((listenerYes) listener).yes();
                        instance=null;
                    }).create();
            } else {
                alert = new AlertDialog.Builder(activity)
                    .setTitle(data[0])
                    .setMessage(HtmlCompat.fromHtml(data[1], HtmlCompat.FROM_HTML_MODE_LEGACY))
                    .setPositiveButton(android.R.string.ok, (d, w) -> {
                        ((listenerYesNo) listener).yes();
                        instance=null;
                    })
                    .setNegativeButton(android.R.string.cancel,  (d, w) -> {
                        ((listenerYesNo) listener).no();
                        instance=null;
                    })
                    .create();
                setCancelable(false);
            }
            alert.setCanceledOnTouchOutside(false);
            return alert;
        }

        /**
         * Create a Dialog void
         * @return Return a new Dialog void.
         */
        private Dialog createVoid(){
            return new AlertDialog.Builder(requireActivity()).create();
        }
    }
}