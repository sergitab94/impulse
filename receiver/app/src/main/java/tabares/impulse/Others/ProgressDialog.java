/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Others;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import tabares.impulse.R;

public class ProgressDialog extends DialogFragment {
    private ProgressBar bar;
    private TextView text;

    /**
     * Called to have the fragment instantiate its user interface view.
     * @param inflater The LayoutInflater object that can be used to inflate any views in the
     *                 fragment
     * @param container The fragment should not add the view itself, but this can be used to
     *                  generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous
     *                           saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.progress_dialog, container, false);
        bar = view.findViewById(R.id.progress_circle);
        text = view.findViewById(R.id.progress_text);
        Objects.requireNonNull(getDialog()).getWindow().setBackgroundDrawableResource(R.color.transparent);
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);
        return view;
    }

    /**
     * Change the progress of the dialog
     * @param progress progress of the dialog
     */
    public void setProgress(int progress){
        bar.setProgress(progress);
        text.setText(getString(R.string.percent, progress));
    }

    /**
     * Hide the dialog
     */
    @Override
    public void dismiss() {
        super.dismiss();
        setProgress(0);
    }
}
