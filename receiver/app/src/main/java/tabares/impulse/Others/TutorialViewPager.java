/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Others;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class TutorialViewPager extends ViewPager {

    private float downX;
    private int page=-1;
    private ListenerNext listener = null;
    private boolean execute = false;

    public interface ListenerNext{
        void next();
    }

    /**
     * Constructor of the tutor
     * @param context Context of application
     * @param attrs A collection of attributes, as found associated with a tag in an XML document
     */
    public TutorialViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Set a listener when change of slider
     * @param listener Run listener when change the slider
     */
    public void setListener(ListenerNext listener){
        this.listener=listener;
    }

    /**
     * Intercept touch event
     * @param event Object used to report movement (mouse, pen, finger, trackball) events.
     * @return True if touch is processed
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return super.onInterceptTouchEvent(event);
    }

    /**
     * Callback when touch the ViewPager
     * @param event Object used to report movement (mouse, pen, finger, trackball) events.
     * @return True if touch is processed
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        performClick();
        if(page<=getCurrentItem() && wasSwipeToRightEvent(event)) {
            if(!execute && listener!=null){
                new Thread(() -> {
                    execute=true;
                    listener.next();
                    execute=false;
                }).start();
            }
            return true;
        }
        return super.onTouchEvent(event);
    }

    /**
     * Call this view's OnClickListener, if it is defined.
     * @return True if touch is performed
     */
    @Override
    public boolean performClick() {
        return super.performClick();
    }

    /**
     * Set maximum page to block
     * @param page Page to block
     */
    public void setBlockPage(int page){
        this.page=page;
    }

    /**
     * Get maximum page to block
     * @return Get page to block
     */
    public int getBlockPage(){
        return page;
    }

    /**
     * Block maximum the next page
     */
    public void setBlockNext(){
        page++;
    }

    /**
     * If slide is equal to current block the next page
     * @param aPage Current slide
     */
    public void setBlockNextPage(int aPage){
        if(aPage==page) page++;
    }

    /**
     * Check if motion event is swipe to right event
     * @param event Object used to report movement (mouse, pen, finger, trackball) events.
     * @return True if swipe to right
     */
    private boolean wasSwipeToRightEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
                return false;

            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                return downX - event.getX() > 0;

            default:
                return false;
        }
    }
}