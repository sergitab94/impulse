/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Others;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.preference.PreferenceManager;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;

import tabares.impulse.R;

public class Files {

    private static final String varPath = "path";

    // =============================================================================
    // PERMISSIONS
    // =============================================================================

    /**
     * Check if the application can read external storage
     * @param context Context of the application
     * @return True if NOT can; False if YES can
     */
    public static boolean checkPermission(Context context){
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            removePath(context);
            return true;
        }
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            removePath(context);
            return true;
        }
        return false;
    }

    /**
     * Check if the application can read external storage and if it has permission to the folder
     * @param context Context of the application
     * @return True if NOT can; False if YES can
     */
    public static boolean checkPermissionComplete(Context context){
        if (checkPermission(context)) return true;

        //Check permission folder
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String path = settings.getString(varPath, null);
        if (path == null)  return true;
        DocumentFile documentFile = DocumentFile.fromTreeUri(context, Uri.parse(path));
        if (documentFile == null || !documentFile.exists() || !documentFile.canWrite()){
            removePath(settings);
            return true;
        }
        return false;
    }

    // =============================================================================
    // PATHS
    // =============================================================================

    /**
     * Returns the path in String format so that a human can read it
     * @param context Context of the application
     * @return String with the path
     * @throws IOException Folder not available
     */
    public static String getPathHuman(@NonNull Context context) throws IOException{
        DocumentFile file = getPath(context, null);
        return readHuman(context, file.getUri().getEncodedPath());
    }

    /**
     * Obtain the path of a File
     * @param context Context of the application
     * @param subfolder A subfolder for main path
     * @return Document to obtain
     * @throws IOException File not available
     */
    private static DocumentFile getPath(@NonNull Context context, String subfolder) throws IOException{
        if (checkPermission(context)) throw new IOException("I/O not permission");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String path = settings.getString(varPath, null);
        if (path == null) throw new IOException("Without Folder");

        DocumentFile documentFile = DocumentFile.fromTreeUri(context, Uri.parse(path));
        if (documentFile == null || !documentFile.exists() ) throw new IOException("Without Folder");
        if (subfolder != null) {
            DocumentFile endFile = searchFolder(documentFile, subfolder);
            if (endFile==null) documentFile = documentFile.createDirectory(subfolder);
            else documentFile = endFile;
        }
        return documentFile;
    }

    /**
     * Search if exist a file in a folder
     * @param path Path of the folder
     * @param search Name of file
     * @return Null if not exist null; If exist return a complete path
     */
    private static DocumentFile searchFolder(DocumentFile path, String search) {
        for (DocumentFile file: path.listFiles()) {
            if (file.isDirectory() && search.equals(file.getName())) return file;
        }
        return null;
    }

    /**
     * Convert a String path in a String readable for humans
     * @param context Context of the application
     * @param path Path to the convert
     * @return Path readable
     * @throws IOException When the path is impossible
     */
    private static String readHuman(Context context, String path) throws  IOException{
        if (path==null) return "";
        path = path.replaceAll("/document/.*$", "");
        path = URLDecoder.decode(path, "UTF-8");

        if (path.equals("/tree/primary:")) return context.getString(R.string.internal);
        else if(path.equals("/tree/downloads")) return  context.getString(R.string.internal) + ": Downloads";
        else if(path.matches("^/tree/[a-zA-Z0-9]*-[a-zA-Z0-9]*:$")) return context.getString(R.string.external);
        else if (path.contains("/tree/primary")) return path.replaceAll("^/tree/primary:", context.getString(R.string.internal) + ": ");
        else if(path.contains("/tree/raw:/storage/emulated/0/")) return path.replaceAll("^/tree/raw:/storage/emulated/0/", context.getString(R.string.internal) + ": ");
        else if(path.contains("/storage/emulated/0/")) return path.replaceAll("^/storage/emulated/0/", context.getString(R.string.internal) + ": ");
        else return path.replaceAll("^/tree/[a-zA-Z0-9]+-[a-zA-Z0-9]*:", context.getString(R.string.external) + ": ");
    }

    // =============================================================================
    // FILES
    // =============================================================================

    /**
     * Gets a OutputStream from a file localize in a subfolder
     * @param context Context of the application
     * @param subfolder Subfolder where the file was saved
     * @param nameFile Name of file to get
     * @return Stream of the file
     * @throws IOException File not available or insufficient permissions
     */
    public static OutputStream getFile(@NonNull Context context, String subfolder, @NonNull String nameFile) throws IOException{
        DocumentFile documentFile = getPath(context, subfolder);
        DocumentFile file = searchFile(documentFile, nameFile);
        if (file==null) file = documentFile.createFile("", nameFile);
        if (file==null) throw new IOException("I/O not permission");
        OutputStream out = context.getContentResolver().openOutputStream(file.getUri());
        if (out==null) throw new IOException("I/O not permission");
        out.write("".getBytes());
        return out;
    }

    /**
     * Gets a OutputStream from a file
     * @param context Context of the application
     * @param nameFile Name of file to get
     * @return Stream of the file
     * @throws IOException File not available or insufficient permissions
     */
    public static OutputStream getFile(@NonNull Context context, @NonNull String nameFile) throws IOException{
        return getFile(context,  null, nameFile);
    }

    /**
     * Rename a file in a subfolder
     * @param context Context of the application
     * @param subfolder Subfolder where the file was saved
     * @param from Name of the file to rename
     * @param to Name final of the file
     * @throws IOException File not available or insufficient permissions
     */
    public static void renameFile(@NonNull Context context, String subfolder, @NonNull String from, @NonNull String to) throws IOException{
        DocumentFile documentFile = searchFile(getPath(context, subfolder), from);
        if(documentFile!=null) documentFile.renameTo(to);
    }

    /**
     * Rename a file
     * @param context Context of the application
     * @param from Name of the file to rename
     * @param to Name final of the file
     * @throws IOException File not available or insufficient permissions
     */
    public static void renameFile(@NonNull Context context, @NonNull String from, @NonNull String to) throws IOException{
        renameFile(context, null, from, to);
    }

    /**
     * Delete a file in a subfolder
     * @param context Context of the application
     * @param subfolder Subfolder where the file was saved
     * @param delete Name of the file to delete
     * @throws IOException File not available or insufficient permission
     */
    public static void deleteFile(@NonNull Context context, String subfolder, @NonNull String delete) throws IOException{
        DocumentFile documentFile = searchFile(getPath(context, subfolder), delete);
        if(documentFile!=null) documentFile.delete();
    }

    /**
     * Delete a file
     * @param context Context of the application
     * @param delete Name of the file to delete
     * @throws IOException File not available or insufficient permission
     */
    public static void deleteFile(@NonNull Context context, @NonNull String delete) throws IOException{
        deleteFile(context, null, delete);
    }

    /**
     * Search a file in a path
     * @param path Path of folder
     * @param search Name of file
     * @return if not exist null; If exist return a complete path
     */
    private static DocumentFile searchFile(DocumentFile path, String search) {
        for (DocumentFile file: path.listFiles()) {
            if (file.isFile() && search.equals(file.getName())) return file;
        }
        return null;
    }

    // =============================================================================
    // SAVE PREFERENCES
    // =============================================================================

    /**
     * Delete path from preferences of the application
     * @param settings Interface for accessing and modifying preference data returned by {@link Context}
     */
    private static void removePath(SharedPreferences settings){
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(varPath);
        editor.apply();
    }

    /**
     * Delete path from preferences of the application
     * @param context Context of the application
     */
    private static void removePath(Context context){
        removePath(PreferenceManager.getDefaultSharedPreferences(context));
    }

    /**
     * Save path to preferences of the application
     * @param context Context of the application
     * @param documentFile URI of the path to save
     */
    public static void savePath(@NonNull Context context, @NonNull Uri documentFile){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(varPath, documentFile.toString());
        editor.apply();
    }
}