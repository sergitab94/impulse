/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Others;

import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import tabares.impulse.R;

public class TutorialSlides extends PagerAdapter {
    private final Context context;
    private final LayoutInflater inflater;
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> future = null;

    private final int[] image = {
            R.drawable.tutorial_0,
            R.drawable.tutorial_1,
            R.drawable.tutorial_2,
            R.drawable.tutorial_3,
            R.drawable.tutorial_4,
            R.drawable.tutorial_5,
            R.drawable.tutorial_6
    };
    private final String[] title;
    private final String[] text;
    private final AnimatedVectorDrawable[] animated;

    /**
     * Create different slides of the page adapter
     * @param context Context of the application
     */
    public TutorialSlides(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        title = context.getResources().getStringArray(R.array.titleT);
        text = context.getResources().getStringArray(R.array.textT);
        animated = new AnimatedVectorDrawable[image.length];

        if(image.length != title.length || text.length != title.length)
            Log.e("Impulse", "Error Level: Failed tutorial constructor");
    }

    /**
     * Obtain the number of the slides in page adapter.
     * @return Number of the slides
     */
    @Override
    public int getCount() {
        return image.length;
    }

    /**
     * Determines whether a page View is associated with a specific key object as returned by
     * {@link #instantiateItem(ViewGroup, int)}.
     * @param view Page View to check for association with object
     * @param object Object to check for association with view
     * @return True if view is associated with the key object object
     */
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    /**
     * Create the page for the given position
     * @param container The containing View in which the page will be shown.
     * @param position The page position to be instantiated.
     * @return Returns an Object representing the new page. This does not need to be a View, but can
     * be some other container of the page.
     */
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.fragment_slide, container, false);

        ImageView imageO = view.findViewById(R.id.image);
        TextView titleO = view.findViewById(R.id.title);
        TextView textO = view.findViewById(R.id.text);


        titleO.setText(title[position]);
        textO.setText(text[position]);

        animated[position] = (AnimatedVectorDrawable) ContextCompat.getDrawable(context, image[position]);
        imageO.setImageDrawable(animated[position]);
        if(future==null && position==0) animated[0].start();

        container.addView(view);
        return view;
    }

    /**
     * Remove a page for the given position
     * @param container The containing View from which the page will be removed
     * @param position The page position to be removed
     * @param object The same object that was returned by {@link #instantiateItem(ViewGroup, int)}
     */
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    /**
     * Animate the image central
     * @param position Position of the image animated
     */
    public void animatedItem(int position){
        if(future!=null) future.cancel(true);
        if(animated[position]==null) return;
        animated[position].start();
        if(position==0){
            animated[0].start();
            return;
        }
        future = executor.scheduleAtFixedRate(() -> new Handler(Looper.getMainLooper()).post(() -> animated[position].start()),
                0, 15, TimeUnit.SECONDS);
    }
}