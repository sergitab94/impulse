/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.impulse.Others;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkSuggestion;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import tabares.impulse.R;
import tabares.pulse.Pulse;

public class Wifi {
    private final ActivityResultLauncher<Intent> addWifi, powerWifi;
    private Intent wifiAdd;

    /**
     * Class create of wifiSuggestion
     */
    public static class WifiSuggestion{
        String SSID, pass;
        boolean hide;
        Pulse.TypeSec sec;

        /**
         * Initialize the class
         * @param hide True if hide
         * @param sec Type of security
         * @param SSID Name of access point
         * @param pass Password of access point
         */
        public WifiSuggestion(boolean hide, Pulse.TypeSec sec, String SSID, String pass){
            this.hide = hide;
            this.sec = sec;
            this.SSID = SSID;
            this.pass = pass;
        }

        /**
         * Initialize the class
         * @param hide True if hide
         * @param SSID Name of access point
         */
        public WifiSuggestion(boolean hide, String SSID) {
            this.hide = hide;
            this.sec = Pulse.TypeSec.OPEN;
            this.SSID = SSID;
        }

        /**
         * Get SSID
         * @return SSID of access point
         */
        public String getSSID() {
            return SSID;
        }

        /**
         * Convert object to string
         * @return Readable string from access point
         */
        @NonNull
        @Override
        public String toString() {
            if(sec== Pulse.TypeSec.OPEN){
                return "Hidden:" + hide +
                        " Security:" + sec +
                        " SSID:" + SSID;
            } else {
                return "Hidden:" + hide +
                        " Security:" + sec +
                        " SSID:" + SSID +
                        " Password:" + pass;
            }
        }
    }

    /**
     * Construct of the class
     * @param fragment Fragment to invoke activity
     */
    public Wifi(Fragment fragment){
        addWifi = fragment.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {});

        powerWifi = fragment.registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> addWifi.launch(wifiAdd));
    }

    /**
     * Check if it should reset
     * @return True if it should reset
     */
    public static boolean isReset(){
        return (Build.VERSION.SDK_INT==Build.VERSION_CODES.Q);
    }

    /**
     * Remove all wifi configurations
     * @param frg Fragment of application
     */
    public static void resetAll(Fragment frg){
        if(Build.VERSION.SDK_INT!=Build.VERSION_CODES.Q) return;
        WifiManager wifiManager = (WifiManager) frg.requireContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        wifiManager.removeNetworkSuggestions(new ArrayList<>());
    }

    /**
     * Connect to wifi suggestion for all version
     * @param frg Fragment used to launch activity
     * @param wifiSuggestion Suggestion of connect to wifi
     * @return True if save correctly
     */
    public boolean connect(Fragment frg, WifiSuggestion wifiSuggestion){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.R) return more29(frg, wifiSuggestion);
        else if(Build.VERSION.SDK_INT==Build.VERSION_CODES.Q) return equal29(frg, wifiSuggestion);
        else return less29(frg, wifiSuggestion);
    }

    /**
     * Connect to wifi suggestion for API version less 29
     * @param frg Fragment used to launch activity
     * @param wifi Suggestion of connect to wifi
     * @return True if save correctly
     */
    private boolean less29(Fragment frg, WifiSuggestion wifi){
        WifiManager wifiManager = (WifiManager) frg.requireContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        if(!wifiManager.isWifiEnabled()) wifiManager.setWifiEnabled(true);

        //Configure wifi
        WifiConfiguration wc = new WifiConfiguration();
        wc.SSID = "\"" + wifi.SSID +"\"";
        wc.status = WifiConfiguration.Status.ENABLED;

        if(wifi.hide) wc.hiddenSSID = true;

        if(wifi.sec == Pulse.TypeSec.OPEN) {
            wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        } else {
            wc.preSharedKey = "\"" + wifi.pass + "\"";
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        }

        //Add Network and enable
        wifiManager.enableNetwork(wifiManager.addNetwork(wc), true);
        return true;
    }

    /**
     * Connect to wifi suggestion for API version equal to 29
     * @param frg Fragment used to launch activity
     * @param wifi Suggestion of connect to wifi
     * @return True if save correctly
     */
    @TargetApi(Build.VERSION_CODES.Q)
    private boolean equal29(Fragment frg, WifiSuggestion wifi){
        //Add Suggestion
        WifiManager wifiManager = (WifiManager) frg.requireContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        ArrayList<WifiNetworkSuggestion> suggestion =  createSuggestion(wifi);
        int status = wifiManager.addNetworkSuggestions(suggestion);

        //Repair Error
        if(status==WifiManager.STATUS_NETWORK_SUGGESTIONS_ERROR_ADD_DUPLICATE) {
            wifiManager.removeNetworkSuggestions(suggestion);
            status=wifiManager.addNetworkSuggestions(suggestion);
        }

        //Inform Dialog Error
        if(status==WifiManager.STATUS_NETWORK_SUGGESTIONS_ERROR_APP_DISALLOWED) {
            Dialogs.newInstance(frg.requireActivity().getSupportFragmentManager(), "Error", new String[]{
                frg.getString(R.string.error),
                frg.getString(R.string.grantWifi)
            });
            return false;
        } else if(status==WifiManager.STATUS_NETWORK_SUGGESTIONS_ERROR_ADD_EXCEEDS_MAX_PER_APP) {
            Dialogs.newInstance(frg.requireActivity().getSupportFragmentManager(), "Error", new String[]{
                frg.getString(R.string.error),
                frg.getString(R.string.maxWifi)
            });
            return false;
        } else if(status!=WifiManager.STATUS_NETWORK_SUGGESTIONS_SUCCESS){
            Dialogs.newInstance(frg.requireActivity().getSupportFragmentManager(), "Error", new String[]{
                frg.getString(R.string.error),
                frg.getString(R.string.errorWifi, status)
            });
            return false;
        }

        //Enable Wifi
        if(!wifiManager.isWifiEnabled()) {
            Intent panelIntent = new Intent(Settings.Panel.ACTION_INTERNET_CONNECTIVITY);
            frg.startActivity(panelIntent);
        }
        return true;
    }

    /**
     * Connect to wifi suggestion for API version more 29
     * @param frg Fragment used to launch activity
     * @param wifi Suggestion of connect to wifi
     * @return True if save correctly
     */
    @RequiresApi(api = Build.VERSION_CODES.R)
    private boolean more29(Fragment frg, WifiSuggestion wifi){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Settings.EXTRA_WIFI_NETWORK_LIST, createSuggestion(wifi));

        wifiAdd = new Intent(Settings.ACTION_WIFI_ADD_NETWORKS);
        wifiAdd.putExtras(bundle);

        WifiManager wifiManager = (WifiManager) frg.requireContext().getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        if(wifiManager.isWifiEnabled()) addWifi.launch(wifiAdd);
        else {
            Intent panelIntent = new Intent(Settings.Panel.ACTION_INTERNET_CONNECTIVITY);
            powerWifi.launch(panelIntent);
        }
        return false;
    }

    /**
     * Suggestion for connect to access point
     * @param wifi Suggestion of connect to wifi
     * @return List with wifi to connect
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private ArrayList<WifiNetworkSuggestion> createSuggestion(WifiSuggestion wifi){
        WifiNetworkSuggestion wifiEnd;
        if(wifi.sec == Pulse.TypeSec.OPEN){
            wifiEnd = new WifiNetworkSuggestion.Builder()
                .setSsid(wifi.SSID)
                .setIsHiddenSsid(wifi.hide)
                .build();
        } else if (wifi.sec == Pulse.TypeSec.WPA2){
            wifiEnd = new WifiNetworkSuggestion.Builder()
                .setSsid(wifi.SSID)
                .setWpa2Passphrase(wifi.pass)
                .setIsHiddenSsid(wifi.hide)
                .build();
        } else {
            wifiEnd = new WifiNetworkSuggestion.Builder()
                .setSsid(wifi.SSID)
                .setWpa3Passphrase(wifi.pass)
                .setIsHiddenSsid(wifi.hide)
                .build();
        }

        return new ArrayList<WifiNetworkSuggestion>(){{ add(wifiEnd); }};
    }
}
