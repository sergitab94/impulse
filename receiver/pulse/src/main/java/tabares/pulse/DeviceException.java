/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import androidx.annotation.NonNull;

public class DeviceException extends java.lang.Exception {

    /**
     * Different errors of the protocol pulse
     */
    public enum PulseError {
        SensorNotFound,
        FailedPulse,
        IncorrectRS,
        IncorrectCRC,
        DeviceOff,
        FailedBluetooth,
        MoveDevice,
        DeleteTask,
        WithoutStorage
    }

    private final PulseError pulseError;
    private final String data;

    /**
     * Generate a Error
     * @param pulseError Type of the Error
     */
    public DeviceException(PulseError pulseError){
        this.pulseError = pulseError;
        data = null;
    }

    /**
     * Generate a Error
     * @param pulseError Type of the error
     * @param data Short description
     */
    public DeviceException(PulseError pulseError, String data){
        this.pulseError = pulseError;
        this.data = data;
    }

    /**
     * Get type of the error
     * @return Type of the error
     */
    public PulseError getPulseError(){
        return pulseError;
    }

    /**
     * Convert the objet to string
     * @return Readable string for this object
     */
    @NonNull
    @Override
    public String toString() {
        String info = (data==null)? "" : " " + data;
        switch (pulseError){
            case SensorNotFound:
                return "Sensor not found." + info;
            case FailedPulse:
                return "Failed receive data." + info;
            case IncorrectCRC:
                return "The information is corrupt detected with CRC." + info;
            case IncorrectRS:
                return "The information is corrupt detected and irrecoverable with Reed Solomon." + info;
            case DeviceOff:
                return "Device bluetooth failed (Device is off)." + info;
            case FailedBluetooth:
                return "Device bluetooth failed (Interrupt connection)." + info;
            case MoveDevice:
                return "The device is move and the transmission is paused." + info;
            case DeleteTask:
                return "The task is deleted from computer." + info;
            case WithoutStorage:
                return "The device does not have available storage." + info;
            default:
                return "Without exception." + info;
        }
    }
}
