/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse.RS;

import java.util.ArrayList;

public class listRS{
    /**
     * Information of Solenoid
     */
    public static class infSol{
        public final int pos, m, n, k;
        public int sPulse;

        /**
         * Constructor of solenoid
         * @param pos Position of solenoid
         * @param sPulse Size in bits of solenoid
         * @param m Symbol size in bytes
         * @param n Code word size in symbols 2^(m)-1
         * @param k Message size in symbols n-2t (t: number of correctly maximum)
         */
        public infSol(int pos, int sPulse, int m, int n, int k) {
            this.pos=pos;
            this.sPulse=sPulse;
            this.m=m;
            this.n=n;
            this.k=k;
        }

        /**
         * Clone the object
         * @return The copy of the object
         */
        public infSol copy(){
            return new infSol(pos, sPulse, m, n, k);
        }
    }

    private final RS[] list;

    /**
     * Constructor of the object
     */
    public listRS(){
        list = new RS[3];
    }

    //X=0, Y=1, Z=2

    /**
     * Generate the necessary in Reed-Solomon
     * @param sol Size of solenoid (in order) X, Y and Z
     */
    public void createRS(int[] sol){
        for(int x=0; x<3; x++){
            if(sol[x]<1 || sol[x]>8) list[x]=null;
            else list[x]=new RS(sol[x]);
        }
    }

    /**
     * Obtain the information of Reed-Solomon
     * @return Solenoid information (in order) X, Y and Z
     */
    public infSol[] getInfo(){
        infSol[] end = new infSol[3];
        for(int x=0; x<3; x++) {
            if (list[x] == null) {
                end[x] = new infSol(x, 0, 0, 0, 0);
            } else {
                RS.rsInfo data = list[x].getInfo();
                end[x] = new infSol(x, data.sPulse, data.m, data.n, data.k);
            }
        }
        return end;
    }

    /**
     * Process data of Reed-Solomon
     * @param data List of bytes to process
     * @param sol Number of solenoid to process Reed-Solomon (in order) X, Y and Z
     * @return Corrected bytes list
     * @throws Exception Wrong correction
     */
    public ArrayList<Byte> getData(ArrayList<Byte> data, int sol) throws Exception{
        return list[sol].get(data);
    }
}
