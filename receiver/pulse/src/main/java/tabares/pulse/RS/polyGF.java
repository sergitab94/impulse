/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse.RS;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;

class polyGF {
    private final GF field;
    private final ArrayList<Integer> coefficients;

    /**
     * Constructor of polynomial
     * @param field {@link GF} of the polynomial
     */
    protected polyGF(GF field){
        this.field=field;
        this.coefficients=new ArrayList<>();
    }

    /**
     * Constructor of polynomial
     * @param field {@link GF} of the polynomial
     * @param coefficients Coefficient of the polynomial
     */
    protected polyGF(GF field, int coefficients){
        this.field=field;
        this.coefficients=new ArrayList<>();
        this.coefficients.add(coefficients);
    }

    /**
     * Create a polynomial with a degree of the coefficient
     * @param field Galois field
     * @param coefficients Coefficient of the polynomial
     * @param degree Degree of the coefficient
     */
    protected polyGF(GF field, int coefficients, int degree){
        this.field=field;
        this.coefficients=new ArrayList<>(Collections.nCopies(degree+1, 0));
        this.coefficients.set(0, coefficients);
    }

    /**
     * Create polynomial with a list of coefficients
     * @param field Galois field
     * @param coefficients Coefficients of the polynomial
     */
    protected polyGF(GF field, ArrayList<Integer> coefficients){
        this.field=field;
        int length=coefficients.size();

        if(length>1 && coefficients.get(0)==0){
            int firstNonZero=1;
            while((firstNonZero<length) && (coefficients.get(firstNonZero)==0)) firstNonZero++;

            if(firstNonZero==length) this.coefficients=new ArrayList<>(Collections.nCopies(1, 0));
            else this.coefficients=new ArrayList<>(coefficients.subList(firstNonZero, coefficients.size()));
        } else this.coefficients=new ArrayList<>(coefficients);
    }

    /**
     * Get coefficients
     * @return List of coefficients
     */
    protected ArrayList<Integer> getCoefficients(){
        return coefficients;
    }

    /**
     * Obtain a specific coefficient in a degree
     * @param degree Degree of the coefficient
     * @return Coefficient
     */
    protected Integer getCoefficient(int degree){
        if(degree+1>coefficients.size()) return 0;
        return coefficients.get(coefficients.size()-degree-1);
    }

    /**
     * Check if polynomial is Zero
     * @return True if polynomial is Zero
     */
    protected boolean isZero(){
        return (coefficients.size()==1 && coefficients.get(0)==0);
    }

    /**
     * Obtain the degree of the polynomial
     * @return Degree of the polynomial
     */
    protected int getDegree(){
        return coefficients.size()-1;
    }

    /**
     * Add one polynomial to other
     * @param b Polynomial to add to current
     * @return Polynomial add
     */
    protected polyGF add(polyGF b){
        if(isZero()) return new polyGF(field, b.getCoefficients());
        if(b.isZero()) return new polyGF(field, coefficients);

        ArrayList<Integer> small=new ArrayList<>(coefficients);
        ArrayList<Integer> large=new ArrayList<>(b.getCoefficients());

        if(small.size()>large.size()){
            small=new ArrayList<>(b.getCoefficients());
            large=new ArrayList<>(coefficients);
        }

        ArrayList<Integer> sumDiff=new ArrayList<>(Collections.nCopies(large.size(), 0));
        int lengthDiff=large.size()-small.size();

        for(int i=0; i<lengthDiff; i++) sumDiff.set(i, large.get(i));

        for(int i=lengthDiff; i<large.size(); i++)
            sumDiff.set(i, field.add(small.get(i-lengthDiff), large.get(i)));

        return new polyGF(field, sumDiff);
    }

    /**
     * Multiply one polynomial to other
     * @param b Polynomial to multiply to current
     * @return Polynomial multiply
     */
    protected polyGF multiply(polyGF b){
        if (isZero() || b.isZero()) return new polyGF(field, 0);

        int aLength = coefficients.size();
        int bLength = b.getCoefficients().size();
        ArrayList<Integer> bCoeff=b.getCoefficients();
        ArrayList<Integer> product=new ArrayList<>(Collections.nCopies(aLength+bLength-1, 0));


        for (int i=0; i<aLength; i++)
            for (int j=0; j<bLength; j++)
                product.set(i+j, field.add(product.get(i+j),
                        field.multiply(coefficients.get(i), bCoeff.get(j))));

        return new polyGF(field, product);
    }

    /**
     * Multiply one polynomial to a number
     * @param b Number to multiply to current
     * @return Polynomial multiply
     */
    protected polyGF multiply(int b){
        if (b==0) return new polyGF(field, 0);
        if (b==1) return new polyGF(field, coefficients);

        int size = coefficients.size();
        ArrayList<Integer> product=new ArrayList<>(Collections.nCopies(size, 0));
        for (int i = 0; i < size; i++)
            product.set(i, field.multiply(coefficients.get(i), b));

        return new polyGF(field, product);
    }

    /**
     * Divide one polynomial to other
     * @param b Polynomial to divide to current
     * @return Polynomial divide
     */
    protected polyGF divide(polyGF b){
        return tDiv(b).get(0);
    }

    /**
     * Rest of the division ot the one polynomial to other
     * @param b Polynomial to divide to current
     * @return Polynomial rest
     */
    protected polyGF mod(polyGF b){
        return tDiv(b).get(1);
    }

    /**
     * Divide one polynomial to other
     * @param b Polynomial to divide to current
     * @return First value return the divide. Second value return the rest.
     */
    protected ArrayList<polyGF> tDiv(polyGF b){
        if (b.isZero()) return new ArrayList<>();

        polyGF quotient=new polyGF(field, 0);
        polyGF remainder= new polyGF(field, coefficients);

        int denominator = b.getCoefficient(b.getDegree());
        int inverse =field.inverse(denominator);

        while(remainder.getDegree()>=b.getDegree() && !remainder.isZero()) {
            int degreeDiff=remainder.getDegree()-b.getDegree();
            int scale = field.multiply(remainder.getCoefficient((remainder.getDegree())), inverse);

            polyGF iteratorQuotient= new polyGF(field, scale, degreeDiff);
            polyGF term = b.multiply(iteratorQuotient);
            quotient=quotient.add(iteratorQuotient);

            remainder=remainder.add(term);
        }

        ArrayList<polyGF> result=new ArrayList<>();
        result.add(quotient);
        result.add(remainder);
        return result;
    }

    /**
     * Evaluate a polynomial
     * @param value Number to evaluate
     * @return Number of polynomial to evaluate
     */
    protected int eval(int value){
        int y = coefficients.get(0);
        for(int i = 1; i<coefficients.size(); i++)
            y = field.multiply(y, value) ^ coefficients.get(i);
        return y;
    }

    /**
     * Print polynomial
     * @return String to be read by humans
     */
    @NonNull
    @Override
    public String toString() {
        return "Coefficients: "+ coefficients + " Galois Field: " + field;
    }
}
