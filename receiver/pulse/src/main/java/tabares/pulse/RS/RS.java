/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse.RS;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

class RS {
    //Size Pulse
    private int sizePulse=0;

    //Reed Solomon
    private rsInfo tRS;
    private GF field;

    /**
     * Information of solenoid
     */
    protected static class rsInfo{
        protected final int poly, m, n, k, sPulse;

        /**
         *
         * @param poly Polynomial generator
         * @param m Size in bits of solenoid
         * @param n Code word size in symbols 2^(m)-1
         * @param k Code word size in symbols 2^(m)-1
         * @param sPulse Size in bits of solenoid
         */
        protected rsInfo(int poly, int m, int n, int k, int sPulse){
            this.poly=poly;
            this.m=m;
            this.n=n;
            this.k=k;
            this.sPulse=sPulse;
        }
    }

    private static final rsInfo[] type={
        new rsInfo( 11, 3,  7,  5, 21),
        new rsInfo( 19, 4, 15, 11, 30),
        new rsInfo( 11, 3,  7,  5,  7),
        new rsInfo( 19, 4, 15, 11, 15),
        new rsInfo( 37, 5, 31, 25, 31),
        new rsInfo( 67, 6, 50, 40, 50),
        new rsInfo(137, 7, 50, 40, 50),
        new rsInfo(285, 8, 50, 40, 50)
    };

    /**
     * Constructor of the class
     * @param size Bits of pulse
     */
    protected RS(int size){
        if(size<=0 || sizePulse==size) return;
        sizePulse=size;

        tRS=type[size-1];
        field=new GF(tRS.m, tRS.poly);
    }

    /**
     * Obtain the information of Reed-Solomon
     * @return Information of the solenoid
     */
    protected rsInfo getInfo(){
        return tRS;
    }

    /**
     * Process bytes, check redundancy and correct
     * @param rs Data to check
     * @return Data already corrected
     * @throws Exception Error to repair data
     */
    protected ArrayList<Byte> get(ArrayList<Byte> rs) throws Exception {
        ArrayList<Integer> rs_x = new ArrayList<>(), err_pos;
        ArrayList<Byte> end = new ArrayList<>();
        polyGF pRS, syndromes, err_loc, rs_out;
        int sizeRS=tRS.n-tRS.k;

        //Defragment and create polynomial
        if(sizePulse!=tRS.m) rs=defragment(rs, sizePulse, tRS.m);
        for(int x: rs) rs_x.add(x);
        pRS= new polyGF(field, rs_x);

        //Get Syndromes
        syndromes=calcSyndromes(pRS, sizeRS);
        if(syndromes.getDegree()==0) return new ArrayList<>(rs.subList(0, tRS.k));

        //Start repair
        err_loc = berlekampMassey(syndromes, sizeRS);
        err_pos = chienFind(err_loc);
        rs_out = chienForney(pRS, syndromes, err_pos, err_loc.getCoefficients());

        //Check if repair
        syndromes=calcSyndromes(rs_out, sizeRS);
        if(syndromes.getDegree()!=0) throw new Exception();

        //Save in RS
        for(int i=tRS.k-1; i>=0; i--) end.add((byte) ((int) rs_out.getCoefficient(i+sizeRS)));
        Log.d("Pulse", "Repaired: " + rs + " in " + end);
        return end;
    }

    /**
     * Join data if m!=sPulse
     * @param data Bytes to merge
     * @param iBit Bits to input in data
     * @param oBit Bits to output in return
     * @return Bytes merge
     */
    private ArrayList<Byte> defragment(ArrayList<Byte> data, int iBit, int oBit) {
        ArrayList<Byte> info = new ArrayList<>();
        int bit = oBit - 1;
        byte bData = 0x00;

        for (Byte aByte : data) {
            bData = (byte) ((bData << iBit) | aByte);
            if (bit == 0) {
                info.add(bData);
                bData = 0x00;
            }
            bit = (bit + 1) % oBit;
        }

        return info;
    }

    /**
     * Calculate syndromes.
     * @param pRS Message converted in polynomial
     * @param nsym Number o error correcting symbols
     * @return Polynomial syndromes
     */
    private polyGF calcSyndromes(polyGF pRS, int nsym){
        ArrayList<Integer> syndromes_x=new ArrayList<>();
        for(int i=0; i<nsym; i++) {
            syndromes_x.add(pRS.eval(field.exp(i)));
        }
        return new polyGF(field, syndromes_x);
    }

    /**
     * Find error locator with Berlekamp-Massey algorithm
     * @param synd Syndrome to repair
     * @param nsym Number o error correcting symbols
     * @return Error locator
     */
    private polyGF berlekampMassey(polyGF synd, int nsym){
        ArrayList<Integer> err_loc = new ArrayList<>(), old_loc = new ArrayList<>();
        ArrayList<Integer> synd_x = synd.getCoefficients();
        int synd_shift=0;

        err_loc.add(1);
        old_loc.add(1);

        if(synd_x.size()>nsym) synd_shift=synd_x.size()-nsym;

        for(int i=0; i<nsym; i++){
            int K = i+synd_shift;
            int delta = synd_x.get(K);
            for(int j=1; j<err_loc.size(); j++){
                delta ^= field.multiply(err_loc.get(err_loc.size()-j-1), synd_x.get(K-j));
            }

            old_loc.add(0);

            if(delta!=0){
                if(old_loc.size()>err_loc.size()){
                    polyGF new_loc = new polyGF(field, old_loc).multiply(delta);
                    old_loc = new polyGF(field, err_loc)
                            .multiply(field.inverse(delta))
                            .getCoefficients();

                    err_loc = new_loc.getCoefficients();
                }
                err_loc = new polyGF(field, err_loc)
                        .add(new polyGF(field, old_loc).multiply(delta))
                        .getCoefficients();
            }
        }
        return new polyGF(field, err_loc);
    }

    /**
     * Find the roots of error polynomial by bruteforce trial
     * @param synd Syndrome to find errors
     * @return Errors find
     */
    private ArrayList<Integer> chienFind(polyGF synd){
        ArrayList<Integer> error_pos_x = new ArrayList<>();
        for(int i=tRS.n; i>=1; i--) {
            if (synd.eval(field.exp(i)) == 0) error_pos_x.add(i);
        }
        return error_pos_x;
    }

    /**
     * Compute the error evaluator polynomial from the syndrome and error locator
     * @param synd Syndrome of the data
     * @param err_loc Errors find
     * @return Error evaluator polynomial
     */
    private polyGF findEvaluatorInverse(polyGF synd, ArrayList<Integer> err_loc){
        ArrayList<Integer> synd_rev_x = synd.getCoefficients();
        Collections.reverse(synd_rev_x);
        synd_rev_x.add(0);
        ArrayList<Integer> rem_x= synd.multiply(new polyGF(field, err_loc)).getCoefficients();
        return new polyGF(field, new ArrayList<>(rem_x.subList(
                rem_x.size()-err_loc.size(), rem_x.size())));
    }

    /**
     * Forney algorithm, computes the values (error magnitude) to correct the input message.
     * @param pRS Polynomial message
     * @param synd Syndrome of the data
     * @param err_pos Errors position
     * @param err_loc Errors find
     * @return Polynomial message repaired
     * @throws Exception Error to repair data
     */
    private polyGF chienForney(polyGF pRS, polyGF synd, ArrayList<Integer> err_pos, ArrayList<Integer> err_loc) throws Exception {
        ArrayList<Integer> coef_pos=new ArrayList<>(), X = new ArrayList<>();
        ArrayList<Integer> E = new ArrayList<>(Collections.nCopies(tRS.n, 0));
        polyGF err_eval=findEvaluatorInverse(synd, err_loc);
        int Xlength;

        //Calculate errata locator
        for(int i: err_pos) coef_pos.add(tRS.n-i);

        //Second part Chien search
        for(int i: coef_pos) X.add(field.exp(i));

        //Forney
        Xlength=X.size();
        for(int i=0; i<Xlength; i++){
            int Xi=X.get(i), Xi_inv=field.inverse(Xi), err_prime = 1, y;

            for(int j=0; j<Xlength; j++){
                if(j!=i){
                    err_prime = field.multiply(err_prime,
                            field.add(1, field.multiply(Xi_inv, X.get(j))));
                }
            }

            if(err_prime==0) throw new Exception();

            y=err_eval.eval(Xi_inv);
            y=field.multiply(Xi,y);
            E.set(err_pos.get(i)-1, field.divide(y, err_prime));
        }

        return pRS.add(new polyGF(field, E));
    }
}
