/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse.RS;

class GF {
    private final int[] expTable = new int[510], logTable = new int[256];
    private final int size;

    /**
     * Create Galois fields, generate exponential and logarithmic tables
     * @param m Maximum exponent of Galois field in base 2 (2^m)
     * @param primitive Polynomial irreducible
     */
    protected GF(int m, int primitive) {
        int x = 1, mask = (int) Math.pow(2, m);
        size = (int) Math.pow(2, m) - 1;
        for (int i = 0; i < size; i++) {
            expTable[i] = x;
            expTable[i + size] = x;
            x <<= 1;
            if (x >= mask) x ^= primitive;
        }
        for (int i = 0; i < size; i++) logTable[expTable[i]] = i;
    }

    /**
     * Add two numbers in Galois Field
     * @param a First number to add
     * @param b Second number to add
     * @return Result of add
     */
    protected int add(int a, int b) {
        return a ^ b;
    }

    /**
     * Multiply two numbers in Galois Field
     * @param a First number to multiply
     * @param b Second number to multiply
     * @return Result of multiply
     */
    protected int multiply(int a, int b) {
        if (a == 0 || b == 0) return 0;
        if (a == 1) return b;
        if (b == 1) return a;
        return expTable[logTable[a] + logTable[b]];
    }

    /**
     * Divide a number in Galois Field
     * @param a Dividend number
     * @param b Divisor number
     * @return Result of the divide
     */
    protected int divide(int a, int b) {
        if (a == 0 || b == 0) return 0;

        return expTable[(logTable[a] + size - logTable[b]) % size];
    }

    /**
     * Exponent a number in Galois Field
     * @param a Number to exponent
     * @return Result of the exponent
     */
    protected int exp(int a) {
        return expTable[a];
    }

    /**
     * Invert a number
     * @param a Number to invert
     * @return Result of the invert
     */
    protected int inverse(int a) {
        if (a == 0) return 0;
        return expTable[size - logTable[a]];
    }
}
