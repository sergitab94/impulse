/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

class ab_Bluetooth {
    private static final int time=300_000;
    private static final int GATT_ERROR=133, GATT_CONN_TERMINATE_LOCAL_HOST=22;

    private final Pulse pulse;
    private final Listeners listener;
    private final b_Application application;
    private final Context context;
    private final Handler mHandler;

    private BluetoothGatt mGatt = null;
    private BluetoothGattCharacteristic digitalOutput = null;
    private boolean isConnect;
    private byte[] mac, pin;
    private long timeStamp;

    private static final String TAG = "Pulse";

    /**
     * Initialize layer and variables
     * @param pulse Top layer (used in application for exchange between {@link aa_DataLink} and
     *              {@link ab_Bluetooth}
     * @param listener Called when an events occurs
     * @param context Context of the application
     * @param mHandler Send and process Runnable task
     */
    protected ab_Bluetooth(Pulse pulse, Listeners listener, Context context, Handler mHandler){
        this.pulse=pulse;
        this.listener=listener;
        this.context=context;
        this.mHandler = mHandler;
        isConnect = false;

        application = new b_Application(pulse, listener);

        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(stateDev, filter);

        IntentFilter pairingRequestFilter = new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST);
        pairingRequestFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY - 1);
        context.registerReceiver(pairing, pairingRequestFilter);
    }

    /**
     * Reset this layer
     */
    protected void resetLayer(){
        application.resetLayer();
    }

    /**
     * Stop callback and stop bluetooth
     */
    protected void unregisterReceiver(){
        context.unregisterReceiver(stateDev);
        context.unregisterReceiver(pairing);
        resetGatt();
    }

    /**
     * Connect to specific device
     * @param mac Unique identifier for device
     * @param pin Numeric password for connect to device
     */
    protected void connect(byte[] mac, int pin){
        this.mac=mac;
        String pinSt=String.format(Locale.ENGLISH,"%06d", pin);
        StringBuilder macSt = new StringBuilder();
        for(byte m: mac) macSt.append(String.format("%02X", m & 0xFF)).append("-");
        macSt.deleteCharAt(macSt.length()-1);

        Log.d(TAG, "Connect to MAC:" + macSt + " with PIN:"+ pinSt);
        this.pin = pinSt.getBytes(StandardCharsets.UTF_8);

        resetGatt();

        BluetoothAdapter.getDefaultAdapter().startDiscovery();
        BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mac);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            mGatt = device.connectGatt(context, false, gattCallback,
                    BluetoothDevice.TRANSPORT_LE, BluetoothDevice.PHY_OPTION_NO_PREFERRED, mHandler);
        else mGatt = device.connectGatt(context, false, gattCallback);

        timeStamp = System.currentTimeMillis();
    }

    /**
     * Reset {@link b_Application} and stop bluetooth
     */
    protected void disconnect(){
        application.resetLayer();
        resetGatt();
    }

    /**
     * Send to device the order of the power off
     */
    private void powerOff(){
        if(digitalOutput==null) return;
        mHandler.post(() -> {
            final byte[] bytesToWrite = {0x00};
            digitalOutput.setValue(bytesToWrite);
            mGatt.writeCharacteristic(digitalOutput);
        });
    }

    /**
     * Disconnect the device and stop discovery
     */
    private void resetGatt(){
        if(mGatt!=null){
            mGatt.disconnect();
            mGatt.close();
            mGatt=null;
        }
        if(digitalOutput!=null) digitalOutput = null;
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
        try { BluetoothAdapter.getDefaultAdapter().cancelDiscovery(); }
        catch (java.lang.SecurityException ignored){ }
        isConnect=false;
    }

    /**
     * Broadcaster to detect the different states of the {@link BluetoothAdapter} (State on/off and
     * turning on/off)
     */
    private final BroadcastReceiver stateDev = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

            if (state==BluetoothAdapter.STATE_OFF) {
                if(isConnect){
                    Log.d(TAG, "Device is shutdown");
                    pulse.changeToDataLink();
                }
                resetGatt();
                isConnect=false;
                listener.error(new DeviceException(DeviceException.PulseError.DeviceOff));
                listener.state(Pulse.StateDev.error);
            } else listener.state(Pulse.StateDev.preparePulse);
        }
        }
    };

    /**
     * Pairing for the {@link BluetoothDevice}
     */
    private final BroadcastReceiver pairing = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        BluetoothDevice dev= intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        int type = intent.getIntExtra(BluetoothDevice.EXTRA_PAIRING_VARIANT, BluetoothDevice.ERROR);
        if(type==BluetoothDevice.PAIRING_VARIANT_PIN){
            dev.setPin(pin);
            abortBroadcast();
            if (!mGatt.discoverServices()) resetGatt();
        }
        }
    };

    /**
     * Callbacks for BLE GATT services
     */
    private final BluetoothGattCallback gattCallback=new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d(TAG, "Bluetooth Connect");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "Bluetooth Disconnect");
                resetGatt();
                if(timeStamp+time<System.currentTimeMillis()) {
                    Log.d(TAG, "Device timeout");
                    pulse.changeToDataLink();
                    return;
                }

                if(status == GATT_ERROR || status == GATT_CONN_TERMINATE_LOCAL_HOST) {
                    Log.d(TAG, "Retry connect");
                    BluetoothAdapter.getDefaultAdapter().startDiscovery();

                    BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(mac);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        mGatt = device.connectGatt(context, false, gattCallback,
                                BluetoothDevice.TRANSPORT_LE, BluetoothDevice.PHY_OPTION_NO_PREFERRED, mHandler);
                    else mGatt = device.connectGatt(context, false, gattCallback);
                } else pulse.changeToDataLink();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            mHandler.postDelayed(() -> {
                for(BluetoothGattService service : gatt.getServices()){
                    int idService= ((int) (service.getUuid().getMostSignificantBits()>>32)) & 0xFF;
                    if(idService>1) {
                        BluetoothGattCharacteristic characteristic = service.getCharacteristics().get(0);
                        if(characteristic==null) {
                            Log.e(TAG, "Fail bluetooth characteristic");
                            resetGatt();
                            return;
                        }

                        if(!gatt.setCharacteristicNotification(characteristic, true)) {
                            Log.e(TAG, "Fail enable notification");
                            resetGatt();
                            return;
                        }

                        BluetoothGattDescriptor descriptor = characteristic.getDescriptors().get(0);
                        if(descriptor==null) {
                            Log.e(TAG, "Fail bluetooth obtain descriptor");
                            resetGatt();
                            return;
                        }

                        int properties = characteristic.getProperties();
                        if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0)
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        else if ((properties & BluetoothGattCharacteristic.PROPERTY_INDICATE) > 0)
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);

                        if(!gatt.writeDescriptor(descriptor)) {
                            resetGatt();
                            Log.e(TAG, "Fail write descriptor");
                            return;
                        }

                        digitalOutput = characteristic;
                        isConnect = true;
                        listener.state(Pulse.StateDev.receiveBlue);
                        return;
                    }
                }
            }, 2000);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            mHandler.post(() -> {
                super.onCharacteristicChanged(gatt, characteristic);
                application.addFrame(characteristic.getValue());
                if(application.isEnd()) {
                    powerOff();
                    pulse.changeToDataLink();
                }
            });
        }
    };
}
