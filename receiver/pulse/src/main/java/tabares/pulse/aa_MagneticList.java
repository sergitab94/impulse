/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class aa_MagneticList extends ArrayList<aa_MagneticField> {
    /**
     * Delete all rank out of X milliseconds of the end |//////////--|. The list modify itself.
     * @param timeRangeEnd Milliseconds to delete
     */
    protected void deleteEndLeft(long timeRangeEnd) {
        int size = size();
        if (size == 0) return;
        deleteLeft(get(size - 1).getTimestamp() - (timeRangeEnd * 1_000_000));
    }

    /**
     * Delete rank with lower or equal timestamp |//////////--|. The list modify itself.
     * @param timeStamp TimeStamp to delete
     */
    protected void deleteLeft(long timeStamp) {
        int timePos;
        if (size() == 0) return;
        for (timePos = size() - 1; timePos >= 0; timePos--)
            if (timeStamp >= get(timePos).getTimestamp()) {
                if (timeStamp > get(timePos).getTimestamp()) timePos++;
                break;
            }
        if (timePos >= 0) removeRange(0, timePos);
    }

    /**
     * Delete rank out of range between two timestamp |/////--/////|. The list modify itself.
     * @param startTime Timestamp to start range
     * @param endTime Timestamp to end range
     */
    protected void deleteRange(long startTime, long endTime) {
        int timePos, len=size();
        if (len == 0) return;
        for(timePos=0; timePos<len; timePos++)
            if(startTime <= get(timePos).getTimestamp()){
                if (startTime == get(timePos).getTimestamp()) timePos--;
                break;
            }

        if(timePos >=0) removeRange(0, timePos);

        len=size();
        if (len == 0) return;
        for(timePos=0; timePos<len; timePos++)
            if(endTime <= get(timePos).getTimestamp()){
                if (endTime == get(timePos).getTimestamp()) timePos--;
                break;
            }

        if(timePos >=0) removeRange(timePos, len);
    }

    /**
     * Calculate median in the List
     * @param mask Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1 compares
     *             only the X axis.
     * @return A value to median of the list
     */
    private aa_MagneticField median(int mask) {
        float[] finish = {0, 0, 0};
        if (size() == 0) return new aa_MagneticField(finish, 0);

        List<aa_MagneticField> sort = new ArrayList<>(this);
        Collections.sort(sort, new cmpMagnetic(mask));
        int half = size() / 2;
        if ((size() % 2) == 1) return sort.get(half);

        float[] values1 = sort.get(half - 1).getValues(), values2 = sort.get(half).getValues();
        for (int i = 0; i < 3; i++) finish[i] = (values1[i] + values2[i]) / 2;
        long time = (sort.get(half - 1).getTimestamp() + sort.get(half).getTimestamp()) / 2;
        return new aa_MagneticField(finish, time);
    }

    /**
     * Calculate average in the List
     * @param axis Specific axis (X: 0; Y: 1; Z: 2)
     * @return Average of the list
     */
    protected float avg(int axis) {
        if (0 > axis || axis > 2) return 0;
        float finish = 0;
        for (int n = 0; n < size(); n++) finish += get(n).getValues()[axis];
        return finish / size();
    }

    /**
     * Calculate maximum in the List
     * @param axis Specific axis (X: 0; Y: 1; Z: 2)
     * @return Maximum of the list
     */
    protected float max(int axis) {
        if (0 > axis || axis > 2) return 0;
        if (size() == 0) return 0;
        float finish = 0;
        for (int n = 0; n < size(); n++) {
            float tFinish = get(n).getValues()[axis];
            if (tFinish > finish) finish = tFinish;
        }
        return finish;
    }

    /**
     * Calculate minimum in the List
     * @param axis Specific axis (X: 0; Y: 1; Z: 2)
     * @return Minimum of the list
     */
    protected float min(int axis) {
        if (0 > axis || axis > 2) return 0;
        if (size() == 0) return 0;
        float finish = 0;
        for (int n = 0; n < size(); n++) {
            float tFinish = get(n).getValues()[axis];
            if (tFinish < finish) finish = tFinish;
        }
        return finish;
    }

    /**
     * Check if the is equal in a specific range
     * @param range Maximum range that makes one point different from the original
     * @param mask Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1 compares
     *             only the X axis.
     * @return True if is equal
     */
    protected boolean isEqual(int range, int mask) {
        if (size() < 2) return true;
        float[] finish = {0, 0, 0};
        aa_MagneticField median;

        List<aa_MagneticField> sort = new ArrayList<>(this);
        Collections.sort(sort, new cmpMagnetic(mask));
        int half = size() / 2;
        if ((size() % 2) == 1) median = sort.get(half);
        else {
            float[] values1 = sort.get(half - 1).getValues(), values2 = sort.get(half).getValues();
            for (int i = 0; i < 3; i++) finish[i] = (values1[i] + values2[i]) / 2;
            long time = (sort.get(half - 1).getTimestamp() + sort.get(half).getTimestamp()) / 2;
            median = new aa_MagneticField(finish, time);
        }

        return (median.compare(sort.get(0), range, mask) == 0) &&
                (median.compare(sort.get(size() - 1), range, mask) == 0);
    }

    /**
     * Get maximum and minimum
     * @param max Output list with maximum of this list
     * @param min Output list with minimum of this list
     * @param range Maximum range that makes one point different from the original
     * @param mask Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1 compares
     *             only the X axis.
     */
    protected void getMaxMin(aa_MagneticList max, aa_MagneticList min, int range, int mask) {
        aa_MagneticList eq = new aa_MagneticList();
        float pComp = 0, trend = 0;
        int rangeComp = range;
        max.clear();
        min.clear();

        //>0 Decrease, 0: Equal, <0 Increase
        for (int n = 0; n < size() - 1; n++) {
            aa_MagneticField act = get(n);
            if (pComp == 0) {
                eq.add(act);
                act = eq.median(mask);
            }

            float comp = act.compare(get(n + 1), rangeComp, mask);

            if (!((pComp < 0 && comp < 0) || (pComp > 0 && comp > 0))) {
                float tCont = act.compare(get(n + 1), range, mask);

                //If it detects that it is within the appropriate range, it updates the comparison to equal
                if (tCont == 0) comp = 0;

                //If it detects a difference, it lowers the range to 0, otherwise it equals it
                rangeComp = (tCont == 0) ? range : 0;
            }

            if (trend <= 0 && comp > 0) max.add(act);
            else if (trend >= 0 && comp < 0) min.add(act);

            if (pComp != 0 && comp == 0) eq.add(get(n));
            else if (comp != 0) eq.clear();

            pComp = comp;
            if (comp != 0) trend = comp;
        }
    }

    /**
     * Gets the field near to the timestamp without exceeding it
     * @param timeStamp TimeStamp to obtain
     * @return Magnetic field near
     */
    protected aa_MagneticField getFieldTimestamp(long timeStamp) {
        if (size() == 0) return new aa_MagneticField(new float[]{0, 0, 0}, 0);
        aa_MagneticField selectField = get(0);
        long timeMin = Math.abs(timeStamp - selectField.getTimestamp());

        for (int i = 1; i < size(); i++) {
            long timeAct = Math.abs(timeStamp - get(i).getTimestamp());
            if (timeAct > timeMin) return selectField;
            selectField = get(i);
            timeMin = timeAct;
        }

        return selectField;
    }

    /**
     * Gets the final maximum or minimum out of a range
     * @param range Maximum range that makes one point different from the original
     * @param mask Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1 compares
     *             only the X axis.
     * @return End maximum or minimum
     */
    protected ArrayList<aa_MagneticList> outRange(int range, int mask) {
        int rangeComp = range;
        float pComp = 0;

        aa_MagneticList eq = new aa_MagneticList();
        ArrayList<aa_MagneticList> listFinal = new ArrayList<>();

        //>0: Decrease, 0: Equal, <0: Increase
        for (int n = 0; n < size() - 1; n++) {
            aa_MagneticField act = get(n);
            if (pComp == 0) {
                eq.add(act);
                act = eq.median(mask);
            }

            float comp = act.compare(get(n + 1), rangeComp, mask);

            if (!((pComp < 0 && comp < 0) || (pComp > 0 && comp > 0))) {
                float tCont = act.compare(get(n + 1), range, mask);
                //If it detects that it is within the appropriate range, it updates the comparison to equal
                if (tCont == 0) comp = 0;

                //If it detects a difference, it lowers the range to 0, otherwise it equals it
                rangeComp = (tCont == 0) ? range : 0;
            }

            if (pComp != 0 || comp != 0) {
                if (pComp == 0) listFinal.add(new aa_MagneticList());
                (listFinal.get(listFinal.size() - 1)).add(get(n));

                if (pComp != 0 && comp != 0 && (pComp < 0) != (comp < 0)) {
                    listFinal.add(new aa_MagneticList());
                    (listFinal.get(listFinal.size() - 1)).add(get(n));
                }
            }

            if (pComp != 0 && comp == 0) eq.add(get(n));
            else if (comp != 0) eq.clear();

            pComp = comp;
        }
        return listFinal;
    }

    /**
     * Average timestamp in a list
     * @param axis Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1 compares
     *             only the X axis.
     * @return Timestamp of average
     */
    protected long avgTime(int axis) {
        float rangeMax = get(size() - 1).getValues()[axis] - get(0).getValues()[axis];
        float timestamp = 0;

        for (int i = 0; i < size() - 1; i++) {
            float range = (get(i + 1).getValues()[axis] - get(i).getValues()[axis]) / rangeMax;
            timestamp += ((get(i).getTimestamp() + get(i + 1).getTimestamp()) / 2.0) * range;
        }
        return (long) timestamp;
    }

    /**
     * Copy the Magnetic list in other object
     * @return Clone the magnetic list
     */
    protected aa_MagneticList copy() {
        return (aa_MagneticList) clone();
    }

    /**
     * Implement a comparator to order list
     */
    private static class cmpMagnetic implements Comparator<aa_MagneticField> {
        private final int mask;

        /**
         * Constructor of the class
         * @param mask Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1
         *             compares only the X axis.
         */
        public cmpMagnetic(int mask) {
            this.mask = mask;
        }

        /**
         * Compare two Magnetic fields
         * @param mag1 First magnetic field to compare
         * @param mag2 Second magnetic field to compare
         * @return If r&gt;0 → mag2&gt;mag1; If r=0 → mag2=mag1; If r&lt;0 → mag2&lt;mag1
         */
        @Override
        public int compare(aa_MagneticField mag1, aa_MagneticField mag2) {
            return Math.round(mag1.compare(mag2, 0, mask) * 1_000_000);
        }
    }
}