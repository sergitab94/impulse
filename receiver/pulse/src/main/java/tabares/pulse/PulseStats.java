/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import android.hardware.SensorEvent;

public class PulseStats {
    private long max=0, min=0, init=0;
    private long previous=0, startUnix=0, endUnix=0;
    private double avg=0;

    /**
     * Add value event to the statistic
     * @param sensorEvent Information of the {@link SensorEvent} to add
     */
    protected void addValue(SensorEvent sensorEvent){
        long data = sensorEvent.timestamp;
        long diff = data-previous;
        previous=data;
        if (init==0) startUnix = data;
        else if (init==1) min = diff;

        if (init>0){
            avg+=(diff-avg)/init;
            if (diff > max) max = diff;
            if (diff < min) min = diff;
        }

        endUnix=data;
        init++;
    }

    /**
     * Get average of the all data
     * @return Average
     */
    public double getAvg() {
        return avg;
    }

    /**
     * Get maximum of the all data
     * @return Maximum
     */
    public long getMax() {
        return max;
    }

    /**
     * Get minimum of the all data
     * @return Minimum
     */
    public long getMin() {
        return min;
    }

    /**
     * Gets the total number of events
     * @return Number of events
     */
    public long getInit() {
        return init;
    }

    /**
     * Get timestamp of the first element
     * @return Timestamp start
     */
    public long getStartUnix() {
        return startUnix;
    }

    /**
     * Get timestamp of the end element
     * @return Timestamp end
     */
    public long getEndUnix() {
        return endUnix;
    }

    /**
     * Check if the device is compatible
     * @return True if the device is compatible
     * @throws Exception Needs more time on to check if the device is compatible
     */
    public boolean isCompatible() throws  Exception{
        if((endUnix-startUnix)<= 15_000_000_000L) throw new Exception("Error: Insufficient time");
        return (max<50_000_000);
    }

    protected void reset(){
        max=0; min=0; init=0;
        previous=0; startUnix=0; endUnix=0;
        avg=0;
    }
}
