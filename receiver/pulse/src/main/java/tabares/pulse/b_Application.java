/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import tabares.pulse.CRC.CRC;

class b_Application {
    //Information up layers
    private final Listeners listener;
    private final boolean modeRelease;

    //Logic
    private int headerProcess;
    private long payloadProcess;
    private boolean error;

    //Output information
    private final ArrayList<String> args;
    private final ByteArrayOutputStream buffer;

    //Bluetooth
    private final byte[] mac = new byte[6];
    private int pin;

    //Additional information
    private final CRC crc;
    private byte crcResult;
    private long size;
    private Pulse.TypeTask task;
    private final Pulse pulse;

    private static final String TAG = "Pulse";

    /**
     * Initialize layer and all variables
     * @param pulse Allow exchange between {@link aa_DataLink} and {@link ab_Bluetooth}
     * @param listener Called when an events occurs
     */
    protected b_Application(Pulse pulse, Listeners listener){
        this.pulse=pulse;
        this.listener=listener;
        modeRelease =listener instanceof Listeners.PulseListener;
        crc = new CRC();

        //Logic
        headerProcess=0;
        payloadProcess=0;
        error=false;

        //Output data
        args = new ArrayList<>();
        buffer = new ByteArrayOutputStream();
    }

    /**
     * Check if the task not is Bluetooth
     * @return If not is a task bluetooth send True
     */
    protected boolean notIsBluetooth(){
        return task!=Pulse.TypeTask.Bluetooth;
    }

    /**
     * Callback when {@link aa_DataLink} or {@link ab_Bluetooth} add new frames to current
     * information
     * @param data New frame add
     */
    protected void addFrame(byte[] data){
        Log.d(TAG, "Frame: " + Arrays.toString(data));
        for(byte b: data) if(processByte(b)) return;
    }

    /**
     * Callback when {@link aa_DataLink} or {@link ab_Bluetooth} add new frames to current
     * information
     * @param data New frame add
     */
    protected void addFrame(ArrayList<Byte> data){
        Log.d(TAG, "Frame: " + data);
        for(byte b: data) if(processByte(b)) return;
    }

    /**
     * Process headers byte to byte of information (generic)
     * @param b Byte of data to process
     * @return True if not is necessary process more information
     */
    private boolean processByte(byte b){
        crc.add(b);
        if (headerProcess>=11){ //12-ToEnd
            if(task!=Pulse.TypeTask.Bluetooth) return processData(b);
            return true;
        }else if(headerProcess==0){ //1
            //Reset CRC
            crcResult = b;
            crc.reset();
        }else if(headerProcess==1){ //2
            try {
                task = byteToTask(b);
            } catch (DeviceException e) {
                listener.error(e);
                error = true;
                return true;
            }
        }else if(task==Pulse.TypeTask.Bluetooth){
            if(bluetoothByte(b)) return true; //3-11
        } else if(pulseByte(b)) return true; //3-7 (force move to 11)

        headerProcess++;
        return false;
    }

    /**
     * Process byte pulse to connect phone to device bluetooth (only task bluetooth)
     * @param b Byte of data to process
     * @return True if not is necessary process more information
     */
    private boolean bluetoothByte(byte b){
        if(headerProcess==2) pin=0; //3
        if(headerProcess<=7) mac[headerProcess-2]=b; //3-8
        else if (headerProcess<=9) pin=(pin<<8)|(((int) b)&0xFF); //9-10
        else { //11
            pin=(pin<<8)|(((int) b)&0xFF);
            boolean macVoid = true;
            for(byte m:mac) if(m!=0) {
                macVoid = false;
                break;
            }
            if(macVoid || pin==0) {
                listener.error(new DeviceException(DeviceException.PulseError.FailedPulse,
                    "Void data for Bluetooth"));
                resetLayer();
                error=true;
                return true;
            }
            if(crc.evaluate(crcResult)){
                listener.error(new DeviceException(DeviceException.PulseError.IncorrectCRC,
                    "CRC calculate: 0x" + intHex(crc.getCrc()) + ", receive: 0x" + intHex(crcResult)));
                resetLayer();
                error=true;
                return true;
            }
            crc.reset();
            pulse.changeToBluetooth(mac, pin);
        }
        return false;
    }

    /**
     * Process headers of a task other than bluetooth (not task bluetooth)
     * @param b Byte of data to process
     * @return True if not is necessary process more information
     */
    private boolean pulseByte(byte b){
        if(headerProcess==2) size=0; //3
        if(headerProcess<=5) size=(size<<8)|(((int) b)&0xFF); //4-6
        else { //7
            if(crc.evaluate(crcResult)){
                listener.error(new DeviceException(DeviceException.PulseError.IncorrectCRC,
                    "CRC calculate: 0x" + intHex(crc.getCrc()) + ", receive: 0x" + intHex(crcResult)));
                resetLayer();
                error=true;
                return true;
            }
            crc.reset();
            crcResult=b;
            payloadProcess=0;

            //Output Information reset
            buffer.reset();
            args.clear();

            headerProcess=11;
        }
        return false;
    }

    /**
     * Reset this layer
     */
    protected void resetLayer(){
        headerProcess=0;
    }

    /**
     * Check if the layer is end
     * @return True if is End
     */
    protected boolean isEnd() {
        if(error){
            error = false;
            return true;
        }
        if(payloadProcess>=size){
            resetLayer();
            return true;
        }
        return false;
    }

    /**
     * Convert integer to hexadecimal string
     * @param b Integer to convert
     * @return String integer in format byte
     */
    private String intHex(int b){
        return String.format(Locale.ENGLISH, "%02X",b & 0xFF);
    }

    /**
     * Convert byte to {@link Pulse.TypeTask}
     * @param data Byte to convert
     * @return The {@link Pulse.TypeTask} converted
     * @throws DeviceException Non-viable conversion
     */
    private Pulse.TypeTask byteToTask(byte data) throws DeviceException {
        switch (data){
            case 0x00: return Pulse.TypeTask.Bluetooth;
            case 0x01: return Pulse.TypeTask.File;
            case 0x02: return Pulse.TypeTask.Message;
            case 0x03: return Pulse.TypeTask.Wifi;
            case 0x04: return Pulse.TypeTask.Sms;
            case 0x05: return Pulse.TypeTask.Email;
            case 0x06: return Pulse.TypeTask.Bitcoin;
            default:
                throw new DeviceException(DeviceException.PulseError.FailedPulse, "Task Unknown: " + data);
        }
    }

    /**
     * Process payload of the task byte to byte (not task bluetooth)
     * @param b Byte to process
     * @return True if not is necessary process more information
     */
    private boolean processData(byte b){
        payloadProcess++;
        switch (task){
            case File: file(b); break;
            case Message: message(b); break;
            case Wifi: wifi(b); break;
            case Sms:
            case Email:
            case Bitcoin: smsEmailBitcoin(b); break;
        }

        //Check CRC
        if(payloadProcess>=size){
            if(crc.evaluate(crcResult)){
                listener.error(new DeviceException(DeviceException.PulseError.IncorrectCRC,
                    "CRC calculate: 0x" + intHex(crc.getCrc()) + ", receive: 0x" + intHex(crcResult)));
            } else if(modeRelease) ((Listeners.PulseListener) listener).information(task, args.toArray(new String[0]));

            crc.reset();
            resetLayer();
            listener.progress(100);
            return true;
        }

        //Send progress
        listener.progress((payloadProcess/(size*1.0F))*100);
        return false;
    }

    /**
     * Process byte to task File
     * @param b Byte to process
     */
    private void file(byte b){
        if(payloadProcess<=size){
            if(args.size()>0 && modeRelease) ((Listeners.PulseListener) listener).fileWrite(b);
            else getField(b);
        }
    }

    /**
     * Process byte to task Message
     * @param b Byte to process
     */
    private void message(byte b){
        if(payloadProcess<size) buffer.write(b);
        else {
            if(payloadProcess==size) buffer.write(b);
            args.add(new String(buffer.toByteArray(), StandardCharsets.UTF_8));
        }
    }

    /**
     * Process byte to task Wifi
     * @param b Byte to process
     */
    private void wifi(byte b){
        if(payloadProcess<size) {
            if (payloadProcess == 1) {
                args.add(String.valueOf((0x80 & b) > 0));

                switch (0x7F & b) {
                    case 0x00:
                        args.add(Pulse.TypeSec.OPEN.name());
                        break;
                    case 0x01:
                        args.add(Pulse.TypeSec.WPA2.name());
                        break;
                    case 0x02:
                        args.add(Pulse.TypeSec.WPA3.name());
                        break;
                    default:
                        listener.error(new DeviceException(DeviceException.PulseError.FailedPulse,
                                "Security unknown"));
                }
                return;
            }
            getField(b);
        } else {
            if(payloadProcess==size) {
                if(b==0x00) {
                    args.add(new String(buffer.toByteArray(), StandardCharsets.UTF_8));
                    buffer.reset();
                } else buffer.write(b);
            }
            args.add(new String(buffer.toByteArray(), StandardCharsets.UTF_8));
        }
    }

    /**
     * Process byte to tasks SMS, Email and Bitcoin
     * @param b Byte to process
     */
    private void smsEmailBitcoin(byte b){
        if(payloadProcess<size) getField(b);
        else {
            if(payloadProcess==size) buffer.write(b);
            args.add(new String(buffer.toByteArray(), StandardCharsets.UTF_8));
        }
    }

    /**
     * Gets field up to byte 0x00
     * @param b Byte to process
     */
    private void getField(byte b){
        if(b==0x00){
            args.add(new String(buffer.toByteArray(), StandardCharsets.UTF_8));
            buffer.reset();
            return;
        }
        buffer.write(b);
    }
}
