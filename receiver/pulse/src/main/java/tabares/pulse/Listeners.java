/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

public interface Listeners {
    /**
     * The device is a Error
     * @param exception Contain the exception
     */
    void error(DeviceException exception);

    /**
     * The device change the state of the send
     * @param state Contain the different state
     */
    void state(Pulse.StateDev state);

    /**
     * Indicate the progress of the task
     * @param progress Percent of the progress task
     */
    void progress(float progress);

    /**
     * Listeners for receive normal information
     */
    interface PulseListener extends Listeners {
        /**
         * Receive new task
         * @param type Type of task to receive
         * @param args Args of the task
         */
        void information(Pulse.TypeTask type, String[] args);

        /**
         * Write file byte to byte
         * @param raw Information to write to file
         */
        void fileWrite(byte raw);
    }

    /**
     * Listeners for obtain debug information
     */
    interface PulseListenerDebug extends Listeners {
        /**
         * Contain information of pulse dataMagnetic
         * @param data Contain magnetic fields in micro-Tesla (in order) X, Y and Z
         * @param time The time in nanoseconds at which the event happened
         */
        void dataMagnetic(float[] data, long time);
    }
}
