/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import androidx.annotation.NonNull;

@SuppressLint("MissingPermission")
public class Pulse {
    private static Pulse inst = null;
    private final Listeners listener;
    private final Handler mSensorHandler;

    //DataLink Layer
    private final aa_DataLink aaDataLink;
    private final PulseStats pulseStats;
    private final SensorManager sensorManager;
    private final SensorEventListener magneticListener, gyroscopeListener;
    private final Sensor magneticSensor, gyroscopeSensor;

    //Bluetooth Layer
    private final ab_Bluetooth abBluetooth;
    private boolean connBlue;

    private static final String TAG = "Pulse";

    /**
     * Different task to receive
     */
    public enum TypeTask{
        Bluetooth,
        File,
        Message,
        Wifi,
        Sms,
        Email,
        Bitcoin
    }

    /**
     * Level of security
     */
    public enum TypeSec{
        OPEN,
        WPA2,
        WPA3
    }

    /**
     * State of Device
     */
    public enum StateDev {
        error,
        preparePulse,
        prepareBlue,
        receivePulse,
        receiveBlue
    }

    /**
     * Initialize the class
     * @param context Interface to global information about an application environment
     * @param listener Called when an events occurs
     * @throws DeviceException The device not working successfully
     */
    private Pulse(Context context, @NonNull Listeners listener) throws DeviceException {
        this.listener = listener;
        connBlue=false;

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        HandlerThread mSensorThread = new HandlerThread("Pulse thread", Thread.MAX_PRIORITY);
        mSensorThread.start();
        mSensorHandler = new Handler(mSensorThread.getLooper());

        //Initialize layer DataLink
        aaDataLink = new aa_DataLink(this, listener);
        if (listener instanceof Listeners.PulseListenerDebug) {
            pulseStats = new PulseStats();
            magneticListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    aaDataLink.receive(sensorEvent);
                    pulseStats.addValue(sensorEvent);
                    ((Listeners.PulseListenerDebug) listener).dataMagnetic(sensorEvent.values, sensorEvent.timestamp);
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) { }
            };
        } else if (listener instanceof  Listeners.PulseListener){
            pulseStats = null;
            magneticListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    aaDataLink.receive(sensorEvent);
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) { }
            };
        } else throw new DeviceException(DeviceException.PulseError.SensorNotFound);

        gyroscopeListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                aaDataLink.changeGyroscope(event);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) { }
        };

        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorManager.registerListener(magneticListener, magneticSensor, SensorManager.SENSOR_DELAY_FASTEST, 1000, mSensorHandler);
        sensorManager.registerListener(gyroscopeListener, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL, mSensorHandler);

        //Initialize layer Bluetooth
        abBluetooth = new ab_Bluetooth(this, listener, context, mSensorHandler);
        if(BluetoothAdapter.getDefaultAdapter()==null){
            throw new DeviceException(DeviceException.PulseError.SensorNotFound);
        }
        if(BluetoothAdapter.getDefaultAdapter().isEnabled()) listener.state(StateDev.preparePulse);
        else {
            listener.error(new DeviceException(DeviceException.PulseError.DeviceOff));
            listener.state(StateDev.error);
        }
    }

    /**
     * Create a object unique (if it was created and not finalized, return old object).
     *
     * <p class="note">
     * Note: If the method not find a valid sensor throw a HardwareError
     * </p>
     *
     * @param context Interface to global information about an application environment
     * @param listener A {@link tabares.pulse.Listeners.PulseListener PulseListener} object.
     */
    public static void start(Context context, Listeners listener) throws DeviceException {
        Log.d(TAG, "Start Pulse");
        if (inst == null) inst = new Pulse(context, listener);
    }

    /**
     * Stop Pulse
     */
    public static void stop(){
        Log.d(TAG, "Stop Pulse");
        if(inst == null) return;
        inst.sensorManager.unregisterListener(inst.magneticListener, inst.magneticSensor);
        inst.sensorManager.unregisterListener(inst.gyroscopeListener, inst.gyroscopeSensor);
        inst.abBluetooth.unregisterReceiver();
        inst = null;
    }

    /**
     * Create a object unique (if it was created and not finalized, return old object).
     *
     * <p class="note">
     * Note: If the method not find a valid sensor throw a HardwareError
     * </p>
     */
    public static PulseStats getStats(){
        return inst.pulseStats;
    }

    /**
     * Storage is currently not available
     */
    public static void notStorage(){
        inst.aaDataLink.resetListener();
        inst.abBluetooth.disconnect();

        if(inst.connBlue) inst.changeToDataLink();
        inst.listener.error(new DeviceException(DeviceException.PulseError.WithoutStorage));
    }

    /**
     * Change the next layer to {@link ab_Bluetooth}
     * @param mac Unique identifier for device to connect
     * @param pin  Numeric password for connect to device
     */
    protected void changeToBluetooth(byte[] mac, int pin){
        sensorManager.unregisterListener(magneticListener, magneticSensor);
        sensorManager.unregisterListener(gyroscopeListener, gyroscopeSensor);
        listener.state(StateDev.prepareBlue);
        abBluetooth.connect(mac, pin);
        connBlue=true;
    }

    /**
     * Change teh next layer to {@link aa_DataLink}
     */
    protected void changeToDataLink(){
        listener.state(StateDev.preparePulse);
        abBluetooth.resetLayer();
        if(pulseStats!=null) pulseStats.reset();
        sensorManager.registerListener(magneticListener, magneticSensor, SensorManager.SENSOR_DELAY_FASTEST, 1000, mSensorHandler);
        sensorManager.registerListener(gyroscopeListener, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL, mSensorHandler);
        connBlue=false;
    }
}