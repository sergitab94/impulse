/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import android.hardware.SensorEvent;

import androidx.annotation.NonNull;

class aa_MagneticField {
    //X, Y, Z
    private final float[] values;
    private final long timestamp;

    /**
     * Create Magnetic Field
     * @param values Contain magnetic fields in micro-Tesla (in order) X, Y and Z
     * @param timestamp The time in nanoseconds at which the event happened
     */
    protected aa_MagneticField(float[] values, long timestamp) {
        this.values=values.clone();
        this.timestamp=timestamp;
    }

    /**
     * Create Magnetic Field
     * @param event This class represents a {@link android.hardware.Sensor} event and holds
     *              information such as the sensor's type, the time-stamp, accuracy and of course
     *              the sensor's data.
     */
    protected aa_MagneticField(SensorEvent event){
        values=event.values.clone();
        timestamp=event.timestamp;
    }

    /**
     * Timestamp of the event
     * @return The time in nanoseconds at which the event happened
     */
    protected long getTimestamp(){
        return timestamp;
    }

    /**
     * Magnetic fields
     * @return Contain magnetic fields in micro-Tesla (in order) X, Y, Z
     */
    protected float[] getValues(){
        return values;
    }

    /**
     * Compare this magnetic field with other magnetic field
     * @param mF Other Magnetic Field
     * @param range Maximum range that makes one point different from the original
     * @return If r&gt;0 → mF&gt;this; If r=0 → mF=this; If r&lt;0 → mF&lt;this
     */
    protected float compare(aa_MagneticField mF, int range) {
        return compare(mF, range, 7);
    }

    /**
     * Compare this magnetic field with other magnetic field with a mask
     * @param mF Magnetic field to compare
     * @param range Maximum range that makes one point different from the original
     * @param mask Mask to compare axes (X: 1; Y: 2; Z: 4). Where 7 compares all axes and 1 compares
     *             only the X axis.
     * @return If r&gt;0 → mF&gt;this; If r=0 → mF=this; If r&lt;0 → mF&lt;this
     */
    protected float compare(aa_MagneticField mF, int range, int mask) {
        if (1>mask || mask>7) return 0;
        float finish=0;
        for(int i=0; i<3; i++){
            if ((mask & (int) Math.pow(2, i))==0) continue;
            if(values[i]+range<mF.values[i]) finish-=Math.abs(mF.values[i]-values[i]);
            else if(values[i]-range>mF.values[i]) finish+=Math.abs(values[i]-mF.values[i]);
        }
        return finish;
    }

    /**
     * Get maximus axes for a Magnetic field
     * @return Number with a axis X=0, Y=1 and Z=2
     */
    protected int getMaxAxis(){
        int pos=0;
        float max=values[0];
        for(int n=1; n<3; n++) {
            if (values[n]>max) {
                max=values[n];
                pos=n;
            }
        }
        return pos;
    }

    /**
     * Get minimum axes for a Magnetic field
     * @return Number with a axis X=0, Y=1 and Z=2
     */
    protected int getMinAxis(){
        int pos=0;
        float min=values[0];
        for(int n=1; n<3; n++) {
            if (values[n]<min) {
                min=values[n];
                pos=n;
            }
        }
        return pos;
    }

    /**
     * Convert Magnetic field in a String
     * @return Object convert in String
     */
    @NonNull
    @Override
    public String toString() {
        return "X:"+values[0]+";Y:"+values[1]+";Z:"+values[2]+";Time:"+timestamp;
    }
}