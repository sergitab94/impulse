/* **************************************************************************
 **                                                                        **
 **  Impulse                                                               **
 **  Copyright (C) 2019-2021  Sergio Tabarés Medina                        **
 **                                                                        **
 **  This program is free software: you can redistribute it and/or modify  **
 **  it under the terms of the GNU General Public License as published by  **
 **  the Free Software Foundation, either version 3 of the License, or     **
 **  (at your option) any later version.                                   **
 **                                                                        **
 **  This program is distributed in the hope that it will be useful,       **
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          **
 **  GNU General Public License for more details.                          **
 **                                                                        **
 **  You should have received a copy of the GNU General Public License     **
 **  along with this program.  If not, see http://www.gnu.org/licenses/.   **
 **                                                                        **
 ****************************************************************************/

package tabares.pulse;

import android.hardware.SensorEvent;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import tabares.pulse.RS.listRS;

//Low level
class aa_DataLink {
    //Move phone
    private float preRot=0, timeRot=0;
    private boolean move=false;

    //Data receive
    private final aa_MagneticList pulses;

    //Calibrate information
    private boolean calibrate=false, preCalibrate=false;
    private final ArrayList<aa_MagneticList> powerCalibrate, timeCalibrate;

    //Start next pulse
    private long timeRemain=0;

    //Characteristic hardware
    private boolean isPositive=true;
    private final int[] numBit;
    private final ArrayList<ArrayList<Float>> maxPower;

    //Next Layers
    private final b_Application application;

    //Listener
    private final Listeners listener;

    //RS and restructure data
    private final listRS lRS;
    private listRS.infSol[] oSol;

    //Frame
    private static final int sizeFrame=50;

    //Byte information
    private final ArrayList<ArrayList<Byte>> rsvData, frameData;

    //Delete task info
    private listRS.infSol solZ;
    private final ArrayList<Byte> checkFrame;
    private int rsDelete;

    //Data Byte output
    private final ArrayList<Byte> data;
    private int dataBit;

    private static final String TAG = "Pulse";

    /**
     * Initialize layer and all variables
     * @param pulse Top layer used in application for exchange between {@link aa_DataLink} and
     *              {@link ab_Bluetooth}
     * @param listener Called when an events occurs
     */
    protected aa_DataLink(Pulse pulse, Listeners listener){
        //Data receive
        pulses = new aa_MagneticList();

        //Calibrate information
        powerCalibrate = new ArrayList<>();
        timeCalibrate = new ArrayList<>();
        for(int i=0; i<6; i++) powerCalibrate.add(new aa_MagneticList());

        //Characteristic hardware
        numBit = new int[3];
        maxPower = new ArrayList<>();
        for(int i=0; i<3; i++) maxPower.add(new ArrayList<>());

        //RS
        lRS=new listRS();

        //Next layer
        application = new b_Application(pulse, listener);
        this.listener=listener;

        //Byte information
        frameData = new ArrayList<>();
        rsvData = new ArrayList<>();
        for(int i=0; i<3; i++) {
            frameData.add(new ArrayList<>());
            rsvData.add(new ArrayList<>());
        }

        //Delete task info
        checkFrame = new ArrayList<>();

        //Data Byte output
        data =new ArrayList<>();
        dataBit=0;
    }

    /**
     * Callback when the sensor gyroscope changes. Detects if the phone moves, changes the boolean
     * "move"
     * @param event This class represents a {@link android.hardware.Sensor} event and holds
     *              information such as the sensor's type, the time-stamp, accuracy and of course
     *              the sensor's data.
     */
    protected void changeGyroscope(SensorEvent event){
        float diffRot, diffT;
        if(timeRot==0) {
            timeRot=event.timestamp;
            preRot=event.values[0]+event.values[1]+event.values[2];
            return;
        }
        diffT=Math.abs(timeRot-event.timestamp);
        if(diffT<1_000_000_000 && move) {
            preRot=event.values[0]+event.values[1]+event.values[2];
            return;
        }
        diffRot=Math.abs(preRot-event.values[0]-event.values[1]-event.values[2]);
        preRot=event.values[0]+event.values[1]+event.values[2];
        timeRot=event.timestamp;
        move=(diffRot>0.02);
    }

    /**
     * Callback when the magnetic field sensor changes
     * @param event This class represents a {@link android.hardware.Sensor} event and holds
     *              information such as the sensor's type, the time-stamp, accuracy and of course
     *              the sensor's data.
     */
    protected void receive(SensorEvent event){
        if (move) { moveHardware(); return; }
        pulses.add(new aa_MagneticField(event));
        if(!preCalibrate) { preCalibrate(); return; }
        if(!calibrate) { calibrate(); return; }
        readData();
    }

    /**
     * If {@link #changeGyroscope(SensorEvent)} detect a change, this function send error and reset
     * all.
     */
    private void moveHardware(){
        if(calibrate) listener.error(new DeviceException(DeviceException.PulseError.MoveDevice));
        resetListener();
    }

    /**
     * Check it the phone is over the device
     */
    private void preCalibrate(){
        aa_MagneticList maxFunction=new aa_MagneticList(), minFunction=new aa_MagneticList();
        if(pulses.size()<15) return;

        pulses.deleteEndLeft(750); //15 pulses * 50 ms
        pulses.getMaxMin(maxFunction, minFunction, 5, 4);
        int maxSize=maxFunction.size(), minSize=minFunction.size();
        if(!((minSize==3 && maxSize==4) || (minSize==4 && maxSize==3))) return;

        //Pre-Calibrate successfully, save data
        isPositive=(maxSize==3);

        //Check if correct
        if(isPositive) minFunction.remove(0);
        else maxFunction.remove(0);

        if(!maxFunction.isEqual(5, 4)) return;
        if(!minFunction.isEqual(5, 4)) return;
        Log.d(TAG, "Pre-calibrate");

        //Save Timestamp
        aa_MagneticList time=pulses.copy();
        time.remove(pulses.size()-1); //Remove the last one that is less than the rest
        timeCalibrate.addAll(time.outRange(5, 4));

        //Save max, min and delete unnecessary pulses
        if(isPositive){
            pulses.deleteLeft(minFunction.get(2).getTimestamp());
            minFunction.remove(2);
            (powerCalibrate.get(2)).addAll(maxFunction);
            (powerCalibrate.get(5)).addAll(minFunction);
        } else {
            pulses.deleteLeft(maxFunction.get(2).getTimestamp());
            maxFunction.remove(2);
            (powerCalibrate.get(2)).addAll(minFunction);
            (powerCalibrate.get(5)).addAll(maxFunction);
        }
        preCalibrate=true;
    }

    /**
     * Detect number of solenoids and number of levels
     */
    private void calibrate(){
        aa_MagneticList maxFunction=new aa_MagneticList(), minFunction=new aa_MagneticList();
        int sizePul=pulses.size();

        //Detect End
        if(sizePul<2) return;
        long distance = (pulses.get(sizePul-1)).getTimestamp()-(pulses.get(0)).getTimestamp();
        if(distance<200_000_000) return; //4 pulses * 50.000.000 ns
        aa_MagneticList detectStop=pulses.copy();
        detectStop.deleteEndLeft(175); //3.5 pulses * 50 ms
        detectStop.getMaxMin(maxFunction, minFunction, 5, 7);
        if(maxFunction.size()!=0 || minFunction.size()!=0) return;
        Log.d(TAG, "Calibrate");

        //Search Max and Minimum and save in powerCalibrate
        int maxPulsesInt;
        float maxPulse = pulses.get(pulses.size()-1).getTimestamp()-pulses.get(0).getTimestamp();
        maxPulse -= 100_000_000; //2 pulses * 50.000.000 ns
        maxPulse /= 200_000_000; //4 pulses * 50.000.000 ns
        maxPulsesInt = Math.round(maxPulse);

        long timeStamp=pulses.get(0).getTimestamp();
        for(int i=0, s=2, mask=4; i<maxPulsesInt; i++) {
            aa_MagneticList range, rangeOrg, topFunction, bottomFunction;
            int pos;
            rangeOrg = pulses.copy();
            rangeOrg.deleteRange(timeStamp, timeStamp + 200_000_000); //4 pulses * 50.000.000 ns

            // While allow search in the next solenoid if not found nothing
            while(true) {
                range=rangeOrg.copy();
                range.getMaxMin(maxFunction, minFunction, 5, mask);
                topFunction = (isPositive) ? maxFunction : minFunction;
                bottomFunction = (isPositive) ? minFunction : maxFunction;

                if (topFunction.size() != 1 || bottomFunction.size() != 1) {
                    String error = "Failed minFunction " + maxFunction +
                                   " and maxFunction " + minFunction;
                    listener.error(new DeviceException(DeviceException.PulseError.FailedPulse, error));
                    resetListener();
                    return;
                }

                if (isPositive) pos = topFunction.get(0).getMaxAxis();
                else pos = topFunction.get(0).getMinAxis();

                if (pos > s || (i == 0 && pos < s)) {
                    String error = "Failed solenoid not consecutive " + i + ", " + pos;
                    listener.error(new DeviceException(DeviceException.PulseError.FailedPulse, error));
                    resetListener();
                    return;
                }

                if(pos==s) break;
                s=pos;
                mask=(int) Math.pow(2, s);
            }

            (powerCalibrate.get(s)).add(topFunction.get(0));
            (powerCalibrate.get(s+3)).add(bottomFunction.get(0));
            timeStamp += 200_000_000;
        }

        //Number levels
        for(int i=0; i<3; i++) numBit[i]=powerCalibrate.get(i).size();
        numBit[2]-=3;

        //Max power solenoids
        for(int i=0; i<3; i++){
            List<Float> act=maxPower.get(i);
            act.clear();
            if(numBit[i]==0) continue;
            else if (numBit[i]==1) {
                act.add((powerCalibrate.get(i).avg(i)+powerCalibrate.get(i+3).avg(i))/2f);
            } else {
                float base=powerCalibrate.get(i+3).avg(i), passValue;
                int numLevel=(int) Math.pow(2, numBit[i])-2;
                passValue=((powerCalibrate.get(i)).avg(i)-base)/numLevel;

                //Calc zero
                base -= passValue/2;
                act.add(base);

                //Calc 1 to numLevel (1 to 6)
                for(int n=1; n<=numLevel; n++) act.add(base+(passValue*n));
            }
        }

        //TimeStamp of pulse
        long timeUpdate=2;
        for(int i=0; i<3; i++) timeUpdate+=(numBit[i]*2L);
        timeUpdate*=100_000_000; //2 pulses * 50.000.000 ns
        timeRemain=0;

        for(int i=5; i>=0; i--){
            timeRemain+=(timeCalibrate.get(i).avgTime(2)+timeUpdate-timeRemain)/(6-i);
            timeUpdate+=100_000_000; //2 pulses * 50.000.000 ns
        }

        //Prepare RS
        lRS.createRS(numBit);
        oSol =lRS.getInfo();
        solZ = oSol[2].copy(); //Get solenoid Z
        rsDelete = (solZ.sPulse*solZ.k)/solZ.n;
        oSol=orderPulses(oSol);

        //Delete unnecessary pulses
        pulses.deleteLeft(timeRemain);
        timeRemain+=50_000_000; //1 pulse * 50.000.000 ns

        //Delete unnecessary information calibrate
        for(int i=0; i<6; i++) (powerCalibrate.get(i)).clear();
        timeCalibrate.clear();

        Log.d(TAG, "Solenoid number (X, Y, Z): " + Arrays.toString(numBit) +
                " TimeRemain: " + timeRemain);
        listener.state(Pulse.StateDev.receivePulse);
        calibrate=true;
    }

    /**
     * Read frame received
     */
    private void readData() {
        //If out range, search pulse
        if(pulses.size()==0 || pulses.get(pulses.size()-1).getTimestamp()<timeRemain) return;

        //Convert power field to byte
        aa_MagneticField mgField=pulses.getFieldTimestamp(timeRemain-25_000_000); //0.5 pulses * 50.000.000 ns
        float[] field=mgField.getValues();
        for(int i=0; i<3; i++) {
            if(numBit[i]==0)  continue;
            byte rcv=0;
            int numLevel=(int) Math.pow(2, numBit[i])-1;
            if(isPositive) while(rcv<numLevel && field[i]>=maxPower.get(i).get(rcv)) rcv++;
            else while(rcv<numLevel && field[i]<=maxPower.get(i).get(rcv)) rcv++;

            rcv=intToGray(rcv);
            checkFrame.add(rcv);
            frameData.get(i).add(rcv);
        }

        //Prepare to next pulse
        pulses.deleteLeft(timeRemain);
        timeRemain+=50_000_000; //1 pulse * 50.000.000 ns

        if(solZ.sPulse==checkFrame.size() && checkDelete()) {
            Log.d(TAG, "Task delete in computer");
            listener.error(new DeviceException(DeviceException.PulseError.DeleteTask,
                    "Task delete in computer"));
            resetListener();
            return;
        }
        if(frameData.get(2).size()>=sizeFrame) processFrame();
    }

    /**
     * Convert from int number to scale Gray
     * @param input Number in normal scale
     * @return Number in scale Gray
     */
    private byte intToGray(byte input){
        return (byte) (input^(input>>1));
    }

    /**
     * Check if task is deleted in the computer
     * @return True if it is a deleted task; False if it is a correctly task
     */
    private boolean checkDelete(){
        Log.d(TAG, "Frame: " + checkFrame);
        for(int x=0; x<rsDelete; x++){
            if(checkFrame.get(x)!=0x00) {
                checkFrame.clear();
                return false;
            }
        }

        int maxNumPulse = (int) Math.pow(2, (float) (solZ.n*solZ.m)/solZ.sPulse)-1;
        for(int x=rsDelete; x<solZ.sPulse; x++){
            if(checkFrame.get(x)!=maxNumPulse) {
                checkFrame.clear();
                return false;
            }
        }

        checkFrame.clear();
        return true;
    }

    /**
     * Process a frame and pass it to the next layer
     */
    private void processFrame(){
        int sRS=0;

        for (listRS.infSol i : oSol) {
            if(rsvData.get(i.pos).size()==0) continue;

            ArrayList<Byte> rsData = new ArrayList<>(rsvData.get(i.pos));
            sRS=i.sPulse-rsvData.get(i.pos).size();
            rsData.addAll(frameData.get(i.pos).subList(0, sRS));

            Log.d(TAG, "Solenoid " + i.pos + ": " + rsData);

            try{
                rsData=lRS.getData(rsData, i.pos);
            } catch (Exception ignored){
                listener.error(new DeviceException(DeviceException.PulseError.IncorrectRS,
                        "Reed-Solomon error: " + rsData));
                resetListener();
                return;
            }
            joinByte(rsData, i.m);
            rsvData.get(i.pos).clear();
        }

        for (listRS.infSol i : oSol){
            if(i.sPulse==0) continue;
            for(; sRS<=50-i.sPulse; sRS+=i.sPulse){
                ArrayList<Byte> rsData = new ArrayList<>(frameData.get(i.pos).subList(sRS, sRS+i.sPulse));
                Log.d(TAG, "Solenoid " + i.pos + ": " + rsData);

                try {
                    rsData=lRS.getData(rsData, i.pos);
                } catch (Exception e){
                    listener.error(new DeviceException(DeviceException.PulseError.IncorrectRS,
                            "Reed-Solomon error: " + rsData));
                    resetListener();
                    return;
                }
                joinByte(rsData, i.m);
            }
            rsvData.get(i.pos).clear();
            if(sRS!=50) rsvData.get(i.pos).addAll(frameData.get(i.pos).subList(sRS, 50));
            frameData.get(i.pos).clear();
        }

        if(dataBit==0) {
            application.addFrame(data);
            data.clear();
        } else {
            application.addFrame(new ArrayList<>(data.subList(0, data.size()-1)));
            byte end=data.get(data.size()-1);
            data.clear();
            data.add(end);
        }
        if(application.isEnd()) {
            if(application.notIsBluetooth()) listener.state(Pulse.StateDev.preparePulse);
            resetData();
        }
    }

    /**
     * Join bits of pulse in complete bytes
     * @param input List input of bytes, separate in m bits size
     * @param m Bits in bytes element in list
     */
    private void joinByte(ArrayList<Byte> input, int m){
        byte dByte=0;
        if(data.size()>0){
            dByte = data.get(data.size()-1);
            data.remove(data.size()-1);
        }

        for(byte i: input){
            if(8-dataBit<m) dByte= (byte) ((dByte<<8-dataBit) | i>>m-8+dataBit);
            else dByte = (byte) ((dByte << m) | i);

            dataBit += m;
            if(8<=dataBit){
                data.add(dByte);
                dByte = (dataBit>8)? i:0;
                dataBit -= 8;
            }
        }
        data.add(dByte);
    }

    /**
     * Order list for size solenoids
     * @param list All solenoids of device
     * @return List to order all solenoids
     */
    private listRS.infSol[] orderPulses(listRS.infSol[] list){
        listRS.infSol[] end = new listRS.infSol[list.length];

        for(int x=0; x<list.length; x++){
            int z=-1, res=0;
            for(int y=0; y<list.length; y++){
                if(list[y].sPulse>=res && y!=z){
                    z=y;
                    res=list[y].sPulse;
                }
            }
            end[x]=list[z].copy();
            list[z].sPulse=-1;
        }
        return end;
    }

    /**
     * Reset all layer DataLink and reset next layer {@link b_Application}
     */
    protected void resetListener(){
        //Change state if calibrate
        if(calibrate) listener.state(Pulse.StateDev.preparePulse);
        application.resetLayer();
        resetData();
    }

    /**
     * Reset all layer DataLink
     */
    private void resetData(){
        //Delete calibrate
        preCalibrate=calibrate=false;

        //Data receive
        pulses.clear();

        //Calibrate information
        for(int i=0; i<4; i++) (powerCalibrate.get(i)).clear();
        timeCalibrate.clear();

        //Clear data
        for(int i=0; i<3; i++) (maxPower.get(i)).clear();

        //Byte information
        for(int i=0; i<3; i++) rsvData.get(i).clear();
        for(int i=0; i<3; i++) frameData.get(i).clear();

        //Data Byte output
        data.clear();
        dataBit=0;

        //Check frame
        checkFrame.clear();
    }
}
